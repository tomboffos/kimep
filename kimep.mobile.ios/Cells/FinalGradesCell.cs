﻿using System;

using Foundation;
using kimep.mobile.domain.Models;
using UIKit;

namespace kimep.mobile.ios
{
    public partial class FinalGradesCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("FinalGradesCell");
        public static readonly UINib Nib;

        static FinalGradesCell()
        {
            Nib = UINib.FromName("FinalGradesCell", NSBundle.MainBundle);
        }
        internal void UpdateCell(FinalGrade grade)
        {
            TitleLabel.Text = grade.Title;
            DetailsLabel.Text = string.Format("Grade: {0}/ Points: {1}", grade.Grade, grade.Point);

        }
        protected FinalGradesCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
