﻿using System;

using Foundation;
using kimep.mobile.domain.Models;
using UIKit;

namespace kimep.mobile.ios
{
    public partial class ContactCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("ContactCell");
        public static readonly UINib Nib;

        static ContactCell()
        {
            Nib = UINib.FromName("ContactCell", NSBundle.MainBundle);
        }

        internal void UpdateCell(ContactModel contact)
        {
            NameLabel.Text = contact.Name;
            PositionLabel.Text = string.Format("| {0}", contact.Position);
            AddressLabel.Text = contact.Address;
            PhoneLabel.Text = contact.Phone;
        }

        protected ContactCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
