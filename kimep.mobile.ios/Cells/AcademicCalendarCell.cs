﻿using System;
using kimep.mobile.domain.Models;

using Foundation;
using UIKit;

namespace kimep.mobile.ios
{
    public partial class AcademicCalendarCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("AcademicCalendarCell");
        public static readonly UINib Nib;

        static AcademicCalendarCell()
        {
            Nib = UINib.FromName("AcademicCalendarCell", NSBundle.MainBundle);
        }

        internal void UpdateCell(acaEvent e)
        {
            DescriptionLabel.Text = e.Description;
            SemesterLabel.Text = e.Semester;
            TimeZone tz = TimeZone.CurrentTimeZone;
            DateLabel.Text = tz.ToLocalTime(e.Date).ToLongDateString();
        }

        protected AcademicCalendarCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
