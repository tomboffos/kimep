﻿using System;
using Foundation;
using UIKit;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;

namespace kimep.mobile.ios
{
    public partial class MenuCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("MenuCell");
        public static readonly UINib Nib;
        IAuxiliary aux = new Auxiliary();
        static MenuCell()
        {
            Nib = UINib.FromName("MenuCell", NSBundle.MainBundle);
        }
        internal void UpdateCell(MenuItem item)
        {
            lblTitle.Text = item.Text;
            lblTitle.TextColor = UIColor.FromRGB(230, 231, 232);
            lblTitle.HighlightedTextColor = UIColor.FromRGB(71, 57, 131);
            imgIcon.Image = aux.GetColoredImage(item.Icon, UIColor.FromRGB(230, 231, 232));
            imgIcon.HighlightedImage = aux.GetColoredImage(item.Icon, UIColor.FromRGB(71, 57, 131));
        }
        protected MenuCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }        
    }
}
