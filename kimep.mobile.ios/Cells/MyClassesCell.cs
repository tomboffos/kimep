﻿using System;
using System.Linq;
using Foundation;
using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using UIKit;
using kimep.mobile.domain.Common;
using System.Globalization;

namespace kimep.mobile.ios
{
    public partial class MyClassesCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("MyClassesCell");
        public static readonly UINib Nib;
        private IRequests requests = new Requests();
        static MyClassesCell()
        {
            Nib = UINib.FromName("MyClassesCell", NSBundle.MainBundle);
        }

        protected MyClassesCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        internal void UpdateCell(MyClass day, NSDictionary[] alerts)
        {
            string todayOfWeek = (new CultureInfo("en-US")).DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);

            if (day.WeekDay == todayOfWeek)
            {
                //R: 84 G: 72 B: 146
                this.BackgroundColor = UIColor.FromRGB(0xE6, 0xE7, 0xE8);
                // UIColor.FromRGB(238, 227, 234);
                //TimeLabel.TextColor =
                //    InstructorLabel.TextColor =
                //        HallLabel.TextColor =
                //            TitleLabel.TextColor =
                //                DatesLabel.TextColor = null;
                //                    //UIColor.FromRGB(84, 72, 148);
            }
            else
            {
                this.BackgroundColor = null;
                //TimeLabel.TextColor =
                //    InstructorLabel.TextColor =
                //        HallLabel.TextColor =
                //            TitleLabel.TextColor =
                //                DatesLabel.TextColor = UIColor.Gray;
            }

            TimeZone tz = TimeZone.CurrentTimeZone;
            NSDictionary dic = new NSDictionary("CourseID", day.CourseID, 
                                                "Semester", day.Semester,
                                                "WeekDay", day.WeekDay, 
                                                "Time_From", day.Time_From.DateTimeToNSDate(),
                                                "Date_To", day.Date_To.DateTimeToNSDate());

            if (alerts.Any(item =>
                           item["CourseID"].ToString() == dic["CourseID"].ToString() &&
                           item["Semester"].ToString() == dic["Semester"].ToString() &&
                           item["WeekDay"].ToString() == dic["WeekDay"].ToString() &&
                           item["Time_From"].ToString() == dic["Time_From"].ToString()))
            {
                imageNotification.Alpha = 1F;
            }
            else
            {
                imageNotification.Alpha = .05F;
            }

            TimeLabel.Text = day.Time_From == day.Time_To ? "" :
                string.Format("{0:HH:mm}-{1:HH:mm}",
                tz.ToLocalTime(day.Time_From),
                tz.ToLocalTime(day.Time_To ?? DateTime.MinValue));
            TitleLabel.Text = day.Title;
            InstructorLabel.Text = day.Instructor;
            HallLabel.Text = day.Hall == "#NA" ? "" : day.Hall;
            DatesLabel.Text = string.Format("{0:MMM dd, yyyy} - {1:MMM dd, yyyy}", day.Date_From, day.Date_To);
        }
    }
}
