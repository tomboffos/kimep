﻿using System;

using Foundation;
using UIKit;
using kimep.mobile.domain.Models;

namespace kimep.mobile.ios
{
    public partial class AssessmentScoresCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("AssessmentScoresCell");
        public static readonly UINib Nib;

        static AssessmentScoresCell()
        {
            Nib = UINib.FromName("AssessmentScoresCell", NSBundle.MainBundle);
        }
        internal void UpdateCell(AssessmentScoreModel score)
        {
            labelTitle.Text = string.Format("{0}", score.TitleCourses, score.Code);

            var ass1 = string.Format("{0:N2}", score.Score1);
            var ass2 = string.Format("{0:N2}", score.Score2);
            var ass3 = string.Format("{0:N2}", score.Score3);

            labelFirstAssessment.Text = ass1 == "" ? "-" : ass1;
            labelSecondAssessment.Text = ass2 == "" ? "-" : ass2;
            labelFinalAssessment.Text = ass3 == "" ? "-" : ass3;

            labelAccess.Text = score.FinalAssessment;
                //string.Format("Access to the Final assessment / Exam is {0}", score.FinalAssessment);
                //string.Format("Total: {0:N2}", (score.Score1 ?? 0) + (score.Score2 ?? 0) + (score.Score3 ?? 0));
                //;
            switch (score.FinalAssessment)
            {
                case "Allowed":
                    labelAccess.TextColor = UIColor.FromRGB(0x28, 0xA7, 0x45);
                    break;
                case "-":
                    labelAccess.TextColor = UIColor.FromRGB(0xFF, 0xCC, 0x07);
                    break;
                case "Not Allowed":
                    labelAccess.TextColor = UIColor.FromRGB(0xDC, 0x35, 0x45);
                    break;
            }
        }
        protected AssessmentScoresCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
