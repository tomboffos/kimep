﻿using System;
using kimep.mobile.domain.Models;
using Foundation;
using UIKit;

namespace kimep.mobile.ios
{
    public partial class BorrowedBooksCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("BorrowedBooksCell");
        public static readonly UINib Nib;

        static BorrowedBooksCell()
        {
            Nib = UINib.FromName("BorrowedBooksCell", NSBundle.MainBundle);
        }

        protected BorrowedBooksCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        internal void UpdateCell(BorrowedBook book)
        {
            TimeZone tz = TimeZone.CurrentTimeZone;
            AuthorLabel.Text = book.Author;
            DateDueLabel.Text = string.Format("{0:MMM dd, yyyy}", tz.ToLocalTime(book.DateDUE.Value));
            NameLabel.Text = book.Name;
        }
    }
}
