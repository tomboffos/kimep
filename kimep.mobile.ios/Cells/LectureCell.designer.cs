﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace kimep.mobile.ios
{
    [Register ("LectureCell")]
    partial class LectureCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnFavor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Desc { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Icon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Name { get; set; }

        [Action ("BtnFavor_TouchDown")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BtnFavor_TouchDown ();

        void ReleaseDesignerOutlets ()
        {
            if (btnFavor != null) {
                btnFavor.Dispose ();
                btnFavor = null;
            }

            if (Desc != null) {
                Desc.Dispose ();
                Desc = null;
            }

            if (Icon != null) {
                Icon.Dispose ();
                Icon = null;
            }

            if (Name != null) {
                Name.Dispose ();
                Name = null;
            }
        }
    }
}