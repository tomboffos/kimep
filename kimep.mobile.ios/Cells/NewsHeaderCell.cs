﻿using System;

using Foundation;
using UIKit;
using kimep.mobile.domain.Models;
using CoreGraphics;

namespace kimep.mobile.ios.Cells
{
    public partial class NewsHeaderCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("NewsHeaderCell");
        public static readonly UINib Nib;

        static NewsHeaderCell()
        {
            Nib = UINib.FromName("NewsHeaderCell", NSBundle.MainBundle);
        }
        internal void UpdateCell(ShortNews news)
        {
            labelTitle.Text = news.Title;
            //labelShortText.Text = news.ShortText;
            labelAuditory.Text = string.Format("{0}/{1}", news._Auditory, news.Auditory);
            var data = NSData.FromArray(Convert.FromBase64String(news.Image));
            UIImage image = UIImage.LoadFromData(data);
            var ratio = image.Size.Height / image.Size.Width;
            imagePhoto.Frame = new CGRect(0, 0, Bounds.Size.Width, Bounds.Size.Width * ratio);
            imagePhoto.Image = image;
            PhotoConstraint.Constant = Bounds.Size.Width * ratio;
            labelCreatedOn.Text = string.Format("{0} {1}", news.CreatedOn.ToLongDateString(), news.CreatedOn.ToShortTimeString());
        }
        protected NewsHeaderCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
