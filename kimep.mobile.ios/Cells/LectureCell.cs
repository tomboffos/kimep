﻿using System;

using Foundation;
using UIKit;
using kimep.mobile.domain.Models;
using kimep.mobile.domain.Common;
using kimep.mobile.ios.Common;
using System.Threading.Tasks;
using kimep.mobile.ios.Controllers;
using System.Collections.Generic;

namespace kimep.mobile.ios
{
    public partial class LectureCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("LectureCell");
        public static readonly UINib Nib;
        private IRequests req = new Requests();
        private IAuxiliary aux = new Auxiliary();
        LectureModel _lecture;
        LectureViewController _controller;
        static LectureCell()
        {
            Nib = UINib.FromName("LectureCell", NSBundle.MainBundle);
        }
        protected LectureCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
        public void UpdateCell(LectureModel lecture, LectureViewController controller)
        {
            _lecture = lecture;
            _controller = controller;
            Name.Text = lecture.Name;

            btnFavor.SetImage(lecture.IsFavorite ?
                aux.GetColoredImage("Favor", UIColor.FromRGB(0xD2, 0xAB, 0x67)) :
                aux.GetColoredImage("Favor", UIColor.FromRGB(230, 230, 230)),
                UIControlState.Normal);

            if (lecture.Type == "Directory")
            {
                if (lecture.Name == "..")
                {
                    btnFavor.Hidden = true;
                    Icon.Image = UIImage.FromFile("Images/folder_parent.png");
                        //aux.GetColoredImage("folder-out", UIColor.FromRGB(81, 70, 136));
                    Desc.Text = NSBundle.MainBundle.LocalizedString("Go To Parent Folder", "Go To Parent Folder");
                }
                else if (lecture.Name == "Favorites")
                {
                    btnFavor.Hidden = true;
                    Icon.Image = UIImage.FromFile("Images/folder_favorites.png");
                    //aux.GetColoredImage("folder-check", UIColor.FromRGB(0xD2, 0xAB, 0x67));
                    var favs = aux.GetCache<List<LectureModel>>("lecfavor") ?? new List<LectureModel>();
                    Desc.Text = String.Format(
                        NSBundle.MainBundle.LocalizedString("Contains Item(s): {0}", "Contains Item(s): {0}"),
                        favs.Count);
                    //NSBundle.MainBundle.LocalizedString("My Favorites", "My Favorites");
                }
                else if (lecture.Size == 0)
                {
                    btnFavor.Hidden = false;
                    SelectionStyle = UITableViewCellSelectionStyle.None;
                    Icon.Image = UIImage.FromFile("Images/folder_empty.png");
                    //aux.GetColoredImage("folder-empty", UIColor.LightGray);
                    Desc.Text = NSBundle.MainBundle.LocalizedString("The Folder is Empty", "The Folder is Empty");
                }
                else if (lecture.Size == -1)
                {
                    btnFavor.Hidden = true;
                    SelectionStyle = UITableViewCellSelectionStyle.None;
                    Icon.Image = UIImage.FromFile("Images/folder_lock.png");
                    //aux.GetColoredImage("folder-lock", UIColor.LightGray);
                    Desc.Text = NSBundle.MainBundle.LocalizedString("Access Denied", "Access Denied");
                }
                else
                {
                    btnFavor.Hidden = false;
                    Icon.Image = UIImage.FromFile("Images/folder.png");
                    //aux.GetColoredImage("folder", UIColor.FromRGB(81, 70, 136));
                    Desc.Text = String.Format(
                        NSBundle.MainBundle.LocalizedString("Contains Item(s): {0}", "Contains Item(s): {0}"),
                        lecture.Size);
                }
            }
            else
            {
                btnFavor.Hidden = false;
                var image = UIImage.FromFile(string.Format("Images/{0}.png", lecture.Type));
                    //aux.GetColoredImage(lecture.Type, UIColor.FromRGB(81, 70, 136));
                if (image == null)
                {
                    Icon.Image = UIImage.FromFile("Images/file.png");
                }
                else
                {
                    Icon.Image = image;
                }
                Desc.Text = String.Format(
                    NSBundle.MainBundle.LocalizedString("File Size: {0}", "File Size: {0}"),
                    req.SizeSuffix(lecture.Size));
            }
        }
        
        partial void BtnFavor_TouchDown()
        {
            GifOverlay loadPop = new GifOverlay(UIScreen.MainScreen.Bounds);
            // Show loading progress
            _controller.Root.Add(loadPop);

            Task.Factory.StartNew(() =>
            {
                if (_lecture.IsFavorite)
                {
                    _lecture.IsFavorite = false;
                    aux.RemoveFavorites(_lecture);
                }
                else
                {
                    _lecture.IsFavorite = true;
                    aux.AddFavorites(_lecture);
                }
            }).ContinueWith(task =>
            {
                loadPop.Hide();
                _controller.TableView.ReloadData();
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
