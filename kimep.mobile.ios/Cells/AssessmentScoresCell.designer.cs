﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace kimep.mobile.ios
{
    [Register ("AssessmentScoresCell")]
    partial class AssessmentScoresCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelAccess { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelFinalAssessment { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelFirstAssessment { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelSecondAssessment { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelTitle { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (labelAccess != null) {
                labelAccess.Dispose ();
                labelAccess = null;
            }

            if (labelFinalAssessment != null) {
                labelFinalAssessment.Dispose ();
                labelFinalAssessment = null;
            }

            if (labelFirstAssessment != null) {
                labelFirstAssessment.Dispose ();
                labelFirstAssessment = null;
            }

            if (labelSecondAssessment != null) {
                labelSecondAssessment.Dispose ();
                labelSecondAssessment = null;
            }

            if (labelTitle != null) {
                labelTitle.Dispose ();
                labelTitle = null;
            }
        }
    }
}