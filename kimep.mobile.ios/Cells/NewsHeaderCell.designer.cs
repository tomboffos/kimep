﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace kimep.mobile.ios.Cells
{
    [Register ("NewsHeaderCell")]
    partial class NewsHeaderCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imagePhoto { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelAuditory { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelCreatedOn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint PhotoConstraint { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (imagePhoto != null) {
                imagePhoto.Dispose ();
                imagePhoto = null;
            }

            if (labelAuditory != null) {
                labelAuditory.Dispose ();
                labelAuditory = null;
            }

            if (labelCreatedOn != null) {
                labelCreatedOn.Dispose ();
                labelCreatedOn = null;
            }

            if (labelTitle != null) {
                labelTitle.Dispose ();
                labelTitle = null;
            }

            if (PhotoConstraint != null) {
                PhotoConstraint.Dispose ();
                PhotoConstraint = null;
            }
        }
    }
}