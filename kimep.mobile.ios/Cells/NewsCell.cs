﻿using System;

using Foundation;
using UIKit;
using kimep.mobile.domain.Models;

namespace kimep.mobile.ios.Cells
{
    public partial class NewsCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("NewsCell");
        public static readonly UINib Nib;

        static NewsCell()
        {
            Nib = UINib.FromName("NewsCell", NSBundle.MainBundle);
        }

        protected NewsCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        internal void UpdateCell(ShortNews news)
        {
            if (news.ReadOn.HasValue)
            {
                imageviewView.Alpha = labelAuditory.Alpha =
                    labelCreatedOn.Alpha = imagePhoto.Alpha = 
                        labelShortText.Alpha = labelTitle.Alpha = 0.7f;
            }
            labelAuditory.Text = string.Format("{0}/{1}", news._Auditory, news.Auditory);
            labelTitle.Text = news.Title;
            labelShortText.Text = news.ShortText;
            var data = NSData.FromArray(Convert.FromBase64String(news.Image));
            imagePhoto.Image = UIImage.LoadFromData(data);
            labelCreatedOn.Text = string.Format("{0} {1}", news.CreatedOn.ToLongDateString(), news.CreatedOn.ToShortTimeString());
        }
    }
}
