﻿using System;

using Foundation;
using kimep.mobile.domain.Models;
using UIKit;

namespace kimep.mobile.ios
{
    public partial class GPACell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("GPACell");
        public static readonly UINib Nib;

        static GPACell()
        {
            Nib = UINib.FromName("GPACell", NSBundle.MainBundle);
        }
        internal void UpdateCell(GPACRS gpa)
        {
            GPALabel.Text = gpa.GPA.ToString("N2");
            TakenCreditsLabel.Text = gpa.CreditsTaken.ToString("N2");
            EarnedCreditsLabel.Text = gpa.CreditsEarned.ToString("N2");
        }
        protected GPACell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
