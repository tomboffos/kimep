﻿using System;
using System.Linq;
using Foundation;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;
using UIKit;

namespace kimep.mobile.ios
{
    public partial class FinalExamsCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("FinalExamsCell");
        public static readonly UINib Nib;

        static FinalExamsCell()
        {
            Nib = UINib.FromName("FinalExamsCell", NSBundle.MainBundle);
        }
        internal void UpdateCell(FinalExam finalexam, NSDictionary[] alerts)
        {
            TimeZone tz = TimeZone.CurrentTimeZone;

            labelTime.Text = string.Format("{0:HH:mm}-{1:HH:mm}",
                                           tz.ToLocalTime(finalexam.Time_From),
                                           tz.ToLocalTime(finalexam.Time_To));

            labelHall.Text = finalexam.Hall;
            labelTitle.Text = finalexam.TitleCourses;

            if (alerts.Any(item =>
                           item["CourseID"].ToString() == finalexam.CourseID &&
                           item["Date"].ToString() == finalexam.Date.DateTimeToNSDate().ToString() &&
                           item["Time_From"].ToString() == finalexam.Time_From.DateTimeToNSDate().ToString()))
            {
                imageAlert.Alpha = 1F;
            }
            else
            {
                imageAlert.Alpha = .05F;
            }
        }
        protected FinalExamsCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
