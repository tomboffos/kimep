﻿using System;
using System.Linq;
using UIKit;

namespace kimep.mobile.ios
{
    public class PickerEventArgs : EventArgs
    {
        public string Selected { get; set; }
    }
    public class PickerViewModel : UIPickerViewModel
    {
        private string[] _myItems;
        protected static int selectedIndex = 0;
        public event EventHandler OnPickerChanged;
        public PickerViewModel(string[] items)
        {
            _myItems = items;
        }
        public string SelectedItem
        {
            get { return _myItems[selectedIndex]; }
        }
        public override nint GetComponentCount(UIPickerView pickerView)
        {
            return 1;
        }
        public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
        {
            return _myItems.Count();
        }
        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            return _myItems[(int)row];
        }
        public override void Selected(UIPickerView pickerView, nint row, nint component)
        {
            PickerEventArgs args = new PickerEventArgs()
            {
                Selected = _myItems[(int)row]
            };
            OnPickerChanged(this, args);
        }
    }
}
