﻿using System;
using UIKit;
using CoreGraphics;

namespace kimep.mobile.ios.Common
{
    public class DownloadingOverlay : UIView
    {
        // control declarations
        public UIProgressView progressView;
        UILabel loadingLabel;
        UIButton cancelButton;
        public event EventHandler OnCancel;

        public DownloadingOverlay(CGRect frame) : base(frame)
        {
            // configurable bits
            BackgroundColor = UIColor.Black;
            Alpha = 0.75f;
            AutoresizingMask = UIViewAutoresizing.All;

            nfloat labelHeight = 22;
            nfloat labelWidth = Frame.Width - 20;

            nfloat buttonHeight = 30;
            nfloat buttonWidth = 100;

            // derive the center x and y
            nfloat centerX = Frame.Width / 2;
            nfloat centerY = Frame.Height / 2;

            progressView = new UIProgressView(UIProgressViewStyle.Bar);
			progressView.Frame = new CGRect(
			    centerX - (progressView.Frame.Width / 2),
			    centerY - progressView.Frame.Height - 40,
			    progressView.Frame.Width,
			    progressView.Frame.Height);
			progressView.AutoresizingMask = UIViewAutoresizing.All;
			AddSubview(progressView);

			// create and configure the "Loading Data" label
			loadingLabel = new UILabel(new CGRect(
                centerX - (labelWidth / 2),
                centerY,
                labelWidth,
                labelHeight
                ));
            loadingLabel.BackgroundColor = UIColor.Clear;
            loadingLabel.TextColor = UIColor.White;
            loadingLabel.Text = "Downloading File...";
            loadingLabel.TextAlignment = UITextAlignment.Center;
            loadingLabel.AutoresizingMask = UIViewAutoresizing.All;
            AddSubview(loadingLabel);

            // create and configure the Cancel button
            cancelButton = new UIButton(new CGRect(
                centerX - (buttonWidth / 2),
                centerY + 40,
                buttonWidth,
                buttonHeight
                ));
            cancelButton.TouchUpInside += (sender, ea) => {
                OnCancel(sender, ea);
            };
            cancelButton.BackgroundColor = UIColor.White;
            cancelButton.SetTitleColor(UIColor.Black, UIControlState.Normal);
            cancelButton.SetTitle("Cancel", UIControlState.Normal);
            AddSubview(cancelButton);
        }

        /// <summary>
        /// Fades out the control and then removes it from the super view
        /// </summary>
        public void Hide()
        {
            UIView.Animate(
                0.5, // duration
                () => { Alpha = 0; },
                () => { RemoveFromSuperview(); }
            );
        }
    }
}