﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;

namespace kimep.mobile.ios.Common
{
    public class GifOverlay : UIView
    {
        // control declarations
        UIImageView activitySpinner;

        public GifOverlay(CGRect frame) : base(frame)
        {
            BackgroundColor = UIColor.White;
                //UIColor.FromRGB(38, 38, 38);
            Alpha = 0.7f;
            AutoresizingMask = UIViewAutoresizing.All;
            // derive the center x and y
            nfloat centerX = Frame.Width / 2;
            nfloat centerY = Frame.Height / 2;            
            activitySpinner = AnimatedImageView.GetAnimatedImageView(new NSUrl("Images/loading.gif", false), new UIImageView());
            activitySpinner.Frame = new CGRect(centerX - 50, centerY - 50, 100, 100);
            activitySpinner.AutoresizingMask = UIViewAutoresizing.All;
            activitySpinner.BackgroundColor = UIColor.Clear;
            AddSubview(activitySpinner);            
        }
        public void Hide()
        {
            Animate(0.5,
                () =>
                {
                    Alpha = 0;
                },
                () =>
                {
                    RemoveFromSuperview();
                });
        }
    }
}