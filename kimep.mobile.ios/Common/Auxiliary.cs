﻿using System.Linq;
using Foundation;
using UIKit;
using CoreGraphics;
using System.Drawing;
using System;
using kimep.mobile.domain.Common;
using kimep.mobile.domain.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace kimep.mobile.ios.Common
{
    public interface IAuxiliary
    {
        string AlertSpan(NSDictionary dic);
        NSDictionary[] Alerts(string Category);
        void CancelAlerts(NSDictionary dic);
        UIImage GetColoredImage(string imageName, UIColor color);
        UIImage ProfileImage();
        List<DepartmentModel> Contacts();
        List<FinalExam> FinalExams();
        List<AssessmentScoreModel> AssessmentScores();
        GPACRS GPACRS();
        List<LectureModel> Lectures(string DirectoryPath);
        LectureFileInfo GetFileInfo(string FilePath);
        List<BorrowedBook> Borrowed_Books();
        T GetCache<T>(string key);
        PersonalModel Personal();
        acaCalendar Academic_Calendar(int year, int month);
        Ticket Ticket(Guid id);
        Ticket Login(int StudentID, string Password, string Token);
        TodayModel Today();
        List<FinalGrade> Final_grades();
        List<MyClass> Schedule();
        bool InFavorites(LectureModel model);
        bool AddFavorites(LectureModel model);
        bool RemoveFavorites(LectureModel model);
        void Toast(String message, float duration = 3.0f);
        List<ShortNews> News();
        News ReadNews(int id);
    }
    public class Auxiliary : IAuxiliary
    {
        IRequests requests = new Requests();
        public void Toast(String message, float duration = 3.0f)
        {
            using (UIView view = UIApplication.SharedApplication.KeyWindow.RootViewController.View)
            {
                using (UIView residualView = view.ViewWithTag(1989))
                {
                    if (residualView != null)
                        residualView.RemoveFromSuperview();

                    var viewBack = new UIView(new CGRect(0, 0, 300, 100));
                    viewBack.BackgroundColor = UIColor.DarkGray;
                    viewBack.Tag = 1989;
                    using (UILabel lblMsg = new UILabel(new CGRect(20, 0, 260, 100)))
                    {
                        lblMsg.Lines = 0;
                        lblMsg.Text = message;
                        lblMsg.TextColor = UIColor.White;
                        lblMsg.TextAlignment = UITextAlignment.Center;
                        viewBack.Center = view.Center;
                        viewBack.AddSubview(lblMsg);
                        view.AddSubview(viewBack);
                        viewBack.Layer.CornerRadius = 20;
                        UIView.BeginAnimations("Toast");
                        UIView.SetAnimationDuration(duration);
                        viewBack.Alpha = 0.0f;
                        UIView.CommitAnimations();
                    }
                }
            }
        }
        public void SetCache(string value, string key)
        {
            var plist = new NSUserDefaults("group.kz.kimep.kimepmobile", NSUserDefaultsType.SuiteName);
            plist.SetString(value, key);
            plist.Synchronize();
        }
        public T GetCache<T>(string key)
        {
            var plist = new NSUserDefaults("group.kz.kimep.kimepmobile", NSUserDefaultsType.SuiteName);
            var obj = plist.StringForKey(key);
            //var obj = NSUserDefaults.StandardUserDefaults.StringForKey(key);
            if (obj == null)
                return default(T);
            return JsonConvert.DeserializeObject<T>(obj);
        }
        public bool InFavorites(LectureModel model)
        {
            List<LectureModel> fovors = GetCache<List<LectureModel>>("lecfavor") ?? new List<LectureModel>();
            return fovors.Any(f => f.FullName == model.FullName);
        }
        public bool AddFavorites(LectureModel model)
        {
            try
            {
                List<LectureModel> fovors = GetCache<List<LectureModel>>("lecfavor") ?? new List<LectureModel>();
                if (!fovors.Any(f => f.FullName == model.FullName))
                    fovors.Add(model);
                string serilized = JsonConvert.SerializeObject(fovors);
                SetCache(serilized, "lecfavor");
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool RemoveFavorites(LectureModel model)
        {
            try
            {
                List<LectureModel> fovors = GetCache<List<LectureModel>>("lecfavor") ?? new List<LectureModel>();
                LectureModel favor = fovors.FirstOrDefault(f => f.FullName == model.FullName);
                if (favor != null)
                    fovors.Remove(favor);
                string serilized = JsonConvert.SerializeObject(fovors);
                SetCache(serilized, "lecfavor");
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<ShortNews> News()
        {
            try
            {
                string raw = requests.News();
                SetCache(raw, "short_news");
                return JsonConvert.DeserializeObject<List<ShortNews>>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                     NSBundle.MainBundle.LocalizedString(
                         "Unable to load data. Please check your network connection and try again.",
                         "Unable to load data. Please check your network connection and try again."));
            }
        }
        public News ReadNews(int id)
        {
            try
            {
                string raw = requests.ReadNews(id);
                SetCache(raw, string.Format("news_{0}", id));
                return JsonConvert.DeserializeObject<News>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                     NSBundle.MainBundle.LocalizedString(
                         "Unable to load data. Please check your network connection and try again.",
                         "Unable to load data. Please check your network connection and try again."));
            }
        }
        public Ticket Login(int StudentID, string Password, string Token)
        {
            try
            {
                string raw = requests.Login(StudentID, Password, Token);
                if (raw != null)
                {
                    SetCache(raw, "ticket");
                    return JsonConvert.DeserializeObject<Ticket>(raw);
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException(
                     NSBundle.MainBundle.LocalizedString(
                         "Unable to load data. Please check your network connection and try again.",
                         "Unable to load data. Please check your network connection and try again."));
            }
            throw new System.ArgumentException(
                 NSBundle.MainBundle.LocalizedString(
                     "Please check your ID and password and try again.",
                     "Please check your ID and password and try again."));
        }
        public Ticket Ticket(Guid id)
        {
            try
            {
                string raw = requests.Ticket(id);
                SetCache(raw, "ticket");
                return JsonConvert.DeserializeObject<Ticket>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                     NSBundle.MainBundle.LocalizedString(
                         "Unable to load data. Please check your network connection and try again.",
                         "Unable to load data. Please check your network connection and try again."));
            }
        }
        public acaCalendar Academic_Calendar(int year, int month)
        {
            try
            {
                string raw = requests.Academic_Calendar(year, month);
                SetCache(raw, string.Format("academic_calendar_{0}_{1}", year, month));
                return JsonConvert.DeserializeObject<acaCalendar>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                     NSBundle.MainBundle.LocalizedString(
                         "Unable to load data. Please check your network connection and try again.",
                         "Unable to load data. Please check your network connection and try again."));
            }
        }
        public PersonalModel Personal()
        {
            try
            {
                string raw = requests.Personal();
                SetCache(raw, "personal");
                return JsonConvert.DeserializeObject<PersonalModel>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                     NSBundle.MainBundle.LocalizedString(
                         "Unable to load data. Please check your network connection and try again.",
                         "Unable to load data. Please check your network connection and try again."));
            }            
        }
        public List<LectureModel> Lectures(string DirectoryPath)
        {
            try
            {
                string raw = requests.Lectures(DirectoryPath);
                SetCache(raw, DirectoryPath);
                List<LectureModel> lectures = JsonConvert.DeserializeObject<List<LectureModel>>(raw);
                List<LectureModel> favors = GetCache<List<LectureModel>>("lecfavor") ?? new List<LectureModel>();
                lectures.ForEach(lec => lec.IsFavorite = favors.Any(f => f.FullName == lec.FullName));
                return lectures;
            }
            catch
            {
                throw new System.ArgumentException(
                     NSBundle.MainBundle.LocalizedString(
                         "Unable to load data. Please check your network connection and try again.",
                         "Unable to load data. Please check your network connection and try again."));
            }
        }
        public LectureFileInfo GetFileInfo(string FilePath)
        {
            try
            {
                string raw = requests.GetFileInfo(FilePath);
                SetCache(raw, FilePath);
                return JsonConvert.DeserializeObject<LectureFileInfo>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }
        public List<MyClass> Schedule()
        {
            try
            {
                string raw = requests.Schedule();
                SetCache(raw, "my_classes");
                return JsonConvert.DeserializeObject<List<MyClass>>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }
        public List<BorrowedBook> Borrowed_Books()
        {
            try
            {
                string raw = requests.Borrowed_Books();
                SetCache(raw, "borrowed_books");
                return JsonConvert.DeserializeObject<List<BorrowedBook>>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }        
        public List<AssessmentScoreModel> AssessmentScores()
        {
            try
            {
                string raw = requests.AssessmentScores();
                SetCache(raw, "AssessmentScores");
                return JsonConvert.DeserializeObject<List<AssessmentScoreModel>>(raw);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }
        public List<FinalGrade> Final_grades()
        {
            try
            {
                string raw = requests.Final_grades();
                SetCache(raw, "final_grades");
                return JsonConvert.DeserializeObject<List<FinalGrade>>(raw);

            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }    
        public TodayModel Today()
        {
            try
            {
                string today = requests.Today();
                SetCache(today, string.Format("today_{0:yyyy-MM-dd}", DateTime.Today));
                return JsonConvert.DeserializeObject<TodayModel>(today);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }
        public GPACRS GPACRS()
        {
            try
            {
                string gpacrs = requests.GPACRS();
                SetCache(gpacrs, "gpacrs");
                return JsonConvert.DeserializeObject<GPACRS>(gpacrs);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }
        public List<FinalExam> FinalExams()
        {
            try
            {
                string exams = requests.FinalExams();
                SetCache(exams, "finalexams");
                return JsonConvert.DeserializeObject<List<FinalExam>>(exams);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }
        }
        public List<DepartmentModel> Contacts()
        {
            try
            {
                string contacts = requests.Contacts();
                SetCache(contacts, "contacts");
                return JsonConvert.DeserializeObject<List<DepartmentModel>>(contacts);
            }
            catch
            {
                throw new System.ArgumentException(
                    NSBundle.MainBundle.LocalizedString(
                        "Unable to load data. Please check your network connection and try again.",
                        "Unable to load data. Please check your network connection and try again."));
            }            
        }
        public UIImage ProfileImage()
        {
            string uri = string.Format("https://www.kimep.kz/ext/mobile/avatar/thumb/{0}?width={1}&height={2}", Settings.Ticket.id, 600, 600);
            using (var url = new NSUrl(uri))
            {
                using (var data = NSData.FromUrl(url))
                {
                    try
                    {
                        UIImage avatar = UIImage.LoadFromData(data);
                        var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        string jpgFilename = System.IO.Path.Combine(documentsDirectory, "Avatar.PNG");
                        NSData imgData = avatar.AsPNG();
                        NSError err = null;

                        if (!imgData.Save(jpgFilename, false, out err))
                            throw new System.ArgumentException(err.LocalizedDescription);

                        return avatar;
                    }
                    catch
                    {
                        var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        var avatarPath = Path.Combine(documentsDirectory, "Avatar.PNG");

                        if (File.Exists(System.IO.Path.Combine(documentsDirectory, "Avatar.PNG")))
                            return UIImage.FromFile(avatarPath);
                        else
                            return UIImage.FromBundle("Profile");

                        //throw new System.ArgumentException(
                        //    NSBundle.MainBundle.LocalizedString(
                        //        "Unable to load data. Please check your network connection and try again.",
                        //        "Unable to load data. Please check your network connection and try again."));
                    }
                }
            }
        }
        public UIImage GetColoredImage(string imageName, UIColor color)
        {
            UIImage image = UIImage.FromBundle(imageName);

            if (image == null) return null;

            UIImage coloredImage = null;

            UIGraphics.BeginImageContext(image.Size);
            using (CGContext context = UIGraphics.GetCurrentContext())
            {

                context.TranslateCTM(0, image.Size.Height);
                context.ScaleCTM(1.0f, -1.0f);

                var rect = new RectangleF(0, 0, (float)image.Size.Width, (float)image.Size.Height);

                // draw image, (to get transparancy mask)
                context.SetBlendMode(CGBlendMode.Normal);
                context.DrawImage(rect, image.CGImage);

                // draw the color using the sourcein blend mode so its only draw on the non-transparent pixels
                context.SetBlendMode(CGBlendMode.SourceIn);
                context.SetFillColor(color.CGColor);
                context.FillRect(rect);

                coloredImage = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();
            }
            return coloredImage;
        }
        public NSDictionary[] Alerts(string Category)
        {
            var array = UIApplication.SharedApplication.ScheduledLocalNotifications;
            return array.Where(a => a.Category == Category).Select(a => a.UserInfo).ToArray();
        }
        public void CancelAlerts(NSDictionary dic)
        {
            var array = UIApplication.SharedApplication.ScheduledLocalNotifications;
            foreach (var item in array)
            {
                if (dic == null || Compare(item.UserInfo, dic))
                {
                    UIApplication.SharedApplication.CancelLocalNotification(item);
                }
            }
        }
        public bool Compare(NSDictionary item, NSDictionary dic)
        {
            if (item.ContainsKey(new NSString("CourseID")) &&
                item.ContainsKey(new NSString("Semester")) &&
                item.ContainsKey(new NSString("WeekDay")) &&
                item.ContainsKey(new NSString("Time_From")) &&
                dic.ContainsKey(new NSString("CourseID")) &&
                dic.ContainsKey(new NSString("Semester")) &&
                dic.ContainsKey(new NSString("WeekDay")) &&
                dic.ContainsKey(new NSString("Time_From")) &&
                item["CourseID"].ToString() == dic["CourseID"].ToString() &&
                item["Semester"].ToString() == dic["Semester"].ToString() &&
                item["WeekDay"].ToString() == dic["WeekDay"].ToString() &&
                item["Time_From"].ToString() == dic["Time_From"].ToString())
            {
                return true;
            }
            else if (item.ContainsKey(new NSString("CourseID")) &&
                item.ContainsKey(new NSString("Date")) &&
                item.ContainsKey(new NSString("Time_From")) &&
                dic.ContainsKey(new NSString("CourseID")) &&
                dic.ContainsKey(new NSString("Date")) &&
                dic.ContainsKey(new NSString("Time_From")) &&
                item["CourseID"].ToString() == dic["CourseID"].ToString() &&
                item["Date"].ToString() == dic["Date"].ToString() &&
                item["Time_From"].ToString() == dic["Time_From"].ToString())
            {
                return true;
            }
            return false;
        }
        public string AlertSpan(NSDictionary dic)
        {
            var array = UIApplication.SharedApplication.ScheduledLocalNotifications;

            var first = array.FirstOrDefault(item => Compare(item.UserInfo, dic));

            if (first == null)
            {
                return "None";
            }

            NSObject alertSpan = first.UserInfo["AlertSpan"];

            if (alertSpan == null)
                return "None";

            return alertSpan.ToString();
        }
    }
}