﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using System.Drawing;

namespace kimep.mobile.ios.Common
{
    public static class Extantions
    {       
        public static DateTime? NSDateToDateTime(this NSDate date)
        {
            if (date == null)
            {
                return null;
            }
            else
            {
                DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));
                return (DateTime?)reference.AddSeconds(date.SecondsSinceReferenceDate);
            }
        }
        public static NSDate DateTimeToNSDate(this Nullable<DateTime> date)
        {
            if (date == null)
            {
                return null;
            }
            else
            {
                return (date.GetValueOrDefault()).DateTimeToNSDate();
            }
        }
        public static NSDate DateTimeToNSDate(this DateTime date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
                    new DateTime(2001, 1, 1, 0, 0, 0));
            return NSDate.FromTimeIntervalSinceReferenceDate(
                    (date - reference).TotalSeconds);
        }
    }
}
