﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace kimep.mobile.ios
{
    [Register ("ExamDetailsController")]
    partial class ExamDetailsController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelHall { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView pickerAlert { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (labelDate != null) {
                labelDate.Dispose ();
                labelDate = null;
            }

            if (labelHall != null) {
                labelHall.Dispose ();
                labelHall = null;
            }

            if (pickerAlert != null) {
                pickerAlert.Dispose ();
                pickerAlert = null;
            }
        }
    }
}