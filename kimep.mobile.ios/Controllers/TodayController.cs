﻿using System;
using System.Collections.Generic;
using UIKit;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;

namespace kimep.mobile.ios.Controllers
{
    public class TodayController : ContentController
    {
        IAuxiliary aux = new Auxiliary();
        public TodayController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ConfigureView();
        }
        void ConfigureView()
        {
            TableView.AllowsSelection = true;
            Title = DateTime.Today.ToLongDateString();
            TodayModel today;
            try
            {
                today = aux.Today();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                today = aux.GetCache<TodayModel>(string.Format("today_{0:yyyy-MM-dd}",
                                  DateTime.Today)) ?? new TodayModel()
                                  {
                                      Classes = new List<MyClass>(),
                                      Events = new List<acaEvent>()
                                  };
                Title = string.Format("{0} (Offline)", Title);
            }
            TableView.Source = new TodayDataSource(today, this);
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 40f;
        }
        private void Refresh(object sender, EventArgs args)
        {
            ConfigureView();
        }
    }
}