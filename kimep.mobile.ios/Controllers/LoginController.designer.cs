﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace kimep.mobile.ios.Controllers
{
    [Register ("LoginController")]
    partial class LoginController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton buttonLogin { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelLogin { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField textfieldPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField textfieldStudentID { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewFrame { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewFramePassword { get; set; }

        [Action ("ButtonLogin_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ButtonLogin_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (buttonLogin != null) {
                buttonLogin.Dispose ();
                buttonLogin = null;
            }

            if (labelLogin != null) {
                labelLogin.Dispose ();
                labelLogin = null;
            }

            if (textfieldPassword != null) {
                textfieldPassword.Dispose ();
                textfieldPassword = null;
            }

            if (textfieldStudentID != null) {
                textfieldStudentID.Dispose ();
                textfieldStudentID = null;
            }

            if (viewFrame != null) {
                viewFrame.Dispose ();
                viewFrame = null;
            }

            if (viewFramePassword != null) {
                viewFramePassword.Dispose ();
                viewFramePassword = null;
            }
        }
    }
}