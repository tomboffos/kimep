﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using kimep.mobile.ios.Common;
using kimep.mobile.ios.DataSources;
using System.Threading.Tasks;
using kimep.mobile.domain.Common;

namespace kimep.mobile.ios.Controllers
{
    public class MenuController : UITableViewController
    {
        private RootController _root;
        private IRequests requests = new Requests();
        public MenuController(RootController root)
        {
            _root = root;
        }
        public override void ViewDidLoad()
        {
            TableView = new UITableView(View.Bounds, UITableViewStyle.Grouped);
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            
            TableView.AllowsMultipleSelection = false;
            TableView.BackgroundColor = UIColor.FromRGB(71, 57, 131);            
            TableView.RegisterNibForCellReuse(UINib.FromName("MenuCell", null), "MenuCell");
            TableView.Source = new MenuDataSource(_root);            
            TableView.ScrollEnabled = false;

            base.ViewDidLoad();
        }
        public override UIStatusBarStyle PreferredStatusBarStyle()
        {
            return UIStatusBarStyle.LightContent;
        }

    }
}