﻿using CoreGraphics;
using Foundation;
using kimep.mobile.domain.Common;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;
using System;
using System.Threading.Tasks;
using UIKit;

namespace kimep.mobile.ios.Controllers
{
    public partial class LoginController : UIViewController
    {
        private IAuxiliary aux = new Auxiliary();
        public LoginController() : base("LoginController", null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            labelLogin.Text = NSBundle.MainBundle.LocalizedString("Please enter ID and Password", "Please enter ID and Password");

            viewFrame.Layer.BorderColor = UIColor.FromRGB(209, 210, 212).CGColor;
            viewFrame.Layer.BorderWidth = 1.5f;
            viewFrame.Layer.CornerRadius = 7;
            UIImageView leftView = new UIImageView();
            leftView.Frame = new CGRect(0, 0, 40, 25);            
            leftView.Image = UIImage.FromBundle("user");         
            textfieldStudentID.LeftView = leftView;
            textfieldStudentID.LeftViewMode = UITextFieldViewMode.Always;
            textfieldStudentID.Placeholder = NSBundle.MainBundle.LocalizedString("ID", "ID");

            viewFramePassword.Layer.BorderColor = UIColor.FromRGB(209, 210, 212).CGColor;
            viewFramePassword.Layer.BorderWidth = 1.5f;
            viewFramePassword.Layer.CornerRadius = 7;
            UIImageView leftViewPassword = new UIImageView();
            leftViewPassword.Frame = new CGRect(0, 0, 40, 25);
            leftViewPassword.Image = UIImage.FromBundle("password");
            textfieldPassword.LeftView = leftViewPassword;
            textfieldPassword.LeftViewMode = UITextFieldViewMode.Always;
            textfieldPassword.Placeholder = NSBundle.MainBundle.LocalizedString("Password", "Password");

            buttonLogin.BackgroundColor = UIColor.FromRGB(71, 57, 131);
            buttonLogin.Layer.CornerRadius = 7;
            buttonLogin.SetTitle(NSBundle.MainBundle.LocalizedString("Login", "Login"), UIControlState.Normal);
            buttonLogin.ContentEdgeInsets = new UIEdgeInsets(5, 0, 0, 0);
        }

        partial void ButtonLogin_TouchUpInside(UIButton sender)
        {
            if (textfieldPassword.Text != string.Empty && textfieldStudentID.Text != string.Empty)
            {
                int studentID = 0;
                if (int.TryParse(textfieldStudentID.Text, out studentID))
                {
                    var bounds = UIScreen.MainScreen.Bounds;
                    GifOverlay loadPop = new GifOverlay(bounds);
                    // Show loading progress            
                    View.Add(loadPop);

                    Task.Factory.StartNew(() =>
                    {
                        InvokeOnMainThread(() =>
                        {
                            try
                            {
                                AppDelegate appDelegate = (AppDelegate)UIApplication.SharedApplication.Delegate;
                                Settings.Ticket = aux.Login(studentID, textfieldPassword.Text, null);
                                appDelegate.Window = null;
                                appDelegate.Window = new UIWindow(UIScreen.MainScreen.Bounds);
                                appDelegate.Window.RootViewController = new TabController();
                                appDelegate.Window.MakeKeyAndVisible();
                            }
                            catch (Exception ex)
                            {
                                aux.Toast(ex.Message, 7);
                            }
                        });
                    }).ContinueWith(task =>
                    {
                        loadPop.Hide();
                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }                
            }            
        }
    }
}