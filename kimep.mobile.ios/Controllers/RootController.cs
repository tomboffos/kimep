using CoreGraphics;
using Foundation;
using kimep.mobile.domain.Common;
using kimep.mobile.ios.Common;
using kimep.mobile.ios.Controllers;
using System;
using System.Drawing;
using UIKit;

namespace kimep.mobile.ios
{
	public class RootController : RESideMenuViewController
    {
        IAuxiliary aux = new Auxiliary();
        public RootController()
        {
            var image = UIImage.FromFile("Images/menubtn.png").Scale(new CGSize(80, 40));
            //var image = aux.GetColoredImage("menu.png", UIColor.White);
            //var image = aux.GetColoredImage("Images/menubtnbg2.png", UIColor.White);
            var button = UIButton.FromType(UIButtonType.Custom);
            button.SetBackgroundImage(image, UIControlState.Normal);
            button.TouchDown += ShowMenu;
            button.Frame = new RectangleF(0, 0, (float)image.Size.Width, (float)image.Size.Height);

            ContentViewController = new UINavigationController(new TodayController()
            {   
                Root = this,
                NavigationItem = {
                    LeftBarButtonItem = new UIBarButtonItem(button)
                }                
            });

            MenuViewController = new MenuController(this);
            //BackgroundImage = UIImage.FromBundle("Images/MenuBackground.png");
            BackgroundImage = UIImage.FromBundle("Images/menubtnbg.png");
            IsPanGestureEnabled = true;
            ScaleMenuView = false;
            
            Settings.ProfileImage = aux.ProfileImage();
        }
        public void ShowMenu(object sender, EventArgs e)
		{
            PresentMenuViewController();
		}
    }
}

