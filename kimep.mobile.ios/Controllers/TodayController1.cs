﻿//using System;
//using System.Collections.Generic;
//using UIKit;
//using kimep.mobile.domain.Models;
//using kimep.mobile.ios.Common;
//using kimep.mobile.domain.Common;
//using System.Threading.Tasks;
//using System.Drawing;
//using Foundation;

//namespace kimep.mobile.ios.Controllers
//{
//    public class TodayController : UIViewController
//    {
//        IRequests requests = new Requests();
//        IAuxiliary aux = new Auxiliary();
//        public UITableView TableView { get; set; }

//        public EventHandler Refresh_Click;
//        public RootController Root;
//        bool useRefreshControl = false;
//        UIRefreshControl refreshControl;
        

//        public TodayController()
//        {
//            Refresh_Click += Refresh;
//        }

//        async Task RefreshAsync(object sender, EventArgs e)
//        {
//            // only activate the refresh control if the feature is available  
//            if (useRefreshControl)
//            {
//                refreshControl.BeginRefreshing();

//                var bounds = UIScreen.MainScreen.Bounds;
//                GifOverlay loadPop = new GifOverlay(bounds);
//                // Show loading progress
//                Root.View.Add(loadPop);

//                await Task.Factory.StartNew(() =>
//                {
//                    InvokeOnMainThread(() =>
//                    {
//                        Refresh_Click(sender, e);
//                    });
//                }).ContinueWith(task =>
//                {
//                    // Hide loading progress
//                    refreshControl.EndRefreshing();
//                    loadPop.Hide();
//                }, TaskScheduler.FromCurrentSynchronizationContext());
//            }
//        }
//        #region * iOS Specific Code  
//        // This method will add the UIRefreshControl to the table view if  
//        // it is available, ie, we are running on iOS 6+  
//        void AddRefreshControl()
//        {
//            if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
//            {
//                // the refresh control is available, let's add it  
//                refreshControl.ValueChanged += async (sender, e) =>
//                {
//                    await RefreshAsync(sender, e);
//                };
//                useRefreshControl = true;
//            }
//        }
//        #endregion
//        public override void ViewWillAppear(bool animated)
//        {
//            base.ViewWillAppear(animated);

//            UINavigationBar navBar = NavigationController.NavigationBar;
//            navBar.Translucent = false;
//            UIImage orgImage = UIImage.FromFile("Images/menubtnbg.png");
//            nfloat height = navBar.Frame.Height;
//            nfloat width = orgImage.Size.Width * height / orgImage.Size.Height;
//            UIImageView imageView = new UIImageView(orgImage);
//            imageView.ContentMode = UIViewContentMode.ScaleToFill;
//            imageView.Frame = new RectangleF(0, 0, (float)width, (float)height);
//            navBar.InsertSubview(imageView, 1);
//            navBar.BarTintColor = UIColor.FromRGB(210, 171, 103);
//            navBar.TitleTextAttributes = new UIStringAttributes()
//            {
//                Font = UIFont.FromName("Bebas Neue Cyrillic", 23f),
//                BaselineOffset = -2,
//                ForegroundColor = UIColor.FromRGB(0x47, 0x39, 0x83)
//            };
//        }
//        public override void ViewDidLoad()
//        {
//            base.ViewDidLoad();

//            var image = UIImage.FromBundle("menubtn");
//            var button = UIButton.FromType(UIButtonType.Custom);
//            button.SetBackgroundImage(image, UIControlState.Normal);
//            button.TouchDown += Root.ShowMenu;
//            button.Frame = new RectangleF(0, 0, (float)image.Size.Width, (float)image.Size.Height);

//            NavigationItem.LeftBarButtonItem = new UIBarButtonItem(button);

//            //UIView statusBar = UIApplication.SharedApplication.ValueForKey(new NSString("statusBar")) as UIView;
//            UIView statusBar = new UIView (UIApplication.SharedApplication.KeyWindow.WindowScene.StatusBarManager.StatusBarFrame);
//            if (statusBar.RespondsToSelector(new ObjCRuntime.Selector("setBackgroundColor:")))
//            {
//                statusBar.BackgroundColor = UIColor.LightTextColor;
//            }

//            refreshControl = new UIRefreshControl();
//            refreshControl.TintColor = UIColor.Clear;
//            TableView = new UITableView(View.Bounds, UITableViewStyle.Plain);
//            //await RefreshAsync(this, new EventArgs());
//            AddRefreshControl();
//            TableView.Add(refreshControl);
//            View.AddSubview(TableView);

//            ConfigureView();
//        }
    

//        void ConfigureView()
//        {            
//            TableView.AllowsSelection = true;
//            Title = DateTime.Today.ToLongDateString();
//            TodayModel today;
//            try
//            {
//                today = aux.Today();
//            }
//            catch (Exception ex)
//            {
//                aux.Toast(ex.Message);
//                today = aux.GetCache<TodayModel>(string.Format("today_{0:yyyy-MM-dd}",
//                                  DateTime.Today)) ?? new TodayModel()
//                                  {
//                                      Classes = new List<MyClass>(),
//                                      Events = new List<acaEvent>()
//                                  };
//                Title = string.Format("{0} (Offline)", Title);
//            }
//            TableView.Source = new TodayDataSource(today, this);
//            TableView.RowHeight = UITableView.AutomaticDimension;
//            TableView.EstimatedRowHeight = 40f;
//        }
//        private void Refresh(object sender, EventArgs args)
//        {
//            ConfigureView();
//        }
//    }
//}