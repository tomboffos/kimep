﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace kimep.mobile.ios
{
    [Register ("ClassDetailsController")]
    partial class ClassDetailsController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelTimeFrom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelWeekDay { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView pickerAlert { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel textInstructor { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (labelTimeFrom != null) {
                labelTimeFrom.Dispose ();
                labelTimeFrom = null;
            }

            if (labelWeekDay != null) {
                labelWeekDay.Dispose ();
                labelWeekDay = null;
            }

            if (pickerAlert != null) {
                pickerAlert.Dispose ();
                pickerAlert = null;
            }

            if (textInstructor != null) {
                textInstructor.Dispose ();
                textInstructor = null;
            }
        }
    }
}