﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using kimep.mobile.domain.Common;
using kimep.mobile.ios.Common;
using CoreGraphics;
using CoreAnimation;

namespace kimep.mobile.ios.Controllers
{
    public class TabController : UITabBarController
    {
        UIViewController tab0, tab1, tab2;
        IRequests requests = new Requests();
        IAuxiliary aux = new Auxiliary();
        public TabController()
        {
            nfloat heightTabBar = TabBar.Frame.Height;
            nfloat widthTabBar = TabBar.Frame.Width;

            tab0 = new UINavigationController(new NewsController(this));
            tab0.TabBarItem = new UITabBarItem();
            tab0.TabBarItem.SetFinishedImages(
                UIImage.FromBundle("news_active").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal),
                UIImage.FromBundle("news_inactive").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal));
            tab0.TabBarItem.ImageInsets = new UIEdgeInsets(7, 0, -7, 0);
            tab0.TabBarItem.TitlePositionAdjustment = new UIOffset(0, 17);
            tab0.Title = NSBundle.MainBundle.LocalizedString("News", "News");
            tab0.TabBarItem.SetTitleTextAttributes(new UITextAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 12f),
                TextColor = UIColor.White
            }, UIControlState.Normal);
            tab0.TabBarItem.SetTitleTextAttributes(new UITextAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 12f),
                TextColor = UIColor.FromRGB(0xD2, 0xAB, 0x67)
            }, UIControlState.Selected);

            tab1 = new RootController();
            tab1.TabBarItem = new UITabBarItem();            
            tab1.TabBarItem.SetFinishedImages(
                UIImage.FromBundle("main_active").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal),
                UIImage.FromBundle("main_inactive").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal));
            tab1.TabBarItem.ImageInsets = new UIEdgeInsets(-1, 0, 1, 0);
            tab1.TabBarItem.TitlePositionAdjustment = new UIOffset(0, 17);
            tab1.Title = NSBundle.MainBundle.LocalizedString("Home", "Home");
            tab1.TabBarItem.SetTitleTextAttributes(new UITextAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 12f),
                TextColor = UIColor.White
            }, UIControlState.Normal);
            tab1.TabBarItem.SetTitleTextAttributes(new UITextAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 12f),
                TextColor = UIColor.FromRGB(0xD2, 0xAB, 0x67)
            }, UIControlState.Selected);


            tab2 = new UINavigationController(new ProfileController());
            tab2.TabBarItem = new UITabBarItem();
            tab2.TabBarItem.SetFinishedImages(
                UIImage.FromBundle("profile_active").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal),
                UIImage.FromBundle("profile_inactive").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal));
            tab2.TabBarItem.ImageInsets = new UIEdgeInsets(7, 0, -7, 0);
            tab2.TabBarItem.TitlePositionAdjustment = new UIOffset(0, 17);
            tab2.Title = NSBundle.MainBundle.LocalizedString("Profile", "Profile");
            tab2.TabBarItem.SetTitleTextAttributes(new UITextAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 12f),
                TextColor = UIColor.White
            }, UIControlState.Normal);
            tab2.TabBarItem.SetTitleTextAttributes(new UITextAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 12f),
                TextColor = UIColor.FromRGB(0xD2, 0xAB, 0x67)
            }, UIControlState.Selected);

            ViewControllers = new UIViewController[] 
            {
                tab0, tab1, tab2
            };
            
            SelectedIndex = 1;

            TabBar.BarTintColor = UIColor.FromRGB(71, 57, 131);
            TabBar.ShadowImage = new UIImage();

            CALayer topBorder = new CALayer();
            topBorder.Frame = new CGRect(0.0f, 0.0f, widthTabBar, 4f);
            UIImage imageShaddow = UIImage.FromFile("Images/bottom_bg_shadow.png").Scale(new CGSize(widthTabBar, 4f));
            topBorder.BackgroundColor = UIColor.FromPatternImage(imageShaddow).CGColor;
            TabBar.Layer.InsertSublayer(topBorder, 0);
        }
    }
}