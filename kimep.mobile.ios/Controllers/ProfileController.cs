﻿using CoreGraphics;
using Foundation;
using kimep.mobile.domain.Common;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;
using System;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UIKit;
using System.Collections.Generic;

namespace kimep.mobile.ios.Controllers
{
    public partial class ProfileController : UIViewController
    {
        private UIScrollView _scrollView;
        private UIView _contentView;
        private IAuxiliary aux = new Auxiliary();
        private IRequests requests = new Requests();
        // Create the image picker control
        private UIImagePickerController imagePicker = new UIImagePickerController();
        public ProfileController() : base("ProfileController", null)
        {
        }
        protected void Handle_FinishedPickingMedia(object sender, UIImagePickerMediaPickedEventArgs e)
        {
            GifOverlay loadPop = new GifOverlay(UIScreen.MainScreen.Bounds);
            // Show loading progress
            imagePicker.Add(loadPop);
            Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(() =>
                {
                    // determine what was selected, video or image
                    if (e.Info[UIImagePickerController.MediaType].ToString() == "public.image")
                    {
                        // Dismiss the picker
                        imagePicker.DismissModalViewController(true);
                        // get the original image
                        UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;
                        if (originalImage != null)
                        {
                            //One thing you might notice is when you take a picture and throw it into an image view, 
                            //it will be side ways, so we need to rotate it based it's orientation
                            UIImage rotateImage = RotateImage(originalImage, originalImage.Orientation);

                            using (NSData imageData = rotateImage.AsPNG())
                            {
                                Byte[] myByteArray = new Byte[imageData.Length];
                                Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
                                try
                                {
                                    int result = requests.Upload(myByteArray, "Avatar.PNG").Result;
                                    if (result > 0)
                                    {
                                        Settings.ProfileImage = aux.ProfileImage();
                                    }                                    
                                }
                                catch (Exception ex)
                                {
                                    aux.Toast(ex.Message);
                                }
                            }
                        }
                    }
                });
            }).ContinueWith(task =>
            {
                // Hide loading progress
                loadPop.Hide();
                btnAvatar.SetImage((UIImage)Settings.ProfileImage, UIControlState.Normal);
            }, TaskScheduler.FromCurrentSynchronizationContext());            
        }
        //Method that will take in a photo and rotate it based on the orientation that the image was taken in
        double radians(double degrees) { return degrees * Math.PI / 180; }
        private UIImage RotateImage(UIImage src, UIImageOrientation orientation)
        {
            UIGraphics.BeginImageContext(src.Size);

            if (orientation == UIImageOrientation.Right)
            {
                CGAffineTransform.MakeRotation((nfloat)radians(90));
            }
            else if (orientation == UIImageOrientation.Left)
            {
                CGAffineTransform.MakeRotation((nfloat)radians(-90));
            }
            else if (orientation == UIImageOrientation.Down)
            {
                // NOTHING
            }
            else if (orientation == UIImageOrientation.Up)
            {
                CGAffineTransform.MakeRotation((nfloat)radians(90));
            }

            src.Draw(new CGPoint(0, 0));
            UIImage image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return image;
        }
        // Implement the cancellation handler method so 
        // that the picker will disappear:
        void Handle_Canceled(object sender, EventArgs e)
        {
            imagePicker.DismissModalViewController(true);
        }
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }
        public void OpenCamera()
        {
            // Set the source and media type:
            if (UIImagePickerController.IsSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
            {
                imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;
                // Display the Image Picker controller
                NavigationController.PresentViewControllerAsync(imagePicker, true);
            }
        }
        public void OpenPhotoLibrary()
        {
            // Set the source and media type:
            if (UIImagePickerController.IsSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary))
            {
                imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                // Display the Image Picker controller
                NavigationController.PresentViewControllerAsync(imagePicker, true);
            }
        }
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            Title = NSBundle.MainBundle.LocalizedString("Profile", "Profile");

            UINavigationBar navBar = NavigationController.NavigationBar;            
            navBar.Translucent = false;
            navBar.BarTintColor = UIColor.FromRGB(210, 171, 103);
            navBar.TitleTextAttributes = new UIStringAttributes()
            {                
                Font = UIFont.FromName("Bebas Neue Cyrillic", 23f),
                BaselineOffset = -3,
                ForegroundColor = UIColor.FromRGB(0x47, 0x39, 0x83)
            };
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var image = UIImage.FromBundle("poweroff");
            var button = UIButton.FromType(UIButtonType.Custom);
            button.SetBackgroundImage(image, UIControlState.Normal);
            button.TouchDown += delegate
            {
                Settings.ProfileImage = null;
                Settings.Ticket = null;
                Settings.personalInfo = null;
                NSUserDefaults defs = new NSUserDefaults("group.kz.kimep.kimepmobile", NSUserDefaultsType.SuiteName);
                NSDictionary dict = defs.ToDictionary();
                foreach (var key in dict)
                {
                    defs.RemoveObject(key.Key.ToString());
                }
                defs.Synchronize();

                AppDelegate appDelegate = (AppDelegate)UIApplication.SharedApplication.Delegate;
                appDelegate.Window.RootViewController = new LoginController();
            };
            button.Frame = new RectangleF(0, 0, (float)image.Size.Width, (float)image.Size.Height);

            NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);


            btnAvatar.SetTitle(NSBundle.MainBundle.LocalizedString("Change", "Change"), UIControlState.Normal);

            // Assign delegate handler
            imagePicker.FinishedPickingMedia += Handle_FinishedPickingMedia;
            imagePicker.Canceled += Handle_Canceled;

            if (Settings.ProfileImage == null)
            {
                Settings.ProfileImage = aux.ProfileImage();
            }

            if (Settings.personalInfo == null)
            {
                try
                {
                    Settings.personalInfo = aux.Personal();
                }
                catch (Exception ex)
                {
                    aux.Toast(ex.Message);
                    Settings.personalInfo = aux.GetCache<PersonalModel>("personal") ??
                        new PersonalModel();
                }
            }

            labelFullName.Text = string.Format("{0} {1}", Settings.personalInfo.FirstName, Settings.personalInfo.LastName);
            labelStudentID.Text = Settings.personalInfo.StudentID.ToString();
            labelProgramID.Text = Settings.personalInfo.ProgramID;
            TimeZone tz = TimeZone.CurrentTimeZone;
            if (Settings.personalInfo.XRay.HasValue)
            {
                labelXRay.Text = tz.ToLocalTime(Settings.personalInfo.XRay.Value).ToShortDateString();
                if (Settings.personalInfo.XRay.Value.AddYears(1) < DateTime.Today)
                {
                    labelXRay.TextColor = UIColor.Red;
                }
            }
            else
            {
                labelXRay.Hidden = true;
            }

            btnAvatar.ImageView.ContentMode = UIViewContentMode.ScaleToFill;
            btnAvatar.SetImage((UIImage)Settings.ProfileImage, UIControlState.Normal);
            btnAvatar.ImageEdgeInsets = new UIEdgeInsets(0, 0, 20, 0);
            btnAvatar.TitleEdgeInsets = new UIEdgeInsets(0, -1000, 0, -400);

            GPACRS gpacrs;
            try
            {
                gpacrs = aux.GPACRS();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                gpacrs = aux.GetCache<GPACRS>("gpacrs");
            }
            labelGPA.Text = string.Format("{0:N2}", gpacrs.GPA);
            labelCreditsTaken.Text = string.Format("{0:N2}", gpacrs.CreditsTaken);
            labelCreditsEarned.Text = string.Format("{0:N2}", gpacrs.CreditsEarned);
            
            //#region * Scrolling View
            //_scrollView = new UIScrollView
            //{
            //    ShowsHorizontalScrollIndicator = false,
            //    TranslatesAutoresizingMaskIntoConstraints = false
            //};
            //_contentView = new UIView { TranslatesAutoresizingMaskIntoConstraints = false };
            //var subviews = View.Subviews.ToArray();
            //var constraints = View.Constraints
            //    .Where(constraint => constraint.SecondAttribute != NSLayoutAttribute.NoAttribute)
            //    .Select(constraint => NSLayoutConstraint.Create(
            //        constraint.FirstItem == View ? _contentView : constraint.FirstItem,
            //        constraint.FirstAttribute,
            //        constraint.Relation,
            //        constraint.SecondItem == View ? _contentView : constraint.SecondItem,
            //        constraint.SecondAttribute,
            //        constraint.Multiplier,
            //        constraint.Constant)).ToArray();
            //View.RemoveConstraints(View.Constraints);
            //Array.ForEach(subviews, view => view.RemoveFromSuperview());
            //_contentView.AddSubviews(subviews);
            //_scrollView.Add(_contentView);
            //_contentView.AddConstraints(constraints);
            //Add(_scrollView);

            //var edges = new[]
            //{
            //    NSLayoutAttribute.Top,
            //    NSLayoutAttribute.Bottom,
            //    NSLayoutAttribute.Left,
            //    NSLayoutAttribute.Right
            //};

            //View.AddConstraint(NSLayoutConstraint.Create(View, NSLayoutAttribute.Width, NSLayoutRelation.Equal,
            //    _contentView, NSLayoutAttribute.Width, 1, 0));
            //_scrollView.AddConstraints(
            //    edges.Select(
            //        edge => NSLayoutConstraint.Create(_scrollView, edge, NSLayoutRelation.Equal, _contentView, edge, 1, 0))
            //        .ToArray());
            //View.AddConstraints(
            //    edges.Select(
            //        edge => NSLayoutConstraint.Create(View, edge, NSLayoutRelation.Equal, _scrollView, edge, 1, 0))
            //        .ToArray());
            //#endregion
        }
        partial void BtnAvatar_TouchUpInside(UIButton sender)
        {
            var alert = UIAlertController.Create(
                NSBundle.MainBundle.LocalizedString("Profile photo", "Profile photo"),
                NSBundle.MainBundle.LocalizedString("Please select a source", "Please select a source"), 
                UIAlertControllerStyle.ActionSheet);

            alert.ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen;
            alert.ModalTransitionStyle = UIModalTransitionStyle.CoverVertical;            

            alert.AddAction(UIAlertAction.Create(
                NSBundle.MainBundle.LocalizedString("Camera", "Camera"), 
                UIAlertActionStyle.Default, action => OpenCamera()));
            alert.AddAction(UIAlertAction.Create(
                NSBundle.MainBundle.LocalizedString("Gallery", "Gallery"), 
                UIAlertActionStyle.Default, action => OpenPhotoLibrary()));

            alert.AddAction(UIAlertAction.Create(
                NSBundle.MainBundle.LocalizedString("Cancel", "Cancel"), 
                UIAlertActionStyle.Cancel, null));

            PresentViewController(alert, animated: true, completionHandler: null);
        }
    }
}