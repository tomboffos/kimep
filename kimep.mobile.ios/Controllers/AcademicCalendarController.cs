﻿using System;
using System.Collections.Generic;
using UIKit;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;

namespace kimep.mobile.ios.Controllers
{
    public class AcademicCalendarController : ContentController
    {
        IAuxiliary aux = new Auxiliary();
        acaCalendar academic_calendar;
        public AcademicCalendarController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            ConfigureView();
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
        }
        private UIBarButtonItem NavWard(DateTime newdate, bool IsBack)
        {
            UIBarButtonItem btn = new UIBarButtonItem(
            newdate.ToString("MMMM yyyy"),
            UIBarButtonItemStyle.Plain,
                (sender, args) =>
                {
                    try
                    {
                        Title = Title.Replace(" (Offline)", string.Empty);
                        academic_calendar = aux.Academic_Calendar(newdate.Year, newdate.Month);
                    }
                    catch (Exception ex)
                    {
                        aux.Toast(ex.Message);
                        academic_calendar = aux.GetCache<acaCalendar>(
                            string.Format("academic_calendar_{0}_{1}",
                                          newdate.Year, newdate.Month)) ??
                                                    new acaCalendar()
                                                    {
                                                        Events = new List<acaEvent>(),
                                                        Month = newdate.Month,
                                                        Year = newdate.Year
                                                    };
                        if (!Title.EndsWith("(Offline)"))
                            Title = string.Format("{0} (Offline)", Title);
                    }
                    TableView.Source = new AcademicCalendarDataSource(academic_calendar);
                    TableView.ReloadData();
                    SetBar();
                }
            );

            btn.SetTitleTextAttributes(new UITextAttributes() {
                TextColor = UIColor.FromRGB(71, 57, 131),
                Font = UIFont.FromName("Bebas Neue Cyrillic", 18f)
            }, UIControlState.Normal);

            btn.SetTitleTextAttributes(new UITextAttributes()
            {
                TextColor = UIColor.FromRGB(71, 57, 131),
                Font = UIFont.FromName("Bebas Neue Cyrillic", 18f)
            }, UIControlState.Selected);

            return btn;
        }
        void SetBar()
        {
            DateTime dt = new DateTime(academic_calendar.Year, academic_calendar.Month, 1);            
            SetToolbarItems(
                new UIBarButtonItem[] {
                            NavWard(dt.AddMonths(-1), true),
                            new UIBarButtonItem(
                                UIBarButtonSystemItem.FlexibleSpace)
                                {
                                    Width = 50
                                },
                            NavWard(dt.AddMonths(1), false)
            }, true);
        }
        void ConfigureView()
        {
            TableView.AllowsSelection = true;
            NavigationController.ToolbarHidden = false;
            TableView.RegisterNibForCellReuse(UINib.FromName("AcademicCalendarCell", null), "Cell");
            try
            {
                Title = Title.Replace(" (Offline)", string.Empty);
                academic_calendar = aux.Academic_Calendar(DateTime.Today.Year, DateTime.Today.Month);
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                academic_calendar = aux.GetCache<acaCalendar>(
                    string.Format("academic_calendar_{0}_{1}",
                                  DateTime.Today.Year, DateTime.Today.Month)) ??
                                            new acaCalendar()
                                            {
                                                Events = new List<acaEvent>(),
                                                Month = DateTime.Today.Month,
                                                Year = DateTime.Today.Year
                                            };
                if (!Title.EndsWith("(Offline)"))
                    Title = string.Format("{0} (Offline)", Title);
            }

            TableView.Source = new AcademicCalendarDataSource(academic_calendar);
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 40f;
            SetBar();
        }
        private void Refresh(object sender, EventArgs args)
        {
            ConfigureView();
        }
    }
}