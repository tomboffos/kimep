﻿using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using UIKit;
using kimep.mobile.domain.Common;

namespace kimep.mobile.ios.Controllers
{
    public class MyClassesController : ContentController
    {
        IAuxiliary aux = new Auxiliary();
        public MyClassesController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            ConfigureView();
        }
        void ConfigureView()
        {
            TableView.AllowsSelection = true;
            TableView.RegisterNibForCellReuse(UINib.FromName("MyClassesCell", null), "Cell");
            List<MyClass> schedule;
            try
            {
                Title = Title.Replace(" (Offline)", string.Empty);
                schedule = aux.Schedule();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                schedule = aux.GetCache<List<MyClass>>("my_classes") ?? new List<MyClass>();
                if (!Title.EndsWith("(Offline)"))
                    Title = string.Format("{0} (Offline)", Title);
            }
            TableView.Source = new MyClassesDataSource(schedule, this);
            //TableView.RowHeight = UITableView.AutomaticDimension;
            //TableView.EstimatedRowHeight = 40f;
        }
        private void Refresh(object sender, EventArgs args)
        {
            ConfigureView();
        }
    }
}