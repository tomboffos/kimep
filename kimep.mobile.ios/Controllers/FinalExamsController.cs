﻿using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using UIKit;

namespace kimep.mobile.ios.Controllers
{
    public class FinalExamsController : ContentController
    {
        IAuxiliary aux = new Auxiliary();
        public FinalExamsController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            ConfigureView();
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            //ConfigureView();
            aux.Toast("Dear student, please, don’t forget to RE-CHECK your Personal Final Exams schedule on-line BEFORE the exams due to possible changes in the schedule!", 
                7f);
        }
        void ConfigureView()
        {
            TableView.AllowsSelection = true;
            TableView.RegisterNibForCellReuse(UINib.FromName("FinalExamsCell", null), "Cell");
            List<FinalExam> finalexams;
            try
            {
                Title = Title.Replace(" (Offline)", string.Empty);
                finalexams = aux.FinalExams();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                finalexams = aux.GetCache<List<FinalExam>>("finalexams") ??
                                   new List<FinalExam>();
                if (!Title.EndsWith("(Offline)"))
                    Title = string.Format("{0} (Offline)", Title);
            }

            TableView.Source = new FinalExamsDataSource(finalexams, this);
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 40f;
        }
        private void Refresh(object sender, EventArgs args)
        {
            ConfigureView();
        }
    }
}