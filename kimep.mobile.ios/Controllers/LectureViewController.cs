﻿using kimep.mobile.domain.Common;
using kimep.mobile.ios.Common;
using System;
using System.Linq;
using System.Collections.Generic;
using UIKit;
using Foundation;
using kimep.mobile.domain.Models;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using MediaPlayer;
using CoreGraphics;
using System.Drawing;
using CoreAnimation;

namespace kimep.mobile.ios.Controllers
{
    public class LectureViewController : ContentController
    {
        private String path = @"\\l-drive\lecture";
        private String parent = @"\\l-drive\lecture";
        public LectureViewController()
        {
            Refresh_Click += Refresh;
        }
        public void DataBind()
        {
            var bounds = UIScreen.MainScreen.Bounds;
            GifOverlay loadPop = new GifOverlay(bounds);
            // Show loading progress
            Root.View.Add(loadPop);

            Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(() =>
                {
                    TableView.Source = new LectureTableViewSource(this);
                });
            }).ContinueWith(task =>
            {
                TableView.ReloadData();
                TableView.RowHeight = UITableView.AutomaticDimension;
                TableView.EstimatedRowHeight = 40f;
                // Hide loading progress
                loadPop.Hide();
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            TableView.RegisterNibForCellReuse(UINib.FromName("LectureCell", null), "Cell");
            DataBind();            
        }
        private void Refresh(object sender, EventArgs args)
        {
            DataBind();
        }
        private class LectureTableViewSource : UITableViewSource
        {
            private LectureViewController _controller;
            private List<LectureModel> lectures;
            private IRequests req = new Requests();
            private IAuxiliary aux = new Auxiliary();
            private bool IsDownloadCanceled = false;
            private CGSize sectionHeaderSize;
            private UIFont sectionHeaderFont = UIFont.FromName("Bebas Neue Cyrillic", 21f);
            public LectureTableViewSource(LectureViewController controller)
            {
                _controller = controller;

                if (_controller.path == "Favorites")
                {
                    lectures = aux.GetCache<List<LectureModel>>("lecfavor") ?? new List<LectureModel>();
                }
                else
                {
                    try
                    {
                        _controller.Title = _controller.Title.Replace(" (Offline)", string.Empty);
                        lectures = aux.Lectures(_controller.path);
                    }
                    catch (Exception ex)
                    {
                        aux.Toast(ex.Message);
                        lectures = aux.GetCache<List<LectureModel>>(_controller.path) ?? new List<LectureModel>();
                        if (!_controller.Title.EndsWith("(Offline)"))
                        {
                            _controller.Title = string.Format("{0} (Offline)", _controller.Title);
                        }
                    }                    
                }

                if (lectures != null && String.Compare(_controller.path, @"\\l-drive\lecture", false) > 0)
                {
                    lectures.Insert(0, new LectureModel()
                    {
                        Name = "..",
                        Type = "Directory",
                        FullName = _controller.parent,
                        Size = 1
                    });
                }             
            }            
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                LectureCell cell = (LectureCell)tableView.DequeueReusableCell("Cell", indexPath);
                if (_controller.path != "Favorites" && indexPath.Section == 0)
                {  
                    cell.UpdateCell(new LectureModel() {
                        Name = "Favorites",
                        Type = "Directory",
                        FullName = null,
                        Size = 1
                    }, _controller);
                }
                else
                {
                    cell.UpdateCell(lectures[indexPath.Row], _controller);
                }
                return cell;
            }
            public override nint NumberOfSections(UITableView tableView)
            {
                if (_controller.path == "Favorites")
                    return 1;
                else
                    return 2;
            }
            public override UIView GetViewForHeader(UITableView tableView, nint section)
            {
                if (section == 0)
                    return null;

                UILabel title = new UILabel();
                title.Font = sectionHeaderFont;
                title.Text = _controller.path.Replace(@"\\l-drive\lecture", "L-Drive").ToUpper();
                title.TextColor = UIColor.White;
                title.LineBreakMode = UILineBreakMode.WordWrap;
                title.Lines = 0;
                title.Frame = new CGRect(10, 10, sectionHeaderSize.Width, sectionHeaderSize.Height);

                UIView view = new UIView(new RectangleF(0, 0, (float)sectionHeaderSize.Width + 20, (float)sectionHeaderSize.Height + 20));
                view.BackgroundColor = UIColor.LightGray;

                view.AddSubview(title);

                return view;
            }
            public override nfloat GetHeightForHeader(UITableView tableView, nint section)
            {
                if (section == 0)
                    return 0;

                string text = _controller.path.Replace(@"\\l-drive\lecture", "L-Drive").ToUpper();
                sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
                return sectionHeaderSize.Height + 20;
            }
            public override nint RowsInSection(UITableView tableview, nint section)
            {
                if (_controller.path != "Favorites" && section == 0)
                {
                    return 1;
                }
                return lectures.Count;
            }
            private string GetParentPath(string path)
            {
                return path.Substring(0, path.LastIndexOf('\\', path.Length - 2));
            }
            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                if (_controller.path != "Favorites" && indexPath.Section == 0)
                {
                    _controller.parent = _controller.path;
                    _controller.path = "Favorites";
                }
                else
                {
                    var selected = lectures[indexPath.Row];
                    if (selected.Type == "Directory")
                    {
                        if (selected.Name == "..")
                        {
                            _controller.parent = GetParentPath(_controller.parent);
                            _controller.path = selected.FullName;
                        }
                        else if (selected.Size > 0)
                        {
                            if (_controller.path == "Favorites")
                                _controller.parent = GetParentPath(selected.FullName);
                            else
                                _controller.parent = _controller.path;

                            _controller.path = selected.FullName;
                        }
                    }
                    else
                    {
                        switch (selected.Type)
                        {
                            case "mp3":
                            case "mp4":
                            case "ogg":
                            case "ogv":
                            case "oga":
                            case "wav":
                            case "webm":
                                var uri = string.Format("https://www.kimep.kz/ext/mobile/lectures/media/{0}?file={1}", selected.UID, selected.Name);
                                UIApplication.SharedApplication.OpenUrl(new NSUrl(uri));
                                break;
                            default:
                                string url = string.Format(@"http://www.kimep.kz/ext/mobile/lectures/download?FilePath={0}", selected.FullName);
                                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                                string localPath = System.IO.Path.Combine(documentsPath, selected.Name);
                                if (File.Exists(localPath) && FileIsSame(localPath, selected.FullName))
                                    OpenFile(localPath);
                                else
                                    DownloadFile(localPath, url);
                                break;
                        }
                    }
                }
                _controller.DataBind();
            }
            public bool FileIsSame(string localPath, string remotePath)
            {
                LectureFileInfo remoteInfo = aux.GetFileInfo(remotePath);
                FileInfo localInfo = new FileInfo(localPath);
                if (remoteInfo.Length == localInfo.Length &&
                    remoteInfo.CreationTimeUtc < localInfo.CreationTimeUtc)
                {
                    return true;
                }
                return false;
            }
            public void OpenFile(string localPath)
            {
                NSUrl localUrl = new NSUrl(localPath, true);
                if (localUrl != null)
                {
                    var uidic = UIDocumentInteractionController.FromUrl(localUrl);
                    uidic.Delegate = new DocInteractionC(_controller);
                    uidic.PresentPreview(true);
                }
                else
                {
                    aux.Toast("The file couldn't be opened");
                }
            }
            private void DownloadCanceled(Object sender, EventArgs args)
            {
                IsDownloadCanceled = true;
            }
            private void DownloadFile(string localPath, string url)
            {
                var bounds = UIScreen.MainScreen.Bounds;
                DownloadingOverlay downPop = new DownloadingOverlay(bounds);
                downPop.OnCancel += DownloadCanceled;
                _controller.Root.Add(downPop);

                var webClient = new WebClient();

                webClient.DownloadProgressChanged += (sender, e) =>
                {
                    if (IsDownloadCanceled)
                    {
                        webClient.CancelAsync();
                    }
                    downPop.progressView.Progress = (float)e.BytesReceived / e.TotalBytesToReceive;
                };

                webClient.DownloadDataCompleted += (s, e) =>
                {
                    if (!IsDownloadCanceled)
                    {
                        var data = e.Result;
                        File.WriteAllBytes(localPath, data);
                        try
                        {
                            InvokeOnMainThread(() =>
                            {
                                OpenFile(localPath);
                            });
                        }
                        catch (Exception ex)
                        {
                            aux.Toast(ex.Message);
                        }
                    }
                    else
                    {
                        IsDownloadCanceled = false;
                    }
                    downPop.Hide();
                };
                webClient.DownloadDataAsync(new Uri(url));
            }
        }
    }
    public class DocInteractionC : UIDocumentInteractionControllerDelegate
    {
        readonly UIViewController _viewController;
        public DocInteractionC(UIViewController controller)
        {
            _viewController = controller;
        }
        public override UIViewController ViewControllerForPreview(UIDocumentInteractionController controller)
        {
            return _viewController;
        }
        public override UIView ViewForPreview(UIDocumentInteractionController controller)
        {
            return _viewController.View;
        }
    }
}
