﻿using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using UIKit;

namespace kimep.mobile.ios.Controllers
{
    public class AssessmentScoresController : ContentController
    {
        IAuxiliary aux = new Auxiliary();
        public AssessmentScoresController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ConfigureView();
        }
        void ConfigureView()
        {
            TableView.AllowsSelection = false;
            List<AssessmentScoreModel> assesment_scores;
            try
            {
                Title = Title.Replace(" (Offline)", string.Empty);
                assesment_scores = aux.AssessmentScores();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                assesment_scores = aux.GetCache<List<AssessmentScoreModel>>("AssessmentScores") ??
                                       new List<AssessmentScoreModel>();
                if (!Title.EndsWith("(Offline)"))
                    Title = string.Format("{0} (Offline)", Title);
            }
            TableView.Source = new AssessmentScoresDataSource(assesment_scores);
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 40f;
        }
        private void Refresh(object sender, EventArgs args)
        {
            ConfigureView();
        }
    }
}