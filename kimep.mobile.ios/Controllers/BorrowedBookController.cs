﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;
using kimep.mobile.domain.Common;

namespace kimep.mobile.ios.Controllers
{
    public class BorrowedBookController : ContentController
    {
        IAuxiliary aux = new Auxiliary();
        public BorrowedBookController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ConfigureView();
        }
        void ConfigureView()
        {
            TableView.AllowsSelection = false;

            TableView.RegisterNibForCellReuse(UINib.FromName("BorrowedBooksCell", null), "Cell");
            List<BorrowedBook> books;
            try
            {
                Title = Title.Replace(" (Offline)", string.Empty);
                books = aux.Borrowed_Books();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                books = aux.GetCache<List<BorrowedBook>>("borrowed_books");
                if (!Title.EndsWith("(Offline)"))
                    Title = string.Format("{0} (Offline)", Title);
            }

            TableView.Source = new BorrowedBooksDataSource(books);
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 40f;

        }
        private void Refresh(object sender, EventArgs args)
        {
            ConfigureView();
        }
    }
}