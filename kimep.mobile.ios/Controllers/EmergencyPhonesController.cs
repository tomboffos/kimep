﻿using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using UIKit;
using kimep.mobile.domain.Common;

namespace kimep.mobile.ios.Controllers
{
    public class EmergencyPhonesController : ContentController
    {
        IRequests requests = new Requests();
        IAuxiliary aux = new Auxiliary();
        public EmergencyPhonesController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ConfigureView();
        }
        void ConfigureView()
        {
            TableView.AllowsMultipleSelection = false;
            TableView.RegisterNibForCellReuse(UINib.FromName("ContactCell", null), "Cell");
            List<DepartmentModel> contacts;
            try
            {
                Title = Title.Replace(" (Offline)", string.Empty);
                contacts = aux.Contacts();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                contacts = aux.GetCache<List<DepartmentModel>>("contacts") ??
                                   new List<DepartmentModel>();
                if (!Title.EndsWith("(Offline)"))
                    Title = string.Format("{0} (Offline)", Title);
            }
            TableView.Source = new ContactsDataSource(contacts);
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 40f;
            
        }
        private void Refresh(object sender, EventArgs args)
        {
           ConfigureView();
        }
    }
}