﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace kimep.mobile.ios.Controllers
{
    [Register ("ProfileController")]
    partial class ProfileController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnAvatar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelCreditsEarned { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelCreditsTaken { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelFullName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelGPA { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelProgramID { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelStudentID { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel labelXRay { get; set; }

        [Action ("BtnAvatar_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BtnAvatar_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (btnAvatar != null) {
                btnAvatar.Dispose ();
                btnAvatar = null;
            }

            if (labelCreditsEarned != null) {
                labelCreditsEarned.Dispose ();
                labelCreditsEarned = null;
            }

            if (labelCreditsTaken != null) {
                labelCreditsTaken.Dispose ();
                labelCreditsTaken = null;
            }

            if (labelFullName != null) {
                labelFullName.Dispose ();
                labelFullName = null;
            }

            if (labelGPA != null) {
                labelGPA.Dispose ();
                labelGPA = null;
            }

            if (labelProgramID != null) {
                labelProgramID.Dispose ();
                labelProgramID = null;
            }

            if (labelStudentID != null) {
                labelStudentID.Dispose ();
                labelStudentID = null;
            }

            if (labelXRay != null) {
                labelXRay.Dispose ();
                labelXRay = null;
            }
        }
    }
}