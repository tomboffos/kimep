﻿using CoreGraphics;
using Foundation;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;
using System;
using System.Drawing;
using UIKit;

namespace kimep.mobile.ios.Controllers
{
    public class NewsDetailsController : UIViewController
    {
        private IAuxiliary aux = new Auxiliary();
        UIScrollView scrollView;
        private int _id;
        public NewsDetailsController(int id)
        {
            _id = id;
        }
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            Title = NSBundle.MainBundle.LocalizedString("News", "News");

            UINavigationBar navBar = NavigationController.NavigationBar;
            navBar.Translucent = false;
            navBar.BarTintColor = UIColor.FromRGB(210, 171, 103);
            navBar.TitleTextAttributes = new UIStringAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 23f),
                BaselineOffset = -1,
                ForegroundColor = UIColor.FromRGB(0x47, 0x39, 0x83)
            };
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationItem.SetLeftBarButtonItem(
            new UIBarButtonItem(
                UIImage.FromBundle("arrow_left").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal),
                UIBarButtonItemStyle.Plain,
                (sender, args) =>
                {
                    NavigationController.PopViewController(true);
                }), true);

            News news;
            try
            {
                //Title = Title.Replace(" (Offline)", string.Empty);
                news = aux.ReadNews(_id);
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                news = aux.GetCache<News>(string.Format("news_{0}", _id)) ?? new News();
                //if (!Title.EndsWith("(Offline)"))
                  //  Title = string.Format("{0} (Offline)", Title);
            }

            scrollView = new UIScrollView();
            scrollView.BackgroundColor = UIColor.White;
            scrollView.Frame = View.Frame;

            nfloat top = 10;

            UILabel TitleText = new UILabel();
            UIFont TitleTextFont = UIFont.FromName("Bebas Neue Cyrillic", 22f);
            TitleText.Font = TitleTextFont;
            TitleText.Text = news.Title;
            TitleText.TextColor = UIColor.FromRGB(71, 57, 131);
            TitleText.LineBreakMode = UILineBreakMode.WordWrap;
            TitleText.Lines = 0;
            CGSize TitleTextSize = ((NSString)news.Title).StringSize(TitleTextFont, new CGSize(View.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            TitleText.Frame = new CGRect(10, top, TitleTextSize.Width, TitleTextSize.Height);
            scrollView.AddSubview(TitleText);

            top += TitleText.Frame.Height + 10;

            UILabel CreatedOnText = new UILabel();
            string CreatedOn = string.Format("{0} {1}", news.CreatedOn.ToLongDateString(), news.CreatedOn.ToShortTimeString());
            UIFont CreatedOnTextFont = UIFont.FromName("Segoe UI", 14f);
            CreatedOnText.Font = CreatedOnTextFont;
            CreatedOnText.Text = CreatedOn;
            CreatedOnText.TextAlignment = UITextAlignment.Right;
            CreatedOnText.TextColor = UIColor.FromRGB(210, 171, 103);
            CreatedOnText.LineBreakMode = UILineBreakMode.WordWrap;
            CreatedOnText.Lines = 0;
            CGSize CreatedOnTextSize = ((NSString)CreatedOn).StringSize(CreatedOnTextFont, new CGSize(View.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            CreatedOnText.Frame = new CGRect(10, top, View.Frame.Width - 20, CreatedOnTextSize.Height);
            scrollView.AddSubview(CreatedOnText);

            top += CreatedOnText.Frame.Height + 10;

            UIImageView imagePhoto = new UIImageView();
            if (news.Images.Length > 0)
            {
                var data = NSData.FromArray(Convert.FromBase64String(news.Images[0]));
                UIImage image = UIImage.LoadFromData(data);
                var ratio = image.Size.Height / image.Size.Width;
                imagePhoto.Frame = new CGRect(0, top, View.Bounds.Size.Width, View.Bounds.Size.Width * ratio);
                imagePhoto.Image = image;
            }
            scrollView.AddSubview(imagePhoto);

            top += imagePhoto.Frame.Height + 10;

            UILabel FullText = new UILabel();
            UIFont FullTextFont = UIFont.FromName("Segoe UI", 16f);
            FullText.Font = FullTextFont;
            FullText.Text = news.FullText;
            FullText.TextColor = UIColor.FromRGB(71, 57, 131);
            FullText.LineBreakMode = UILineBreakMode.WordWrap;
            FullText.Lines = 0;
            CGSize FullTextSize = ((NSString)news.FullText).StringSize(FullTextFont, new CGSize(View.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            FullText.Frame = new CGRect(10, top, FullTextSize.Width, FullTextSize.Height);
            scrollView.AddSubview(FullText);

            scrollView.ContentSize = new CGSize(View.Bounds.Size.Width, top + FullText.Frame.Height + 100);
            View.AddSubview(scrollView);
        }
    }
}