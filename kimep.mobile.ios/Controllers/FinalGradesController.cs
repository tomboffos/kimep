﻿using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using UIKit;

namespace kimep.mobile.ios.Controllers
{
    public class FinalGradesController : ContentController
    {
        IAuxiliary aux = new Auxiliary();
        public FinalGradesController()
        {
            Refresh_Click += Refresh;
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ConfigureView();
        }
        void ConfigureView()
        {
            TableView.AllowsSelection = false;
            List<FinalGrade> final_grades;
            try
            {
                Title = Title.Replace(" (Offline)", string.Empty);
                final_grades = aux.Final_grades();
            }
            catch (Exception ex)
            {
                aux.Toast(ex.Message);
                final_grades = aux.GetCache<List<FinalGrade>>("final_grades") ??
                                       new List<FinalGrade>();
                if (!Title.EndsWith("(Offline)"))
                    Title = string.Format("{0} (Offline)", Title);
            }
            TableView.Source = new FinalGradesDataSource(final_grades);

            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 40f;
        }
        private void Refresh(object sender, EventArgs args)
        {
            ConfigureView();
        }
    }
}