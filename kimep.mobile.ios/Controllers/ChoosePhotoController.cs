﻿using System;
using Foundation;
using UIKit;
using kimep.mobile.ios.Common;
using kimep.mobile.ios.Controllers;

namespace kimep.mobile.ios
{
    public partial class ChoosePhotoController : UIViewController
    {
        private ProfileController _parent;
        public ChoosePhotoController(ProfileController parent) : base("ChoosePhotoController", null)
        {
            _parent = parent;
        }
        partial void BtnCamera_TouchUpInside(UIButton sender)
        {
            DismissModalViewController(false);
            //_parent.OpenCamera();
        }
        partial void BtnPhotoLibrary_TouchUpInside(UIButton sender)
        {
            DismissModalViewController(false);            
            //_parent.OpenPhotoLibrary();
        }
        partial void BtnCancel_TouchUpInside(UIButton sender)
        {
            // Dismiss the picker
            DismissModalViewController(false);
        }
        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            // Dismiss the picker
            DismissModalViewController(false);
            base.TouchesBegan(touches, evt);
        }
        public override void ViewDidLoad()
        {
            // Set view of Camera button
            btnCamera.Layer.BackgroundColor = (UIColor.White).CGColor;            
            btnCamera.Layer.BorderColor = (UIColor.LightGray).CGColor;
            btnCamera.Layer.BorderWidth = 1f;
            btnCamera.Layer.CornerRadius = 10f;
            // Set view of Photo Library button
            btnPhotoLibrary.Layer.BackgroundColor = (UIColor.White).CGColor;
            //btnPhotoLibrary.Layer.BorderColor = (UIColor.LightGray).CGColor;
            //btnPhotoLibrary.Layer.BorderWidth = 0f;
            btnPhotoLibrary.Layer.CornerRadius = 10f;
            // Set view of cancel button
            btnCancel.Layer.BackgroundColor = (UIColor.White).CGColor;
            //btnCancel.Layer.BorderColor = (UIColor.LightGray).CGColor;
            //btnCancel.Layer.BorderWidth = 0f;
            btnCancel.Layer.CornerRadius = 10f;
            // Set Modal View Transparent
            View.BackgroundColor = UIColor.FromWhiteAlpha(0.8f, 0.2f);
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

    }
}