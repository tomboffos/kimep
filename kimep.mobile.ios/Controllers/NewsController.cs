﻿using Foundation;
using kimep.mobile.domain.Common;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using UIKit;
using System.Drawing;

namespace kimep.mobile.ios.Controllers
{
    public partial class NewsController : UIViewController
    {
        IAuxiliary aux = new Auxiliary();
        public UITableView TableView;
        //public EventHandler Refresh_Click;
        bool useRefreshControl = false;
        UIRefreshControl refreshControl;
        public UITabBarController _tab;
        public NewsController(UITabBarController tab)
        {
            _tab = tab;
        }
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }
        void AddRefreshControl()
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
            {
                // the refresh control is available, let's add it  
                refreshControl.ValueChanged += async (sender, e) =>
                {
                    await RefreshAsync(sender, e);
                };
                useRefreshControl = true;
            }
        }
        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();
            TableView = new UITableView(View.Bounds, UITableViewStyle.Plain);
            refreshControl = new UIRefreshControl();
            refreshControl.TintColor = UIColor.Clear;
            await RefreshAsync(this, new EventArgs());
            AddRefreshControl();
            TableView.Add(refreshControl);
            View.AddSubview(TableView);

            await ConfigureView();
        }
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

        }
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            Title = NSBundle.MainBundle.LocalizedString("News", "News");

            UINavigationBar navBar = NavigationController.NavigationBar;
            navBar.Translucent = false;

            UIImage orgImage = UIImage.FromFile("Images/menubtnbg.png");
            nfloat height = navBar.Frame.Height;
            nfloat width = orgImage.Size.Width * height / orgImage.Size.Height;
            UIImageView imageView = new UIImageView(orgImage);
            imageView.ContentMode = UIViewContentMode.ScaleToFill;
            imageView.Frame = new RectangleF(0, 0, (float)width, (float)height);
            navBar.InsertSubview(imageView, 1);

            navBar.BarTintColor = UIColor.FromRGB(210, 171, 103);
            navBar.TitleTextAttributes = new UIStringAttributes()
            {
                Font = UIFont.FromName("Bebas Neue Cyrillic", 23f),
                BaselineOffset = -2,
                ForegroundColor = UIColor.FromRGB(0x47, 0x39, 0x83)
            };
        }
        async Task ConfigureView()
        {
            TableView.AllowsSelection = true;
            TableView.RegisterNibForCellReuse(UINib.FromName("NewsCell", null), "NewsCell");
            TableView.RegisterNibForCellReuse(UINib.FromName("NewsHeaderCell", null), "NewsHeaderCell");
            
            List<ShortNews> news = new List<ShortNews>();

            var bounds = UIScreen.MainScreen.Bounds;
            GifOverlay loadPop = new GifOverlay(bounds);
            // Show loading progress            
            _tab.Add(loadPop);

            await Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(() =>
                {
                    try
                    {
                        Title = Title.Replace(" (Offline)", string.Empty);
                        news = aux.News();
                    }
                    catch (Exception ex)
                    {
                        aux.Toast(ex.Message);
                        news = aux.GetCache<List<ShortNews>>("short_news") ?? new List<ShortNews>();
                        if (!Title.EndsWith("(Offline)"))
                            Title = string.Format("{0} (Offline)", Title);
                    }
                });
            }).ContinueWith(task =>
            {
                // Hide loading progress
                TableView.Source = new NewsDataSource(news, this);
                TableView.RowHeight = UITableView.AutomaticDimension;
                TableView.EstimatedRowHeight = 40f;
                loadPop.Hide();
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        async Task RefreshAsync(object sender, EventArgs e)
        {
            // only activate the refresh control if the feature is available  
            if (useRefreshControl)
            {
                refreshControl.BeginRefreshing();
                await ConfigureView();
                refreshControl.EndRefreshing();
            }
        }
    }
}