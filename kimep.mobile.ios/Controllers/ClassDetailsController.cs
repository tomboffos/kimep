﻿using System;
using System.Linq;
using Foundation;
using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using UIKit;
using kimep.mobile.domain.Common;
using System.Drawing;

namespace kimep.mobile.ios
{
    public partial class ClassDetailsController : UIViewController
    {
        MyClass _myClass;
        IRequests requests = new Requests();
        IAuxiliary aux = new Auxiliary();
        TimeZone tz = TimeZone.CurrentTimeZone;
        DateTime Time_From;
        NSDictionary dic;
        public ClassDetailsController(MyClass myClass) : base("ClassDetailsController", null)
        {
            _myClass = myClass;
            Time_From = tz.ToLocalTime(_myClass.Time_From);
            dic = new NSDictionary(
                "AlertSpan", "", 
                "CourseID", _myClass.CourseID,
                "Semester", _myClass.Semester,
                "WeekDay", _myClass.WeekDay,
                "Time_From", _myClass.Time_From.DateTimeToNSDate(),
                "Date_To", _myClass.Date_To.DateTimeToNSDate());
        }
        void OnAlertChanged(object sender, EventArgs e)
        {
            if (_myClass.Date_To.HasValue)
            {
                aux.CancelAlerts(dic);
                TimeSpan TimeOfDay = Time_From.TimeOfDay;
                switch (((PickerEventArgs)e).Selected)
                {
                    case "None":
                        return;
                    case "At time of event":
                        break;
                    case "5 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 5, 0));
                        break;
                    case "10 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 10, 0));
                        break;
                    case "20 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 20, 0));
                        break;
                    case "30 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 30, 0));
                        break;
                    case "1 hour before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(1, 0, 0));
                        break;
                    case "2 hours before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(2, 0, 0));
                        break;
                }
                // Monday - 1
                DateTime today = DateTime.Today + TimeOfDay;
                int dayUntill = ((requests.WeekayToInt(_myClass.WeekDay) - (int)today.DayOfWeek + 7) % 7);
                DateTime DateToSet = today.AddDays(dayUntill);
                var notification = new UILocalNotification();

                notification.UserInfo = new NSDictionary(
                    "AlertSpan", ((PickerEventArgs)e).Selected,
                    "CourseID", _myClass.CourseID,
                    "Semester", _myClass.Semester,
                    "WeekDay", _myClass.WeekDay,
                    "Time_From", _myClass.Time_From.DateTimeToNSDate(),
                    "Date_To", _myClass.Date_To.DateTimeToNSDate());

                notification.Category = "Classes";
                notification.TimeZone = NSTimeZone.LocalTimeZone;
                notification.RepeatCalendar = NSCalendar.CurrentCalendar;
                notification.RepeatInterval = NSCalendarUnit.Week;
                notification.FireDate = DateToSet.DateTimeToNSDate();
                notification.AlertAction = "View Alert";
                notification.AlertBody = string.Format("{0:HH:mm} {1}\r\n{2}\r\n{3}", Time_From, _myClass.Hall, _myClass.Title, _myClass.Instructor);
                notification.ApplicationIconBadgeNumber = 1;
                notification.SoundName = UILocalNotification.DefaultSoundName;
                UIApplication.SharedApplication.ScheduleLocalNotification(notification);
            }
        }
        public override void ViewDidLoad()
        {
            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(
                    UIImage.FromBundle("arrow_left").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal),
                    UIBarButtonItemStyle.Plain,
                    (sender, args) =>
                    {
                        NavigationController.PopViewController(true);
                    }), true);

            NavigationItem.Title = _myClass.Title;
            //textCourseTitle.Text = _myClass.Title;
            textInstructor.Text = _myClass.Instructor;
            labelWeekDay.Text = _myClass.WeekDay;
            labelTimeFrom.Text = Time_From.ToString("HH:mm");
            string[] values = new string[]
            {
                "None",
                "At time of event",
                "5 minutes before",
                "10 minutes before",
                "20 minutes before",
                "30 minutes before",
                "1 hour before",
                "2 hours before"
            };
            var PVM = new PickerViewModel(values);
            PVM.OnPickerChanged += OnAlertChanged;
            pickerAlert.Model = PVM;
            string cat = aux.AlertSpan(dic);
            int index = Array.FindIndex(values, row => row == cat);
            pickerAlert.Select(index, 0, true);
            base.ViewDidLoad();
        }
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

