﻿using System;
using Foundation;
using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using UIKit;
using kimep.mobile.domain.Common;

namespace kimep.mobile.ios
{
    public partial class ExamDetailsController : UIViewController
    {
        TimeZone tz = TimeZone.CurrentTimeZone;
        FinalExam _finalexam;
        IRequests requests = new Requests();
        IAuxiliary aux = new Auxiliary();
        NSDictionary dic;
        DateTime DateToSet;
        public ExamDetailsController(FinalExam finalexam) : base("ExamDetailsController", null)
        {
            _finalexam = finalexam;
            DateToSet = finalexam.Date.Date + finalexam.Time_From.TimeOfDay;

            dic = new NSDictionary(
                    "AlertSpan", "",
                    "CourseID", finalexam.CourseID,
                    "Date", finalexam.Date.DateTimeToNSDate(),
                    "Time_From", finalexam.Time_From.DateTimeToNSDate());
        }

        public override void ViewDidLoad()
        {
            NavigationItem.SetLeftBarButtonItem(
            new UIBarButtonItem(
                UIImage.FromBundle("arrow_left").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal),
                UIBarButtonItemStyle.Plain,
                (sender, args) =>
                {
                    NavigationController.PopViewController(true);
                }), true);

            NavigationItem.Title = _finalexam.TitleCourses;

            labelHall.Text = _finalexam.Hall;
            DateTime dateTime = tz.ToLocalTime(DateToSet);
            labelDate.Text = string.Format("{0} {1}", dateTime.ToLongDateString(), dateTime.ToShortTimeString());

            string[] values = new string[]
            {
                "None", "At time of event", "5 minutes before",
                "10 minutes before", "20 minutes before",
                "30 minutes before", "1 hour before",
                "2 hours before"
            };
            var PVM = new PickerViewModel(values);
            PVM.OnPickerChanged += OnAlertChanged;
            pickerAlert.Model = PVM;
            string cat = aux.AlertSpan(dic);
            int index = Array.FindIndex(values, row => row == cat);
            pickerAlert.Select(index, 0, true);
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        void OnAlertChanged(object sender, EventArgs e)
        {
            if (_finalexam.Date >= DateTime.Today)
            {
                aux.CancelAlerts(dic);
                TimeSpan TimeOfDay = DateToSet.TimeOfDay;
                switch (((PickerEventArgs)e).Selected)
                {
                    case "None":
                        return;
                    case "At time of event":
                        break;
                    case "5 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 5, 0));
                        break;
                    case "10 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 10, 0));
                        break;
                    case "20 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 20, 0));
                        break;
                    case "30 minutes before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(0, 30, 0));
                        break;
                    case "1 hour before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(1, 0, 0));
                        break;
                    case "2 hours before":
                        TimeOfDay = TimeOfDay.Subtract(new TimeSpan(2, 0, 0));
                        break;
                }

                var notification = new UILocalNotification();

                notification.UserInfo = new NSDictionary(
                    "AlertSpan", ((PickerEventArgs)e).Selected,
                    "CourseID", _finalexam.CourseID,
                    "Time_From", _finalexam.Time_From.DateTimeToNSDate(),
                    "Date", _finalexam.Date.DateTimeToNSDate());

                notification.Category = "Exams";
                notification.TimeZone = NSTimeZone.LocalTimeZone;
                notification.FireDate = (DateToSet.Date + TimeOfDay).DateTimeToNSDate();
                notification.AlertAction = "View Alert";
                notification.AlertBody = string.Format("{0:MMMM dd, yyyy HH:mm}\r\n{1}\r\n{2}", tz.ToLocalTime(DateToSet), _finalexam.Hall, _finalexam.TitleCourses);
                notification.ApplicationIconBadgeNumber = 1;
                notification.SoundName = UILocalNotification.DefaultSoundName;
                UIApplication.SharedApplication.ScheduleLocalNotification(notification);
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

