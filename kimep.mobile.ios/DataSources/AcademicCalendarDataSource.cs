﻿using Foundation;
using System.Globalization;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using kimep.mobile.ios.Controllers;
using kimep.mobile.ios.Common;
using System.Drawing;
using CoreGraphics;

namespace kimep.mobile.ios
{
    public class AcademicCalendarDataSource : UITableViewSource
    {
        private acaCalendar _calendar;
        IAuxiliary aux = new Auxiliary();        
        public AcademicCalendarDataSource(acaCalendar calendar)
        {
            _calendar = calendar;
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (AcademicCalendarCell)tableView.DequeueReusableCell("Cell", indexPath);
            cell.UpdateCell(_calendar.Events[indexPath.Row]);
            return cell;
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            UILabel title = new UILabel();
            title.Font = UIFont.FromName("Bebas Neue Cyrillic", 21f);
            title.Text = string.Format("{0}, {1}",
                            CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(_calendar.Month),
                            _calendar.Year.ToString()).ToUpper();
            title.TextColor = UIColor.White;
            title.Frame = new CGRect(10, 10, tableView.Frame.Width - 20, 20);

            UIView view = new UIView(new RectangleF(0, 0, (float)tableView.Frame.Width, 40));
            view.BackgroundColor = UIColor.LightGray;
            view.AddSubview(title);

            return view;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return 40;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _calendar.Events.Count();
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }
    }
}
