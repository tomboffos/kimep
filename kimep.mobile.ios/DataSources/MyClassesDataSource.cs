﻿using Foundation;
using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UIKit;
using CoreGraphics;
using System.Drawing;

namespace kimep.mobile.ios
{
    public class MyClassesDataSource : UITableViewSource
    {
        private IAuxiliary aux = new Auxiliary();
        private Schedule[] schedule;
        private UIViewController _parent;
        private NSDictionary[] alerts;
        private UIFont sectionHeaderFont = UIFont.FromName("Bebas Neue Cyrillic", 21f);
        public MyClassesDataSource(List<MyClass> myClasses, UIViewController parent)
        {
            _parent = parent;
            alerts = aux.Alerts("Classes");
            foreach (NSDictionary a in alerts)
            {
                if (!myClasses.Any(i => 
                                   i.CourseID == a["CourseID"].ToString() &&
                                   i.Semester == a["Semester"].ToString() &&
                                   i.WeekDay == a["WeekDay"].ToString() &&
                                   i.Time_From.DateTimeToNSDate().ToString() == a["Time_From"].ToString()))
                {
                    aux.CancelAlerts(a);
                }
                if ((a["Date_To"] as NSDate).Compare(DateTime.Today.DateTimeToNSDate()) == NSComparisonResult.Ascending)
                {
                    aux.CancelAlerts(a);
                }
            }            

            schedule = (from c in myClasses
                        group c by GetDayOfWeek(c.WeekDay) into _c
                        select new Schedule
                        {
                            WeekDay = DateTimeFormatInfo.CurrentInfo.GetDayName(_c.Key),
                            Classes = _c.ToArray()
                        }).ToArray();
        }
        private DayOfWeek GetDayOfWeek(string WeekDay)
        {
            switch (WeekDay.ToLower())
            {
                case "monday":
                    return DayOfWeek.Monday;
                case "tuesday":
                    return DayOfWeek.Tuesday;
                case "wednesday":
                    return DayOfWeek.Wednesday;
                case "thursday":
                    return DayOfWeek.Thursday;
                case "friday":
                    return DayOfWeek.Friday;
                case "saturday":
                    return DayOfWeek.Saturday;
                default:
                    return DayOfWeek.Sunday;
            }
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (MyClassesCell)tableView.DequeueReusableCell("Cell", indexPath);
            cell.UpdateCell(schedule[indexPath.Section].Classes[indexPath.Row], alerts);
            return cell;
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            UILabel title = new UILabel();
            title.Font = sectionHeaderFont;
            string text = schedule[(int)section].WeekDay.ToUpper();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            title.Text = text;
            title.TextColor = UIColor.White;
            title.LineBreakMode = UILineBreakMode.WordWrap;
            title.Lines = 0;
            title.Frame = new CGRect(10, 10, sectionHeaderSize.Width, sectionHeaderSize.Height);

            UIView view = new UIView(new RectangleF(0, 0, (float)sectionHeaderSize.Width + 20, (float)sectionHeaderSize.Height + 20));
            view.BackgroundColor = UIColor.LightGray;
            view.AddSubview(title);

            return view;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            string text = schedule[(int)section].WeekDay.ToUpper();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            return sectionHeaderSize.Height + 20;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return schedule[(int)section].Classes.Count();
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return schedule.Count();
        }
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            ClassDetailsController classDetails = new ClassDetailsController(schedule[indexPath.Section].Classes[indexPath.Row]);
            _parent.NavigationController.PushViewController(classDetails, animated: true);
        }
    }
}
