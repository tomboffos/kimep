﻿using Foundation;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using kimep.mobile.ios.Common;
using CoreGraphics;
using System.Drawing;

namespace kimep.mobile.ios
{
    public class FinalGradesDataSource : UITableViewSource
    {
        private FinalGradesSemester[] grades;
        private IAuxiliary aux = new Auxiliary();
        private UIFont sectionHeaderFont = UIFont.FromName("Bebas Neue Cyrillic", 21f);
        public FinalGradesDataSource(List<FinalGrade> Grades)
        {
            grades = (from c in Grades
                      group c by c.Semester into _c
                      select new FinalGradesSemester
                      {
                          Semester = _c.Key,
                          Grades = _c.ToArray()
                      }).ToArray();
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Section == 0)
            {
                tableView.RegisterNibForCellReuse(UINib.FromName("GPACell", null), "Cell1");
                var cellGPA = (GPACell)tableView.DequeueReusableCell("Cell1", indexPath);
                GPACRS gpacrs = new GPACRS();
                try
                {
                    gpacrs = aux.GPACRS();                    
                }
                catch (Exception ex)
                {
                    aux.Toast(ex.Message);
                    gpacrs = aux.GetCache<GPACRS>("gpacrs");
                }
                cellGPA.UpdateCell(gpacrs);
                return cellGPA;
            }
            tableView.RegisterNibForCellReuse(UINib.FromName("FinalGradesCell", null), "Cell2");
            var cell = (FinalGradesCell)tableView.DequeueReusableCell("Cell2", indexPath);
            var grade = grades[indexPath.Section - 1].Grades[indexPath.Row];
            cell.UpdateCell(grade);
            return cell;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (section == 0)
                return 1;
            else
                return grades[(int)section - 1].Grades.Count();
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            if (section == 0)
                return null;

            UILabel title = new UILabel();
            title.Font = sectionHeaderFont;
            string text = grades[(int)section - 1].Semester;
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            title.Text = text;
            title.TextColor = UIColor.White;
            title.LineBreakMode = UILineBreakMode.WordWrap;
            title.Lines = 0;
            title.Frame = new CGRect(10, 10, sectionHeaderSize.Width, sectionHeaderSize.Height);

            UIView view = new UIView(new RectangleF(0, 0, (float)sectionHeaderSize.Width + 20, (float)sectionHeaderSize.Height + 20));
            view.BackgroundColor = UIColor.LightGray;
            view.AddSubview(title);

            return view;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            if (section == 0)
                return 0;

            string text = grades[(int)section - 1].Semester;
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            return sectionHeaderSize.Height + 20;
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return grades.Count() + 1;
        }
    }
}
