﻿using CoreGraphics;
using Foundation;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Cells;
using kimep.mobile.ios.Common;
using kimep.mobile.ios.Controllers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using UIKit;

namespace kimep.mobile.ios
{
    public class NewsDataSource : UITableViewSource
    {
        private IAuxiliary aux = new Auxiliary();
        private List<ShortNews> _news;
        private NewsController _parent;
        public NewsDataSource(List<ShortNews> news, NewsController parent)
        {
            _parent = parent;
            _news = news;
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Row == 0)
            {
                NewsHeaderCell cell1 = (NewsHeaderCell)tableView.DequeueReusableCell("NewsHeaderCell", indexPath);
                cell1.UpdateCell(_news[indexPath.Row]);
                return cell1;
            }
            NewsCell cell2 = (NewsCell)tableView.DequeueReusableCell("NewsCell", indexPath);
            cell2.UpdateCell(_news[indexPath.Row]);
            return cell2;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _news.Count();
        }        
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            var bounds = UIScreen.MainScreen.Bounds;
            GifOverlay loadPop = new GifOverlay(bounds);
            // Show loading progress            
            _parent._tab.Add(loadPop);

            Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(() =>
                {
                    NewsDetailsController newsDetails = new NewsDetailsController(_news[indexPath.Row].ID);
                    _parent.NavigationController.PushViewController(newsDetails, animated: true);
                });
            }).ContinueWith(task =>
            {
                tableView.DeselectRow(indexPath, false);
                // Hide loading progress
                loadPop.Hide();
            }, TaskScheduler.FromCurrentSynchronizationContext());



        }
    }
}
