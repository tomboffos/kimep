﻿using Foundation;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace kimep.mobile.ios
{
    public class BorrowedBooksDataSource : UITableViewSource
    {
        private List<BorrowedBook> _books;
        public BorrowedBooksDataSource(List<BorrowedBook> books)
        {
            _books = books;
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (BorrowedBooksCell)tableView.DequeueReusableCell("Cell", indexPath);
            var item = _books[indexPath.Row];
            cell.UpdateCell(item);
            return cell;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (_books == null) return 0;
            return _books.Count();
        }
    }
}
