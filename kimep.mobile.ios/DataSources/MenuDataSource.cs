﻿using System;
using System.Linq;
using Foundation;
using UIKit;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Menus;
using kimep.mobile.ios.Controllers;
using kimep.mobile.domain.Common;
using System.Threading.Tasks;
using System.Drawing;
using CoreAnimation;
using kimep.mobile.ios.Common;
using CoreGraphics;

namespace kimep.mobile.ios.DataSources
{
    public class MenuDataSource : UITableViewSource
    {
        //public event EventHandler OnMenuItemSelected;
        Requests requests = new Requests();
        Auxiliary aux = new Auxiliary();
        static readonly NSString CellIdentifier = new NSString("MenuCell");
        readonly public MenuSection[] mainMenu = MainMenu.MainMenuSections;
        readonly RootController _root;
        public MenuDataSource(RootController root)
        {
            _root = root;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return mainMenu[(int)section].Items.Count();
        }
        public override string TitleForHeader(UITableView tableView, nint section)
        {
            return mainMenu[(int)section].Title;
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            UIView view = new UIView(new System.Drawing.RectangleF(0, 0, (float)tableView.Frame.Width, (float)tableView.SectionHeaderHeight));

            UILabel label = new UILabel();
            label.Opaque = false;
            label.TextColor = UIColor.LightGray;
            label.Font = UIFont.FromName("Helvetica-Bold", 20f);
            label.Frame = new System.Drawing.RectangleF(20, 40, (float)tableView.Frame.Width - 40, 40);
            label.Text = mainMenu[(int)section].Title;

            CALayer bottomBorder = new CALayer();
            bottomBorder.BorderColor = UIColor.LightGray.CGColor;
            bottomBorder.BorderWidth = 2;
            bottomBorder.Frame = new RectangleF(-1, (float)label.Frame.Size.Height - 1, (float)label.Frame.Size.Width, 1);

            label.Layer.AddSublayer(bottomBorder);

            view.AddSubview(label);
            return view;
        }
        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
        {
            //UIView cellBackgroundView = new UIView(new RectangleF(0, 0, (float)cell.Bounds.Width, (float)cell.Bounds.Height));
            //cellBackgroundView.BackgroundColor = UIColor.Black;
            //cell.BackgroundView = cellBackgroundView;

            UIView selectedView = new UIView(new RectangleF(0, 0, (float)cell.Bounds.Width, (float)cell.Bounds.Height));
            selectedView.BackgroundColor = UIColor.FromRGB(210, 171, 103);
            cell.SelectedBackgroundView = selectedView;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {         
            return 100f;
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return mainMenu.Count();
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (MenuCell)tableView.DequeueReusableCell(CellIdentifier, indexPath);
            var item = mainMenu[indexPath.Section].Items[indexPath.Row];

            UIView backColor = new UIView();
            backColor.BackgroundColor = UIColor.FromRGB(230, 230, 230);            
            cell.SelectedBackgroundView = backColor;

            cell.UpdateCell(item);

            if (tableView.IndexPathForSelectedRow == null && indexPath.Section == 0 && indexPath.Row  == 0)
            {
                cell.SetSelected(true, false);
            }

            return cell;
        }       
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            var item = mainMenu[indexPath.Section].Items[indexPath.Row];

            var bounds = _root.View.Bounds;
            GifOverlay loadPop = new GifOverlay(bounds);
            // Show loading progress
            _root.View.Add(loadPop);
            // Show content page
            _root.PresentViewController();
            
            Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(() =>
                {                    
                    ContentController viewController = (ContentController)item.Controller;
                    viewController.Title = item.Title;

                    viewController.Root = _root;
                    _root.AddPanGesture();
                    _root.SetContentViewController(new UINavigationController(viewController), true);
                });
            }).ContinueWith(task =>
            {
                // Hide loading progress
                loadPop.Hide();
            }, TaskScheduler.FromCurrentSynchronizationContext());

        }

    }
}