﻿using Foundation;
using kimep.mobile.domain.Models;
using System;
using System.Linq;
using UIKit;
using kimep.mobile.domain.Common;
using kimep.mobile.ios.Common;
using CoreGraphics;
using System.Drawing;

namespace kimep.mobile.ios
{
    public class TodayDataSource : UITableViewSource
    {
        //IRequests requests = new Requests();
        private IAuxiliary aux = new Auxiliary();
        private TodayModel _today;
        private NSDictionary[] alertsClasses;
        private NSDictionary[] alertsExams;
        private UIViewController _parent;
        private UIFont sectionHeaderFont = UIFont.FromName("Bebas Neue Cyrillic", 21f);
        public TodayDataSource(TodayModel today, UIViewController parent)
        {
            _today = today;
            _parent = parent;
            alertsClasses = aux.Alerts("Classes");
            alertsExams = aux.Alerts("Exams");
        }
        UITableViewCell ExamCell(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.RegisterNibForCellReuse(UINib.FromName("FinalExamsCell", null), "Cell1");
            var cellFinalExams = (FinalExamsCell)tableView.DequeueReusableCell("Cell1", indexPath);
            cellFinalExams.UpdateCell(_today.Exams[indexPath.Row], alertsExams);
            return cellFinalExams;            
        }
        UITableViewCell MyClassCell(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.RegisterNibForCellReuse(UINib.FromName("MyClassesCell", null), "Cell1");
            var cellMyClasses = (MyClassesCell)tableView.DequeueReusableCell("Cell1", indexPath);
            cellMyClasses.UpdateCell(_today.Classes[indexPath.Row], alertsClasses);
            return cellMyClasses;
        }
        UITableViewCell AcademicCalendarCell(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.RegisterNibForCellReuse(UINib.FromName("AcademicCalendarCell", null), "Cell2");
            var cellAcademicCalendar = (AcademicCalendarCell)tableView.DequeueReusableCell("Cell2", indexPath);
            cellAcademicCalendar.UpdateCell(_today.Events[indexPath.Row]);
            return cellAcademicCalendar;           
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Section == 0)
            {
                if (_today.Exams.Any())
                {
                    return ExamCell(tableView, indexPath);
                }
                else if (_today.Classes.Any())
                {
                    return MyClassCell(tableView, indexPath);
                }
                else if (_today.Events.Any())
                {
                    return AcademicCalendarCell(tableView, indexPath);
                }
            }
            else if (indexPath.Section == 1)
            {
                if (_today.Exams.Any() && _today.Classes.Any())
                {
                    return MyClassCell(tableView, indexPath);
                }
                else if (_today.Events.Any())
                {
                    return AcademicCalendarCell(tableView, indexPath);
                }
            }
            else if (_today.Exams.Any() && _today.Classes.Any() && _today.Events.Any())
            {
                return AcademicCalendarCell(tableView, indexPath);
            }
            return null;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (section == 0)
            {
                if (_today.Exams.Any())
                {
                    return _today.Exams.Count();
                }
                else if (_today.Classes.Any())
                {
                    return _today.Classes.Count();
                }
                else if (_today.Events.Any())
                {
                    return _today.Events.Count();
                }
            }
            else if (section == 1)
            {
                if (_today.Exams.Any() && _today.Classes.Any())
                {
                    return _today.Classes.Count();
                }
                else if (_today.Events.Any())
                {
                    return _today.Events.Count();
                }
            }
            else if (_today.Exams.Any() && _today.Classes.Any() && _today.Events.Any())
            {
                return _today.Events.Count();
            }
            return 0;
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            UILabel title = new UILabel();
            title.Font = sectionHeaderFont;
            string text = GetTitle(section).ToUpper();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            title.Text = text;
            title.TextColor = UIColor.White;
            title.LineBreakMode = UILineBreakMode.WordWrap;
            title.Lines = 0;
            title.Frame = new CGRect(10, 10, sectionHeaderSize.Width, sectionHeaderSize.Height);
            UIView view = new UIView(new RectangleF(0, 0, (float)sectionHeaderSize.Width + 20, (float)sectionHeaderSize.Height + 20));
            view.BackgroundColor = UIColor.LightGray;
            view.AddSubview(title);

            return view;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            string text = GetTitle(section).ToUpper();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            return sectionHeaderSize.Height + 20;
        }
        private string GetTitle(nint section)
        {
            if (section == 0)
            {
                if (_today.Exams.Any())
                {
                    return NSBundle.MainBundle.LocalizedString("Final Exams", "Final Exams");
                }
                else if (_today.Classes.Any())
                {
                    return NSBundle.MainBundle.LocalizedString("My Classes", "My Classes");
                }
                else if (_today.Events.Any())
                {
                    return NSBundle.MainBundle.LocalizedString("Academic Calendar", "Academic Calendar");
                }
            }
            else if (section == 1)
            {
                if (_today.Exams.Any() && _today.Classes.Any())
                {
                    return NSBundle.MainBundle.LocalizedString("My Classes", "My Classes");
                }
                else if (_today.Events.Any())
                {
                    return NSBundle.MainBundle.LocalizedString("Academic Calendar", "Academic Calendar");
                }
            }
            else if (_today.Exams.Any() && _today.Classes.Any() && _today.Events.Any())
            {
                return NSBundle.MainBundle.LocalizedString("Academic Calendar", "Academic Calendar");
            }
            return "";
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return 
                (_today.Exams != null && _today.Exams.Any() ? 1 : 0) +
                (_today.Classes != null && _today.Classes.Any() ? 1 : 0) +
                (_today.Events != null && _today.Events.Any() ? 1 : 0);
        }
        void OpenExamDetails(FinalExam exam)
        {
            ExamDetailsController examsDetails = new ExamDetailsController(exam);
            _parent.NavigationController.PushViewController(examsDetails, animated: true);
        }
        void OpenClassDetails(MyClass myclass)
        {
            ClassDetailsController classDetails = new ClassDetailsController(myclass);
            _parent.NavigationController.PushViewController(classDetails, animated: true);      
        }
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Section == 0)
            {
                if (_today.Exams.Any())
                {
                    OpenExamDetails(_today.Exams[indexPath.Row]);
                }
                else if (_today.Classes.Any())
                {
                    OpenClassDetails(_today.Classes[indexPath.Row]);
                }
                //else if (_today.Events.Any())
                //{
                    
                //}
            }
            else if (indexPath.Section == 1)
            {
                if (_today.Exams.Any() && _today.Classes.Any())
                {
                    OpenClassDetails(_today.Classes[indexPath.Row]);
                }
                //else if (_today.Events.Any())
                //{
                   
                //}
            }
            //else if (_today.Exams.Any() && _today.Classes.Any && _today.Events.Any())
            //{
                
            //}
        }
    }
}
