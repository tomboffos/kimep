﻿using Foundation;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using kimep.mobile.ios.Common;
using CoreGraphics;
using System.Drawing;

namespace kimep.mobile.ios
{
    public class AssessmentScoresDataSource : UITableViewSource
    {
        private AssessmentScoresSemester[] scores;
        private IAuxiliary aux = new Auxiliary();
        private UIFont sectionHeaderFont = UIFont.FromName("Bebas Neue Cyrillic", 21f);
        public AssessmentScoresDataSource(List<AssessmentScoreModel> Scores)
        {
            scores = (from c in Scores
                      group c by c.Semester into _c
                      select new AssessmentScoresSemester
                      {
                          Semester = _c.Key,
                          Scores = _c.ToArray()
                      }).ToArray();
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {            
            tableView.RegisterNibForCellReuse(UINib.FromName("AssessmentScoresCell", null), "Cell");
            var cell = (AssessmentScoresCell)tableView.DequeueReusableCell("Cell", indexPath);
            var score = scores[indexPath.Section].Scores[indexPath.Row];
            cell.UpdateCell(score);
            return cell;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return scores[(int)section].Scores.Count();
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            UILabel title = new UILabel();
            title.Font = sectionHeaderFont;
            string text = scores[(int)section].Semester;
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            title.Text = text;
            title.TextColor = UIColor.White;
            title.LineBreakMode = UILineBreakMode.WordWrap;
            title.Lines = 0;
            title.Frame = new CGRect(10, 10, sectionHeaderSize.Width, sectionHeaderSize.Height);

            UIView view = new UIView(new RectangleF(0, 0, (float)sectionHeaderSize.Width + 20, (float)sectionHeaderSize.Height + 20));
            view.BackgroundColor = UIColor.LightGray;
            view.AddSubview(title);

            return view;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            string text = scores[(int)section].Semester;
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            return sectionHeaderSize.Height + 20;
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return scores.Count();
        }
    }
}
