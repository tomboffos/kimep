﻿using CoreGraphics;
using Foundation;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using UIKit;

namespace kimep.mobile.ios
{
    public class FinalExamsDataSource : UITableViewSource
    {
        private IAuxiliary aux = new Auxiliary();
        private FinalExamsDate[] FinalExamsDates;
        private UIViewController _parent;
        private NSDictionary[] alerts;
        private UIFont sectionHeaderFont = UIFont.FromName("Bebas Neue Cyrillic", 21f);
        public FinalExamsDataSource(List<FinalExam> FinalExams, UIViewController parent)
        {
            _parent = parent;
            alerts = aux.Alerts("Exams");
            foreach (NSDictionary a in alerts)
            {
                if (!FinalExams.Any(i => i.CourseID == a["CourseID"].ToString() &&
                                         i.Date.DateTimeToNSDate().ToString() == a["Date"].ToString() &&
                                         i.Time_From.DateTimeToNSDate().ToString() == a["Time_From"].ToString()))
                {
                    aux.CancelAlerts(a);
                }
                if ((a["Date"] as NSDate).Compare(DateTime.Today.DateTimeToNSDate()) == NSComparisonResult.Ascending)
                {
                    aux.CancelAlerts(a);
                }
            }

            TimeZone tz = TimeZone.CurrentTimeZone;

            FinalExamsDates = (from c in FinalExams
                               group c by tz.ToLocalTime(c.Date) into _c
                               select new FinalExamsDate
                               {
                                   Date = _c.Key,
                                   FinalExams = _c.ToArray()
                               }).ToArray();
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (FinalExamsCell)tableView.DequeueReusableCell("Cell", indexPath);
            cell.UpdateCell(FinalExamsDates[indexPath.Section].FinalExams[indexPath.Row], alerts);
            return cell;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return FinalExamsDates[(int)section].FinalExams.Count();
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            UILabel title = new UILabel();
            title.Font = sectionHeaderFont;
            string text = FinalExamsDates[(int)section].Date.ToLongDateString();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            title.Text = text;
            title.TextColor = UIColor.White;
            title.LineBreakMode = UILineBreakMode.WordWrap;
            title.Lines = 0;
            title.Frame = new CGRect(10, 10, sectionHeaderSize.Width, sectionHeaderSize.Height);

            UIView view = new UIView(new RectangleF(0, 0, (float)sectionHeaderSize.Width + 20, (float)sectionHeaderSize.Height + 20));
            view.BackgroundColor = UIColor.LightGray;
            view.AddSubview(title);

            return view;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            string text = FinalExamsDates[(int)section].Date.ToLongDateString();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            return sectionHeaderSize.Height + 20;
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return FinalExamsDates.Count();
        }
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            ExamDetailsController examDetails = new ExamDetailsController(FinalExamsDates[indexPath.Section].FinalExams[indexPath.Row]);
            _parent.NavigationController.PushViewController(examDetails, animated: true);
        }
    }
}
