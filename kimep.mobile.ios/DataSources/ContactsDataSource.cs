﻿using Foundation;
using kimep.mobile.ios.Common;
using kimep.mobile.domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using CoreGraphics;
using System.Drawing;

namespace kimep.mobile.ios
{
    public class ContactsDataSource : UITableViewSource
    {
        private List<DepartmentModel> _contacts;
        private UIFont sectionHeaderFont = UIFont.FromName("Bebas Neue Cyrillic", 21f);
        public ContactsDataSource(List<DepartmentModel> contacts)
        {
            _contacts = contacts;
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (ContactCell)tableView.DequeueReusableCell("Cell", indexPath);
            cell.UpdateCell(_contacts[indexPath.Section].Contacts[indexPath.Row]);
            return cell;
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            UILabel title = new UILabel();
            title.Font = sectionHeaderFont;
            string text = _contacts[(int)section].Division.ToUpper();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            title.Text = text;
            title.TextColor = UIColor.White;
            title.LineBreakMode = UILineBreakMode.WordWrap;
            title.Lines = 0;
            title.Frame = new CGRect(10, 10, sectionHeaderSize.Width, sectionHeaderSize.Height);

            UIView view = new UIView(new RectangleF(0, 0, (float)sectionHeaderSize.Width + 20, (float)sectionHeaderSize.Height + 20));
            view.BackgroundColor = UIColor.LightGray;
            view.AddSubview(title);

            return view;
        }
        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            string text = _contacts[(int)section].Division.ToUpper();
            CGSize sectionHeaderSize = ((NSString)text).StringSize(sectionHeaderFont, new CGSize(tableView.Frame.Width - 20, nfloat.MaxValue), UILineBreakMode.WordWrap);
            return sectionHeaderSize.Height + 20;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _contacts[(int)section].Contacts.Count();
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return _contacts.Count();
        }
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            var contact = _contacts[indexPath.Section].Contacts[indexPath.Row];
            var url = new NSUrl("tel:" + contact.Phone);
            if (!UIApplication.SharedApplication.OpenUrl(url))
            {                
                var av = new UIAlertView("Can‘t make the call", 
                            "1. Turn Airplane Mode on and off. Go to Settings and turn on Airplane Mode, wait five seconds, then turn it off.\r\n" +
                            "2. Check your Do Not Disturb settings.Go to Settings > Do Not Disturb and make sure it's off.\r\n" +
                            "3. Check for any blocked phone numbers.Go to Settings > Phone > Call Blocking & Identification.\r\n" +
                            "4. See if Call Forwarding is turned on. Go to Settings > Phone > Call Forwarding and make sure it's off.", 
                                         null, "OK", null);
                av.Show();
            }
        }
    }
}
