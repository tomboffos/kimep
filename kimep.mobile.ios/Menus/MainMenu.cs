﻿using System;
using kimep.mobile.domain.Models;
using kimep.mobile.ios.Controllers;
using Foundation;

namespace kimep.mobile.ios.Menus
{	
    public static class MainMenu
    {
        public static readonly MenuSection[] MainMenuSections =
            new MenuSection[] {
            new MenuSection {
                ID = new Guid("8A15A95D-12CE-4688-BCD7-9CA54D4755C5"),
                Title = NSBundle.MainBundle.LocalizedString ("KIMEP University", "KIMEP University"),
                Items = new MenuItem[] {
                    new MenuItem() {
                        ID = new Guid("68B75817-1293-47A2-92D5-5B75D5363E21"),
                        Text = NSBundle.MainBundle.LocalizedString ("Today", "Today"),
                        Title = DateTime.Today.ToLongDateString(),
                        Desc = NSBundle.MainBundle.LocalizedString ("Today", "Today"),
                        Icon = "today",
                        Controller = new TodayController()
                    },
                    new MenuItem() {
                        ID = new Guid("68B75817-1293-47A2-92D5-5B75D5063E21"),
                        Text = NSBundle.MainBundle.LocalizedString ("My Classes", "My Classes"),
                        Title = NSBundle.MainBundle.LocalizedString ("My Classes", "My Classes"),
                        Desc = "My Classes",
                        Icon = "schedule",
                        Controller = new MyClassesController()
                    },
                    new MenuItem() {
                        ID = new Guid("1A871FF8-9E76-4DE7-A767-20203EE53777"),
                        Text = NSBundle.MainBundle.LocalizedString ("Assessment Scores", "Assessment Scores"),
                        Title = NSBundle.MainBundle.LocalizedString ("Assessment Scores", "Assessment Scores"),
                        Desc = "Assessment Scores",
                        Icon = "assessments",
                        Controller = new AssessmentScoresController()
                    },
                    new MenuItem() {
                        ID = new Guid("1A871FF8-9E76-4DE7-A767-20203EE53777"),
                        Text = NSBundle.MainBundle.LocalizedString ("Final Exams", "Final Exams"),
                        Title = NSBundle.MainBundle.LocalizedString ("Final Exams", "Final Exams"),
                        Desc = "Final Exams",
                        Icon = "time",
                        Controller = new FinalExamsController()
                    },
                    new MenuItem() {
                        ID = new Guid("1A871FF8-9E76-4DE7-A767-20203EE5396B"),
                        Text = NSBundle.MainBundle.LocalizedString ("Final Grades", "Final Grades"),
                        Title = NSBundle.MainBundle.LocalizedString ("Final Grades", "Final Grades"),
                        Desc = "Final Grades",
                        Icon = "grades",
                        Controller = new FinalGradesController()
                    },
                    new MenuItem() {
                        ID = new Guid("6083D216-1F32-4257-8CE4-ADA540E2F2E3"),
                        Text = NSBundle.MainBundle.LocalizedString ("Academic Calendar", "Academic Calendar"),
                        Title = NSBundle.MainBundle.LocalizedString ("Academic Calendar", "Academic Calendar"),
                        Desc = "Academic Calendar",
                        Icon = "calendar",
                        Controller = new AcademicCalendarController()
                    },
                    new MenuItem() {
                        ID = new Guid("6083D216-1F32-4207-8CE4-ADA040E2F2E3"),
                        Text = NSBundle.MainBundle.LocalizedString ("Emergency Phones", "Emergency Phones"),
                        Title = NSBundle.MainBundle.LocalizedString ("Emergency Phones", "Emergency Phones"),
                        Desc = "Emergency Phones",
                        Icon = "phone",
                        Controller = new EmergencyPhonesController()
                    },
                    new MenuItem() {
                        ID = new Guid("6083D216-1F32-4207-8CE4-ACA040A2F2D3"),
                        Text = NSBundle.MainBundle.LocalizedString ("Borrowed Books", "Borrowed Books"),
                        Title = NSBundle.MainBundle.LocalizedString ("Borrowed Books", "Borrowed Books"),
                        Desc = "Borrowed Books",
                        Icon = "books",
                        Controller = new BorrowedBookController()
                    },
                    new MenuItem() {
                        ID = new Guid("528F386C-8470-43E8-8631-0AA1FC54DC5B"),
                        Text = NSBundle.MainBundle.LocalizedString ("Lectures", "Lectures"),
                        Title = NSBundle.MainBundle.LocalizedString ("Lectures", "Lectures"),
                        Desc = "Lectures",
                        Icon = "lectures",
                        Controller = new LectureViewController()
                    }
                }
            }
        };
    }
}
