﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace mobile.domain.Models
{
    public class CalendarParam
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }
    [Serializable]
    [DataContract]
    public class acaCalendar
    {
        [DataMember]
        public int Month { get; set; }
        [DataMember]
        public int Year { get; set; }
        [DataMember]
        public List<acaEvent> Events { get; set; }
    }
    [Serializable]
    [DataContract]
    public class acaEvent
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int Day { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Semester { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
