﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace mobile.domain.Models
{
    public partial class Final_Exam
    {
        public string CourseID { get; set; }
        public string Group { get; set; }
        public string Hall { get; set; }
        public string Code { get; set; }
        public string TitleCourses { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime Time_From { get; set; }
        public Nullable<System.DateTime> Time_To { get; set; }
    }
    [Serializable]
    [DataContract]
    public class TodayModel
    {
        [DataMember]
        public DateTime Today { get; set; }
        [DataMember]
        public List<MyClass> Classes { get; set; }
        [DataMember]
        public List<acaEvent> Events { get; set; }
        [DataMember]
        public List<Final_Exam> Exams { get; set; }
    }
}
