﻿using System;
using System.Runtime.Serialization;

namespace mobile.domain.Models
{
    [Serializable]
    [DataContract]
    public class PersonalModel
    {
        [DataMember]
        public int StudentID { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string ProgramID { get; set; }
    }
}
