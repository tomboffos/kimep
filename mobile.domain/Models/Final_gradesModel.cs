﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace mobile.domain.Models
{
    [Serializable]
    [DataContract]
    public class Final_grade
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Semester { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Grade { get; set; }
        [DataMember]
        public double? Point { get; set; }
    }
    [Serializable]
    [DataContract]
    public class GPACRS
    {
        [DataMember]
        public double GPA { get; set; }
        [DataMember]
        public double CreditsEarned { get; set; }
        [DataMember]
        public double CreditsTaken { get; set; }
    }
}
