﻿using System;
using System.Runtime.Serialization;

namespace mobile.domain.Models
{
    [Serializable]
    [DataContract]
    public class BorrowedBook
    {
        public long ID { get; set; }
        [DataMember]
        public string BookId { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime? DateDUE { get; set; }
    }
}
