﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mobile.domain.Models
{
    public class DepartmentModel
    {
        public string Division { get; set; }
        public IEnumerable<ContactModel> Contacts { get; set; }
    }
    public class ContactModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
