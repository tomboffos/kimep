﻿using System;
using System.Runtime.Serialization;

namespace mobile.domain.Models
{
    public class CourseTicketParam
    {
        public int limit { get; set; }
        public Guid id { get; set; }
    }
    [Serializable]
    [DataContract]
    public class Mate
    {
        [DataMember]
        public int StudentID { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
    }
    [Serializable]
    [DataContract]
    public class MyClass
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string WeekDay { get; set; }
        [DataMember]
        public string Semester { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string CourseID { get; set; }
        [DataMember]
        public string Hall { get; set; }
        [DataMember]
        public string Instructor { get; set; }
        [DataMember]
        public string LDrive { get; set; }
        [DataMember]
        public string Section { get; set; }
        [DataMember]
        public DateTime Time_From { get; set; }
        [DataMember]
        public DateTime? Time_To { get; set; }
        [DataMember]
        public DateTime Date_From { get; set; }
        [DataMember]
        public DateTime? Date_To { get; set; }
    }
}