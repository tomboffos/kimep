﻿using System;
using System.Runtime.Serialization;

namespace mobile.domain.Models
{
    [Serializable]
    [DataContract]
    public partial class ShortNews
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ShortText { get; set; }
        [DataMember]
        public System.DateTime CreatedOn { get; set; }
        [DataMember]
        public Nullable<int> CategoryID { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public DateTime? ReadOn { get; set; }
    }
    [Serializable]
    [DataContract]
    public partial class News
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ShortText { get; set; }
        [DataMember]
        public string FullText { get; set; }
        [DataMember]
        public string URL { get; set; }
        [DataMember]
        public Nullable<long> PushID { get; set; }
        [DataMember]
        public System.DateTime CreatedOn { get; set; }
        [DataMember]
        public Nullable<int> CategoryID { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public DateTime? ReadOn { get; set; }
    }
}
