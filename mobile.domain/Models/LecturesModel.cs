﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace mobile.domain.Models
{
    [DataContract]
    public class JsonFilePar
    {
        [DataMember]
        public string FilePath { get; set; }
    }
    [DataContract]
    public class LectureFileInfo
    {
        [DataMember]
        public long Length { get; set; }
        [DataMember]
        public DateTime CreationTimeUtc { get; set; }
    }
    [DataContract]
    public class JsonLecturesPar
    {
        [DataMember]
        public string DirectoryPath { get; set; }
        [DataMember]
        public string KeyWord { get; set; }
    }
    [Serializable]
    [DataContract]
    public class LectureModel
    {
        [DataMember]
        public long ID { get; set; }
        [DataMember]
        public Guid UID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public DateTime CreationTime { get; set; }
        [DataMember]
        public string Icon { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public long Size { get; set; }
    }
}
