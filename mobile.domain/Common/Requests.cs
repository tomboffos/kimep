﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using mobile.domain.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace mobile.domain.Common
{
    public static partial class Settings
    {
        public static Ticket Ticket { get; set; }
        public static int StudentID { get; set; }
        public static string Token { get; set; }
        public static PersonalModel personalInfo { get; set; }
    }
    public interface IRequests
    {
        Task<HttpResponseMessage> Send_Json(string param, string url);
        Ticket Ticket(Guid id);
        Ticket Login(int StudentID, string Password, string Token);
        string NotifyReceive(Guid id);
        acaCalendar Academic_Calendar(int year, int month);
        PersonalModel Personal();
        List<BorrowedBook> Borrowed_Books();
        List<Final_grade> Final_grades(Guid id);
        List<ShortNews> News(Guid id);
        News ReadNews(int id);
        GPACRS GPACRS();
        TodayModel Today();
        List<MyClass> Schedule();
        LectureFileInfo GetFileInfo(string FilePath);
        List<LectureModel> Lectures(string DirectoryPath);
        string SizeSuffix(Int64 value, int decimalPlaces = 1);
        T Deserialize<T>(string obj);
        string Serialize(object obj);
        Task<int> Upload(byte[] data, string fileName);
    }
    public class Requests : IRequests
    {
        public async Task<int> Upload(byte[] data, string fileName)
        {
            int result = 0;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            using (var handler = new HttpClientHandler())
            {
                using (var client = new HttpClient(handler))
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        content.Add(new StreamContent(new MemoryStream(data)), "avatar", fileName);
                        client.BaseAddress = new Uri("https://www.kimep.kz/ext/mobile/");
                        var response = await client.PostAsync(string.Format("avatar/upload/{0}", Settings.Ticket.id), content).ConfigureAwait(false);
                        if (response.IsSuccessStatusCode)
                        {
                            int.TryParse(await response.Content.ReadAsStringAsync(), out result);
                        }
                    }
                }
            }
            return result;
        }
        public async Task<HttpResponseMessage> Send_Json(string param, string url)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            using (var handler = new HttpClientHandler())
            {
                using (var client = new HttpClient(handler))
                {                    
                    client.Timeout = TimeSpan.FromMilliseconds(10000);
                    client.BaseAddress = new Uri("https://www.kimep.kz/ext/mobile/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent contentPost = new StringContent(param, Encoding.UTF8, "application/json");
                    return await client.PostAsync(url, contentPost).ConfigureAwait(false);                        
                }
            }
        }
        public Ticket Ticket(Guid id)
        {
            try
            {
                string _param = JsonConvert.SerializeObject(new { id = id });
                var response = Send_Json(_param, "auth/ticket").Result;
                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<Ticket>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
            }
            return null;
        }
        public Ticket Login(int StudentID, string Password, string Token)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new { StudentID = StudentID, Password = Password, Token = Token });
                var response = Send_Json(param, "auth/login").Result;
                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<Ticket>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
            }
            return null;
        }
        public string NotifyReceive(Guid id)
        {
            string param = JsonConvert.SerializeObject(new { id = id });
            var response = Send_Json(param, "notify/recieve").Result;
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }
            return null;
        }
        public acaCalendar Academic_Calendar(int year, int month)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new CalendarParam { Year = year, Month = month });
                var response = Send_Json(param, "schedule/academic_calendar").Result;
                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<acaCalendar>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
            }
            return new acaCalendar();
        }
        public PersonalModel Personal()
        {
            if (Settings.Ticket != null)
            {
                if (Settings.Ticket != null)
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "personal/info").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<PersonalModel>(response.Content.ReadAsStringAsync().Result);
                    }
                }
            }
            return new PersonalModel();
        }
        public List<BorrowedBook> Borrowed_Books()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "library/borrowed").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<List<BorrowedBook>>(response.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
                }
            }
            return new List<BorrowedBook>();
        }
        public List<Final_grade> Final_grades(Guid id)
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/final_grades").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<List<Final_grade>>(response.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
                }
            }
            return new List<Final_grade>();
        }
        public List<ShortNews> News(Guid id)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new { id = id });
                var response = Send_Json(param, "news/news").Result;
                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<ShortNews>>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
            }
            return new List<ShortNews>();
        }
        public News ReadNews(int id)
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { ticket_id = Settings.Ticket.id, news_id = id });
                    var response = Send_Json(param, "news/read").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<News>(response.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
                }
            }
            return new News();
        }
        public Dictionary<string, ContactModel[]> Contacts()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    var response = Send_Json(null, "contacts/contacts").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<Dictionary<string, ContactModel[]>>(response.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
                }
            }
            return null;
        }
        public GPACRS GPACRS()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/GPACRS").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<GPACRS>(response.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
                }
            }
            return null;
        }
        public TodayModel Today()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "today/index").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<TodayModel>(response.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
                }
            }
            return new TodayModel();
        }
        public List<MyClass> Schedule()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/personal").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return JsonConvert.DeserializeObject<List<MyClass>>(response.Content.ReadAsStringAsync().Result);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
                }
            }
            return new List<MyClass>();
         }
        public LectureFileInfo GetFileInfo(string FilePath)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new JsonFilePar { FilePath = FilePath });
                var response = Send_Json(param, "lectures/getfileinfo").Result;
                if (response.IsSuccessStatusCode)
                {
                    var _result = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<LectureFileInfo>(_result);
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
            }
            return null;
        }
        public List<LectureModel> Lectures(string DirectoryPath)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new JsonLecturesPar { DirectoryPath = DirectoryPath });
                var response = Send_Json(param, "lectures/directory").Result;
                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<LectureModel>>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException("Unable to load data. Please check your network connection and try again.");
            }
            return new List<LectureModel>();
        }
        readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return "0.0 bytes"; }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }
        public T Deserialize<T>(string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj);
        }
        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
