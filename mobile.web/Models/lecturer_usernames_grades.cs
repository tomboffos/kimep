//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mobile.web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class lecturer_usernames_grades
    {
        public int ID { get; set; }
        public string full_name { get; set; }
        public string username { get; set; }
        public string E_mail { get; set; }
        public string full_NameRus { get; set; }
        public string Program1 { get; set; }
        public string Program2 { get; set; }
        public string Program3 { get; set; }
        public string Program4 { get; set; }
        public string Program5 { get; set; }
        public string Department { get; set; }
        public string l_drive { get; set; }
    }
}
