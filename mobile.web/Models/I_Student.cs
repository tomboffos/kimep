//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mobile.web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class I_Student
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public I_Student()
        {
            this.mobile_Message1 = new HashSet<mobile_Message>();
            this.MobileLogons = new HashSet<MobileLogon>();
            this.mobile_Documents = new HashSet<mobile_Documents>();
        }
    
        public int StudentID { get; set; }
        public string Password { get; set; }
        public Nullable<int> YearEntry { get; set; }
        public string StatusID { get; set; }
        public string ProgramID { get; set; }
        public Nullable<int> Year { get; set; }
        public string SP { get; set; }
        public string Minor { get; set; }
        public bool Male { get; set; }
        public string Last_Name { get; set; }
        public string First_Name { get; set; }
        public string Last_Name_Rus { get; set; }
        public string First_Name_Rus { get; set; }
        public string Father_s_Name_Rus { get; set; }
        public string Last_Name_Kaz { get; set; }
        public string First_Name_Kaz { get; set; }
        public string Father_s_Name_Kaz { get; set; }
        public string StatusForGrad { get; set; }
        public string AddInfo { get; set; }
        public bool FinAid { get; set; }
        public string DMinor { get; set; }
        public string DMajor { get; set; }
        public Nullable<int> Protokol { get; set; }
        public string Award { get; set; }
        public Nullable<System.DateTime> DateOfGrad { get; set; }
        public string DateOfGradRus { get; set; }
        public Nullable<int> YearGradReq { get; set; }
        public string Catalog { get; set; }
        public Nullable<System.DateTime> DateAdd { get; set; }
        public Nullable<System.DateTime> KIMEPCouncil { get; set; }
        public Nullable<System.DateTime> DateCor { get; set; }
        public bool DiplCollegeInstitutOld { get; set; }
        public Nullable<int> Attestat { get; set; }
        public Nullable<int> ENT { get; set; }
        public Nullable<int> AcSp { get; set; }
        public Nullable<int> OfPerm { get; set; }
        public Nullable<int> Contract { get; set; }
        public Nullable<int> DiplCollegeInstitut { get; set; }
        public string Employee { get; set; }
        public bool AltynBelgi { get; set; }
        public bool OlimpiadInt { get; set; }
        public bool OlimpiadRep { get; set; }
        public string DateOfGradKaz { get; set; }
        public bool StateAward { get; set; }
        public string IIN { get; set; }
        public string GradProj { get; set; }
        public string Cohort { get; set; }
        public Nullable<int> ITest { get; set; }
        public Nullable<int> CrimRec { get; set; }
        public Nullable<int> BankSt { get; set; }
        public string KZSpecCode { get; set; }
        public string PersonalFile { get; set; }
        public string LDP { get; set; }
        public string Certificate { get; set; }
        public string EntryStatus { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mobile_Message> mobile_Message1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MobileLogon> MobileLogons { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mobile_Documents> mobile_Documents { get; set; }
        public virtual StPersonal StPersonal { get; set; }
        public virtual XRay XRay { get; set; }
    }
}
