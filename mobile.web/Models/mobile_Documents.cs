//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mobile.web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class mobile_Documents
    {
        public int ID { get; set; }
        public byte[] Content { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public int Type { get; set; }
        public int StudentID { get; set; }
    
        public virtual mobile_DocumentType mobile_DocumentType { get; set; }
        public virtual I_Student I_Students { get; set; }
    }
}
