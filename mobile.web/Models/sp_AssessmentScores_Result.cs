//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mobile.web.Models
{
    using System;
    
    public partial class sp_AssessmentScores_Result
    {
        public string Semester { get; set; }
        public string Code { get; set; }
        public string TitleCourses { get; set; }
        public string Registration { get; set; }
        public Nullable<double> Score1 { get; set; }
        public Nullable<double> Score2 { get; set; }
        public Nullable<double> Score3 { get; set; }
        public string FinalAssessment { get; set; }
    }
}
