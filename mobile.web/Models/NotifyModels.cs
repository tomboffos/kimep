﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mobile.web.Models
{
    public class GCMReply
    {
        [Key]
        public long message_id { get; set; }
    }
    public class NotifyMessage
    {
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
    }
}