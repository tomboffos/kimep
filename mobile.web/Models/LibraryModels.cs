﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace mobile.web.Models
{
    public class LibraryDBContext : DbContext
    {
        public DbSet<STK_ITEM> STK_ITEMS { get; set; }
    }
    public class STK_ITEM
    {
        [Key]
        public int STK_CAT_REF { get; set; }
        public string STK_ITEM_NO { get; set; }
        public string STK_BOR_BAR_NO { get; set; }
        public Nullable<System.DateTime> STK_ISS_DUE { get; set; }
        public Nullable<System.DateTime> STK_DATE_RECD { get; set; }
        public Nullable<System.DateTime> STK_DATE_DUE { get; set; }
        public string STK_LINE2 { get; set; }
        public string STK_LINE1 { get; set; }
        public string STK_LINE3 { get; set; }
        public string STK_LINE4 { get; set; }
        public string STK_LINE5 { get; set; }
        public string STK_LINE6 { get; set; }
        public Nullable<decimal> STK_COST { get; set; }
        public string STK_EDITION { get; set; }
        public string STK_VOLUME { get; set; }
        public string STK_ISS_LOC { get; set; }
        public Nullable<System.DateTime> STK_LAST_TAKE { get; set; }
        public Nullable<int> STK_TIMES_BORROWED { get; set; }
        public Nullable<int> STK_TIMES_RESERVED { get; set; }
        public Nullable<int> STK_TIMES_RENEWED { get; set; }
        public Nullable<int> STK_ISS_RENEWED { get; set; }
        public Nullable<System.DateTime> STK_ISS_DATE { get; set; }
        public Nullable<int> STK_IS_RESERVED { get; set; }
        public string STK_ISSUE_TYPE { get; set; }
        public string STK_CONVERT_VALUE { get; set; }
        public Nullable<System.DateTime> STK_LAST_DEP { get; set; }
        public Nullable<int> UNIQUE_ITEM_NO { get; set; }
        public string STK_OPER { get; set; }
        public Nullable<System.DateTime> STK_DATETIME { get; set; }
        public Nullable<System.DateTime> STK_LASTACTIVE { get; set; }
        public Nullable<System.DateTime> STK_DATE_ACC { get; set; }
        public string STK_FOR_LOAN { get; set; }
        public Nullable<int> STK_LAST_RESNO { get; set; }
        public string STK_KEY1 { get; set; }
        public string STK_KEY2 { get; set; }
        public string STK_KEY3 { get; set; }
        public string STK_KEY4 { get; set; }
        public string STK_KEY5 { get; set; }
        public string STK_KEY6 { get; set; }
        public string STK_DESCRIPTION { get; set; }
        public string STK_STATS_CODE { get; set; }
        public string STK_OPAC_SHOW { get; set; }
        public string STK_OPER_ALERT { get; set; }
        public string STK_LOC_FLOOR { get; set; }
        public string STK_PROCESS { get; set; }
        public Nullable<System.DateTime> STK_PROCESS_DATE { get; set; }
        public string STK_LOC_TEMP { get; set; }
        public string STK_LOC_PERM { get; set; }
        public string STK_STOCK_TAKE { get; set; }
        public string STK_IS_ON_LOAN { get; set; }
        public string STK_IS_ON_ORDER { get; set; }
        public string STK_FORM { get; set; }
        public string STK_BOR_NAME { get; set; }
        public string STK_ITEM_ORIGIN { get; set; }
        public string STK_LOAN_TYPE { get; set; }
        public Nullable<int> STK_TIMES_BORPR { get; set; }
        public Nullable<int> STK_TIMES_RENPR { get; set; }
        public Nullable<int> STK_TIMES_RESVPR { get; set; }
        public Nullable<System.DateTime> STK_ILL_DUE { get; set; }
        public string STK_ILL_RENEW { get; set; }
        public string STK_ILL_SUPP { get; set; }
        public Nullable<int> STK_RELATED_NO { get; set; }
        public string STK_ITEM_TYPE { get; set; }
        public string STK_CALL_SET { get; set; }
        public Nullable<decimal> STK_ORIG_COST { get; set; }
        public string STK_LIB_GROUP { get; set; }
        public Nullable<int> STK_AVG_RATING { get; set; }
    }
}