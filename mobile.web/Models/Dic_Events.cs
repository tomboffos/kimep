//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mobile.web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dic_Events
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Dic_Events()
        {
            this.Academic_Calendar = new HashSet<Academic_Calendar>();
        }
    
        public int EventID { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Academic_Calendar> Academic_Calendar { get; set; }
    }
}
