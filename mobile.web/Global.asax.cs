﻿using Postal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace mobile.web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            dynamic email = new Email("Error");
            email.User = User.Identity.Name;
            email.To = "timur@kimep.kz";
            email.Subject = "KIMEP MOBILE: Error on the page.";
            email.Message = exception.Message;
            email.Source = exception.Source;
            email.InnerException = exception.InnerException;
            email.Data = exception.Data;
            email.StackTrace = exception.StackTrace;
            email.Send();
        }
    }
}
