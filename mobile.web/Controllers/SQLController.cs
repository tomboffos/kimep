﻿using mobile.web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace mobile.web.Controllers
{
    public class SQLController : Controller
    {

        public const string API_KEY = "AAAAkSZbnxQ:APA91bHxMluoS1a4p1aUv41JUByyfqQnF4FRMNrrTmu49FPIuk1DSjP-b_RhJfoieAafTE3xeLy-Yxk4ZCcrCC8xhNqefLusNQMFNrw2p8vjqx-VAMl2So8EFr4rvL_pNUgTS_Bq5-Pf";
        [HttpPost]
        public JsonResult Push(string Title, string Message)
        {
            string address = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("Authorization", "key=" + API_KEY);
            request.Method = "POST";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = string.Format("{{\r\n  \"to\": \"{0}\",\r\n  \"data\": {{\r\n    \"title\": \"{1}\",\r\n    \"message\": \"{2}\"\r\n  }}\r\n}}", "/topics/global", Title, Message);

                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)request.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Regex message_id = new Regex("{\"message_id\":(?<message_id>.*?)}", RegexOptions.ExplicitCapture);
                long id = 0;
                long.TryParse(message_id.Match(result).Groups["message_id"].Value, out id);                
                return Json(new { message_id = id }, JsonRequestBehavior.AllowGet);                
            }
        }
    }
}