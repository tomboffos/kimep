﻿using mobile.domain.Models;
using mobile.web.Models;
using Newtonsoft.Json;
using System;
using System.Data.Entity.SqlServer;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace mobile.web.Controllers
{
    public class NewsController : Controller
    {
        regofficeEntities db = new regofficeEntities();
        public DateTime? ReadOn(int news_id, Guid id)
        {
            try
            {
                var news_read = db.mobile_News_Read.FirstOrDefault(r => r.NewsID == news_id && r.ReaderID == id);
                if (news_read == null)
                    return null;
                else
                    return news_read.ReadOn;
            }
            catch (Exception ex)
            {
                throw new Exception("News/ReadOn", ex);
            }
        }
        public int Auditory(int news_id)
        {
            try
            {
                return db.mobile_News_Read
                         .Count(r => r.NewsID == news_id);
            }
            catch (Exception ex)
            {
                throw new Exception("News/Auditory", ex);
            }
        }
        public int _Auditory(int news_id)
        {
            try
            {
                return db.mobile_News_Read
                         .Where(r => r.NewsID == news_id)
                         .Select(r => r.ReaderID)
                         .Distinct()
                         .Count();
            }
            catch (Exception ex)
            {
                throw new Exception("News/_Auditory", ex);
            }
        }
        public JsonResult Total()
        {
            try
            {
                return Json(db.mobile_News.Count(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("News/Total", ex);
            }
        }
        [HttpPost]
        public JsonResult NewsAt(Guid id, int First, int Length)
        {
            try
            {
                var _news = (from n in db.mobile_News
                             orderby n.CreatedOn descending
                             select n).Skip(First).Take(Length).ToList();

                var news = from n in _news
                           select new
                           {
                               n.ID,
                               n.CategoryID,
                               Category = n.mobile_Category == null ? null : n.mobile_Category.Category,
                               n.Title,
                               n.ShortText,
                               n.CreatedOn,
                               ReadOn = ReadOn(n.ID, id),
                               Auditory = Auditory(n.ID),
                               _Auditory = _Auditory(n.ID),
                               //Image = MakeThumbnail(n.mobile_News_Image.FirstOrDefault().Image, 300, 225)
                               Image = Convert.ToBase64String(n.ID == _news[0].ID ? ShrinkThumb(n.mobile_News_Image.FirstOrDefault().Image, 300, 600) :
                                      MakeThumbnail(n.mobile_News_Image.FirstOrDefault().Image, 300, 225))
                           };

                return Json(news, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("News/NewsAt", ex);
            }
        }
        [HttpPost]
        public JsonResult News(Guid id)
        {
            try
            {
                var news = (from n in db.mobile_News
                            //where SqlFunctions.DateAdd("week", 1, n.CreatedOn) >= DateTime.Now
                            orderby n.CreatedOn descending
                            select new ShortNews
                            {
                                ID = n.ID,
                                CategoryID = n.CategoryID,
                                Category = n.mobile_Category.Category,
                                Title = n.Title,
                                ShortText = n.ShortText,
                                CreatedOn = n.CreatedOn
                            }).Take(10).ToList();

                news.ForEach(n => n.ReadOn = ReadOn(n.ID, id));

                return Json(news, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("News/News", ex);
            }
        }
        [HttpPost]
        public JsonResult _News(Guid id)
        {
            try
            {
                var _news = (from n in db.mobile_News
                             orderby n.CreatedOn descending
                             //where SqlFunctions.DateAdd("week", 1, n.CreatedOn) >= DateTime.Now
                             select n).Take(10).ToList();

                var news = from n in _news
                           select new
                           {
                               n.ID,
                               n.CategoryID,
                               Category = n.mobile_Category == null ? null : n.mobile_Category.Category,
                               n.Title,
                               n.ShortText,
                               n.CreatedOn,
                               ReadOn = ReadOn(n.ID, id),
                               Auditory = Auditory(n.ID),
                               _Auditory = _Auditory(n.ID),
                               Image = Convert.ToBase64String(n.ID == _news[0].ID ? ShrinkThumb(n.mobile_News_Image.FirstOrDefault().Image, 300, 600) :
                                        MakeThumbnail(n.mobile_News_Image.FirstOrDefault().Image, 300, 225))
                           };
                
                return Json(news, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("News/_News", ex);
            }
        }
        public byte[] ShrinkThumb(byte[] myImage, int thumbWidth, int thumbHeight)
        {
            using (MemoryStream streamImage = new MemoryStream(myImage))
            {
                using (Image originalImage = Image.FromStream(streamImage))
                {
                    int w = originalImage.Width;
                    int h = originalImage.Height;

                    float ratio = (float)w / h;
                    if (w > h)
                    {
                        w = thumbWidth;
                        h = (int)(thumbWidth / ratio);
                    }
                    else
                    {
                        h = thumbHeight;
                        w = (int)(thumbHeight * ratio);
                    }             


                    Image thumbnail = originalImage.GetThumbnailImage(w, h, () => false, IntPtr.Zero);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        thumbnail.Save(ms, ImageFormat.Jpeg);
                        return ms.ToArray();
                    }
                }
            }
        }
        public byte[] MakeThumbnail(byte[] myImage, int thumbWidth, int thumbHeight)
        {
            using (MemoryStream streamImage = new MemoryStream(myImage))
            {
                using (Image originalImage = Image.FromStream(streamImage))
                {
                    int w = originalImage.Width;
                    int h = originalImage.Height;
                    int x = w > h ? (w - h) / 2 : 0;
                    int y = w > h ? 0 : (h - w) / 2;
                    w = w > h ? h : w;
                    h = w > h ? h : w;

                    Image thumbnail = CropImage(originalImage, h, w, x, y).GetThumbnailImage(thumbWidth, thumbHeight, () => false, IntPtr.Zero);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        thumbnail.Save(ms, ImageFormat.Jpeg);
                        return ms.ToArray();
                    }
                }
            }
        }
        public Image CropImage(Image Image, int Height, int Width, int StartAtX, int StartAtY)
        {
            Image outimage;
            MemoryStream mm = null;
            try
            {
                //check the image height against our desired image height
                if (Image.Height < Height)
                {
                    Height = Image.Height;
                }

                if (Image.Width < Width)
                {
                    Width = Image.Width;
                }

                //create a bitmap window for cropping
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(72, 72);

                //create a new graphics object from our image and set properties
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //now do the crop
                grPhoto.DrawImage(Image, new Rectangle(0, 0, Width, Height), StartAtX, StartAtY, Width, Height, GraphicsUnit.Pixel);

                // Save out to memory and get an image from it to send back out the method.
                mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                Image.Dispose();
                bmPhoto.Dispose();
                grPhoto.Dispose();
                outimage = Image.FromStream(mm);

                return outimage;
            }
            catch (Exception ex)
            {
                throw new Exception("Error cropping image, the error was: " + ex.Message);
            }
        }
        [HttpPost]
        public JsonResult Read(Guid ticket_id, int news_id)
        {
            try
            {
                var n = db.mobile_News.First(x => x.ID == news_id);

                var news = new
                {
                    Category = n.mobile_Category == null ? null : n.mobile_Category.Category,
                    n.CategoryID,
                    n.CreatedOn,
                    n.FullText,
                    n.ID,
                    n.PushID,
                    n.ShortText,
                    n.Title,
                    n.URL,
                    ReadOn = ReadOn(n.ID, ticket_id),
                    Images = n.mobile_News_Image.Select(i => Convert.ToBase64String(ShrinkThumb(i.Image, 300, 400)))
                };

                db.mobile_News_Read.Add(new mobile_News_Read()
                {
                    NewsID = news_id,
                    ReaderID = ticket_id,
                    ReadOn = DateTime.Now
                });
                db.SaveChanges();

                return Json(news, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("News/Read", ex);
            }
        }
    }    
}