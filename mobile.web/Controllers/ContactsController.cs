﻿using mobile.domain.Models;
using mobile.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mobile.web.Controllers
{
    public class ContactsController : Controller
    {
        regofficeEntities db = new regofficeEntities();
        [HttpPost]
        public JsonResult Contacts(Guid id)
        {
            var contacts = from c in db.Contacts
                           group c by new { c.Division1.ID, c.Division1.Description } into gc 
                           orderby gc.Key.ID
                           select new DepartmentModel
                           {
                               Division = gc.Key.Description,
                               Contacts = from x in gc                                           
                                          select new ContactModel
                                          {
                                              ID = x.ID,
                                              Name = x.Name,
                                              Address = x.Address,
                                              Phone = x.Phone,
                                              Position = x.Position
                                          }
                           };

            return Json(contacts, JsonRequestBehavior.AllowGet);
        }
    }
}