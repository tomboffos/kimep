﻿using mobile.domain.Models;
using mobile.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mobile.web.Controllers
{
    public class PersonalController : Controller
    {
        regofficeEntities db = new regofficeEntities();
        [HttpPost]
        public JsonResult Info(TicketParam model)
        {
            var Info = (from stud in db.I_Students
                        join log in db.MobileLogons on stud.StudentID equals log.StudentID
                        where log.ID == model.id
                        select new
                        {
                            StudentID = stud.StudentID,
                            FirstName = stud.First_Name,
                            LastName = stud.Last_Name,
                            ProgramID = stud.ProgramID,
                            XRay = stud.XRay.Date,
                        }).FirstOrDefault();

            return Json(Info, JsonRequestBehavior.AllowGet);
        }
    }
}