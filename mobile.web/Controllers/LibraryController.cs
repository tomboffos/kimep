﻿using mobile.domain.Models;
using mobile.web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
namespace mobile.web.Controllers
{
    public class LibraryController : Controller
    {
        regofficeEntities regdb = new regofficeEntities();
        LibraryDBContext libdb = new LibraryDBContext();
        private IEnumerable<BorrowedBook> BorrowedBook(string STK_BOR_BAR_NO)
        {
            return from item in libdb.STK_ITEMS
                   where (item.STK_BOR_BAR_NO == "B" + STK_BOR_BAR_NO ||
                          item.STK_BOR_BAR_NO == "M" + STK_BOR_BAR_NO) && item.STK_ISS_DUE != null
                   orderby item.STK_DATE_DUE
                   select new BorrowedBook
                   {
                       BookId = item.STK_ITEM_NO,
                       Author = item.STK_LINE2,
                       Name = item.STK_LINE1,
                       DateDUE = item.STK_DATE_DUE
                   };
            /* SELECT STK_ITEM_NO AS BookId, STK_LINE2 AS Author, STK_LINE1 AS Name, STK_DATE_DUE AS DateDUE
            FROM dbo.STK_ITEM	
            WHERE (STK_BOR_BAR_NO LIKE ('%' + CAST(@StudentID - 20000000 AS nvarchar))) AND 
                ((20000000 + dbo.GetNumbers(STK_BOR_BAR_NO)) = @StudentID)
                AND NOT STK_ISS_DUE IS NULL		
            ORDER BY STK_DATE_DUE */
        }
        [HttpPost]
        public JsonResult Borrowed(TicketParam model)
        {
            MobileLogon login = regdb.MobileLogons.FirstOrDefault(t => t.ID == model.id);
            if (login != null)
            {
                string STK_BOR_BAR_NO = login.StudentID.ToString().Substring(2, 6);
                var books = BorrowedBook(STK_BOR_BAR_NO);
                return Json(books, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Borrowed(int id)
        {
            string STK_BOR_BAR_NO = id.ToString().Substring(2, 6);
            var books = BorrowedBook(STK_BOR_BAR_NO);
            return Json(books, JsonRequestBehavior.AllowGet);
        }
    }
}