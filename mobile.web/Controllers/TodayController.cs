﻿using mobile.domain.Models;
using mobile.web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace mobile.web.Controllers
{
    public class TodayController : Controller
    {
        regofficeEntities db = new regofficeEntities();
        public JsonResult Index(TicketParam model)
        {
            var culture = new CultureInfo("en-US");
            var dayOfWeek = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);
            var Classes = (from reg in db.W_Registration_for_Courses_S98_WithCurSem
                           join ticket in db.MobileLogons on reg.StudentID equals ticket.StudentID
                           where ticket.ID == model.id
                           join lim in db.C_Limit on
                               new { reg.CourseID, SemesterID = reg.Semester, Section = reg.Group } equals
                               new { lim.C_W_Courses_And_Semesters.CourseID, lim.C_W_Courses_And_Semesters.SemesterID, lim.Section }
                           join _instr in db.lecturer_usernames_grades on lim.Instructor equals _instr.full_name into jinstrs
                           from instr in jinstrs.DefaultIfEmpty()
                           where !lim.IsCanceled &&
                                 !lim.C_W_Courses_And_Semesters.IsCanceled &&
                                 SqlFunctions.DateAdd("day", 1, lim.DateFrom) <= DateTime.Now &&
                                    SqlFunctions.DateAdd("day", 1, lim.DateTo) >= DateTime.Now && reg.Registration != "w"
                                        && lim.Day == dayOfWeek
                           orderby lim.WeekDay.ID, lim.Time_From
                           select new MyClass
                           {
                               ID = lim.ID,
                               WeekDay = lim.Day,
                               CourseID = lim.C_W_Courses_And_Semesters.CourseID,
                               Instructor = lim.Instructor,
                               Semester = lim.C_W_Courses_And_Semesters.SemesterID,
                               Hall = lim.Hall,
                               Title = lim.C_W_Courses_And_Semesters.TitleCourses,
                               Date_From = lim.DateFrom,
                               Date_To = lim.DateTo,
                               Time_From = lim.Time_From,
                               Time_To = lim.Time_To,
                               LDrive = instr.l_drive,
                               Section = lim.Section
                           }).ToList();

            var Events = (from evn in db.Academic_Calendar
                          where evn.Date.HasValue &&
                            evn.Date.Value > SqlFunctions.DateAdd("d", -3, DateTime.Today) &&
                            evn.Date.Value < SqlFunctions.DateAdd("d", 3, DateTime.Today)
                          select new acaEvent
                          {
                              ID = evn.ID,
                              Day = SqlFunctions.DatePart("day", evn.Date) ?? 0,
                              Date = evn.Date.Value,
                              Description = evn.Dic_Events.Description,
                              Priority = evn.Dic_Events.Priority,
                              Semester = evn.Semester
                          }).ToList();

            var login = db.MobileLogons.Where(l => l.ID == model.id).FirstOrDefault();

            var Exams = (from e in db.sp_Final_Exams_Schedule(login.StudentID)
                         where e.Date.Date == DateTime.Today
                         select new Final_Exam
                         {
                             Code = e.Code,
                             CourseID = e.CourseID,
                             Date = e.Date,
                             Group = e.Group,
                             Hall = e.Hall,
                             Time_From = e.Time_From,
                             Time_To = e.Time_To,
                             TitleCourses = e.TitleCourses
                         }).ToList();

            var today = new TodayModel
            {
                Today = DateTime.Today,
                Classes = Classes ?? new List<MyClass>(),
                Events = Events ?? new List<acaEvent>(),
                Exams = Exams ?? new List<Final_Exam>()
            };

            return Json(today, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TodaySummary(TicketParam model)
        {
            var currentweek = (from w in db.vw_CurrentWeek
                               select new
                               {
                                   Week = "Now is the " + w.CurrentWeek + " week of the " + w.Semester + " semester"
                               }).FirstOrDefault();

            return Json(currentweek, JsonRequestBehavior.AllowGet);
        }
    }
}