﻿using mobile.domain.Models;
using mobile.web.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace mobile.web.Controllers
{
    public class AvatarController : Controller
    {
        regofficeEntities db = new regofficeEntities();
        public FileResult Avatar(Guid id)
        {
            var Login = db.MobileLogons.FirstOrDefault(l => l.ID == id);
            if (Login != null)
            {
                var Avatar = db.mobile_Documents.FirstOrDefault(d => d.StudentID == Login.StudentID && d.Type == 1);
                if (Avatar != null)
                {
                    return File(Avatar.Content, Avatar.ContentType, Avatar.FileName);
                }
            }
            return null;
        }
        public FileResult Picture(int id)
        {
            var Avatar = db.mobile_Documents.FirstOrDefault(d => d.StudentID == id && d.Type == 1);
            if (Avatar != null)
            {
                byte[] thumbnail = MakeThumbnail(Avatar.Content, 72, 72);
                return File(thumbnail, Avatar.ContentType, Avatar.FileName);
            }
            return File(Server.MapPath("~/Images/NoAvatar.png"), "image/png", "NoAvatar.png");
        }
        public FileResult Thumbnail(Guid id)
        {
            var Login = db.MobileLogons.FirstOrDefault(l => l.ID == id);
            if (Login != null)
            {
                var Avatar = db.mobile_Documents.FirstOrDefault(d => d.StudentID == Login.StudentID && d.Type == 1);
                if (Avatar != null)
                {
                    byte[] thumbnail = MakeThumbnail(Avatar.Content, 72, 72);
                    return File(thumbnail, "iamge/png", Avatar.FileName);
                }
            }
            return null;
        }
        public FileResult Thumbig(Guid id)
        {
            var Login = db.MobileLogons.FirstOrDefault(l => l.ID == id);
            if (Login != null)
            {
                var Avatar = db.mobile_Documents.FirstOrDefault(d => d.StudentID == Login.StudentID && d.Type == 1);
                if (Avatar != null)
                {
                    byte[] thumbnail = MakeThumbnail(Avatar.Content, 200, 200);
                    return File(thumbnail, "iamge/png", Avatar.FileName);
                }
            }
            return null;
        }
        public FileResult Thumb(Guid id, int width, int height)
        {
            var Login = db.MobileLogons.FirstOrDefault(l => l.ID == id);
            if (Login != null)
            {
                var Avatar = db.mobile_Documents.FirstOrDefault(d => d.StudentID == Login.StudentID && d.Type == 1);
                if (Avatar != null)
                {
                    byte[] thumbnail = MakeThumbnail(Avatar.Content, width, height);
                    return File(thumbnail, "iamge/png", Avatar.FileName);
                }
            }
            return null;
        }
        public byte[] MakeThumbnail(byte[] myImage, int thumbWidth, int thumbHeight)
        {
            using (MemoryStream streamImage = new MemoryStream(myImage))
            {
                using (Image originalImage = Image.FromStream(streamImage))
                {
                    int w = originalImage.Width;
                    int h = originalImage.Height;
                    int x = w > h ? (w - h) / 2 : 0;
                    int y = w > h ? 0 : (h - w) / 2;
                    w = w > h ? h : w;
                    h = w > h ? h : w;
                    
                    Image thumbnail = CropImage(originalImage, h, w, x, y).GetThumbnailImage(thumbWidth, thumbHeight, () => false, IntPtr.Zero);                    
                    Image _thumbnail = RoundCorners(thumbnail, thumbWidth / 2, Color.Transparent);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        _thumbnail.Save(ms, ImageFormat.Png);
                        return ms.ToArray();
                    }
                }
            }            
        }
        //Overload for crop that default starts top left of the image.
        public Image CropImage(Image Image, int Height, int Width)
        {
            return CropImage(Image, Height, Width, 0, 0);
        }
        //The crop image sub
        public Image CropImage(Image Image, int Height, int Width, int StartAtX, int StartAtY)
        {
            Image outimage;
            MemoryStream mm = null;
            try
            {
                //check the image height against our desired image height
                if (Image.Height < Height)
                {
                    Height = Image.Height;
                }

                if (Image.Width < Width)
                {
                    Width = Image.Width;
                }

                //create a bitmap window for cropping
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(72, 72);

                //create a new graphics object from our image and set properties
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //now do the crop
                grPhoto.DrawImage(Image, new Rectangle(0, 0, Width, Height), StartAtX, StartAtY, Width, Height, GraphicsUnit.Pixel);

                // Save out to memory and get an image from it to send back out the method.
                mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                Image.Dispose();
                bmPhoto.Dispose();
                grPhoto.Dispose();
                outimage = Image.FromStream(mm);

                return outimage;
            }
            catch (Exception ex)
            {
                throw new Exception("Error cropping image, the error was: " + ex.Message);
            }
        }
        //Hard resize attempts to resize as close as it can to the desired size and then crops the excess
        public Image HardResizeImage(Image Image, int Width, int Height)
        {
            int width = Image.Width;
            int height = Image.Height;
            Image resized = null;
            if (Width > Height)
            {
                resized = ResizeImage(Image, Width, Width);
            }
            else
            {
                resized = ResizeImage(Image, Height, Height);
            }
            Image output = CropImage(resized, Height, Width);
            //return the original resized image
            return output;
        }
        //Image resizing
        public Image ResizeImage(Image Image, int maxWidth, int maxHeight)
        {
            int width = Image.Width;
            int height = Image.Height;
            if (width > maxWidth || height > maxHeight)
            {
                //The flips are in here to prevent any embedded image thumbnails -- usually from cameras
                //from displaying as the thumbnail image later, in other words, we want a clean
                //resize, not a grainy one.
                Image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipX);
                Image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipX);

                float ratio = 0;
                if (width > height)
                {
                    ratio = (float)width / (float)height;
                    width = maxWidth;
                    height = Convert.ToInt32(Math.Round((float)width / ratio));
                }
                else
                {
                    ratio = (float)height / (float)width;
                    height = maxHeight;
                    width = Convert.ToInt32(Math.Round((float)height / ratio));
                }

                //return the resized image
                return Image.GetThumbnailImage(width, height, null, IntPtr.Zero);
            }
            //return the original resized image
            return Image;
        }
        public Image RoundCorners(Image StartImage, int CornerRadius, Color BackgroundColor)
        {
            CornerRadius *= 2;
            Bitmap RoundedImage = new Bitmap(StartImage.Width, StartImage.Height);
            using (Graphics g = Graphics.FromImage(RoundedImage))
            {
                g.Clear(BackgroundColor);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Brush brush = new TextureBrush(StartImage);
                GraphicsPath gp = new GraphicsPath();
                gp.AddArc(0, 0, CornerRadius, CornerRadius, 180, 90);
                gp.AddArc(0 + RoundedImage.Width - CornerRadius, 0, CornerRadius, CornerRadius, 270, 90);
                gp.AddArc(0 + RoundedImage.Width - CornerRadius, 0 + RoundedImage.Height - CornerRadius, CornerRadius, CornerRadius, 0, 90);
                gp.AddArc(0, 0 + RoundedImage.Height - CornerRadius, CornerRadius, CornerRadius, 90, 90);
                g.FillPath(brush, gp);
                return RoundedImage;
            }
        }
        [HttpPost]
        public int Upload(Guid id)
        {
            var Login = db.MobileLogons.FirstOrDefault(l => l.ID == id);
            if (Login != null && Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    byte[] DocFile = new byte[file.ContentLength];
                    file.InputStream.Read(DocFile, 0, file.ContentLength);
                    var Avatar = db.mobile_Documents.FirstOrDefault(d => d.StudentID == Login.StudentID && d.Type == 1);
                    string contentType = file.FileName.ToLower().EndsWith(".jpg") || file.FileName.EndsWith(".jpeg") ? "image/jpeg" :
                                         file.FileName.ToLower().EndsWith(".png") ? "image/png" : "application/octet-stream";
                    if (Avatar == null)
                    {
                        Avatar = new mobile_Documents()
                        {
                            Content = DocFile,
                            FileName = file.FileName,
                            ContentType = contentType,
                            StudentID = Login.StudentID,
                            Type = 1
                        };
                        db.mobile_Documents.Add(Avatar);
                    }
                    else
                    {
                        Avatar.Content = DocFile;
                        Avatar.FileName = file.FileName;
                        Avatar.ContentType = contentType;
                        Avatar.StudentID = Login.StudentID;
                        Avatar.Type = 1;
                    }
                    db.SaveChanges();
                    return Avatar.ID;                
                }
            }
            return 0;
        }
    }
}