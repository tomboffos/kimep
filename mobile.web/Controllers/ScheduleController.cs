﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mobile.web.Models;
using mobile.domain.Models;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Runtime.Caching;
using System.Configuration;
using CrystalDecisions.Shared;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace mobile.web.Controllers
{
    public class ScheduleController : Controller
    {
        regofficeEntities db = new regofficeEntities();
        protected static MemoryCache cache = new MemoryCache("kimep.web");
        [HttpPost]
        public JsonResult Final_grades(TicketParam model)
        {
            var Final_grades = from grade in db.Grades_FTES
                               join ticket in db.MobileLogons on grade.StudentID equals ticket.StudentID
                               join sem in db.Semesters on grade.SemesterID equals sem.SemesterID
                               join point in db.Points on grade.Grade equals point.GR
                               where ticket.ID == model.id
                               join cours in db.C_W_Courses_And_Semesters on new { grade.CourseID, grade.SemesterID } equals new { cours.CourseID, cours.SemesterID }
                               orderby sem.NN descending
                               select new Final_grade
                               {
                                   ID = grade.StudentID,
                                   Semester = grade.SemesterID,
                                   Grade = grade.Grade,
                                   Point = point.Points,
                                   Title = cours.TitleCourses
                               };

            return Json(Final_grades, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AssessmentScores(TicketParam model)
        {
            var Midterm_grades = db.sp_AssessmentScores(model.id);
            return Json(Midterm_grades, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult FinalExams(TicketParam model)
        {
            var login = db.MobileLogons.Where(l => l.ID == model.id).FirstOrDefault();

            if (login == null)
                return Json(null, JsonRequestBehavior.AllowGet);

            var exams = from e in db.sp_Final_Exams_Schedule(login.StudentID)
                        select new
                        {
                            Code = e.Code,
                            CourseID = e.CourseID,
                            Group = e.Group,
                            Hall = e.Hall,
                            Time_From = e.Time_From,
                            Time_To = e.Time_To,
                            TitleCourses = e.TitleCourses,                            
                            Date = e.Date.AddDays(1)                            
                        };

            return Json(exams, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult _FinalExams(TicketParam model)
        {
            var login = db.MobileLogons.Where(l => l.ID == model.id).FirstOrDefault();

            if (login == null)
                return Json(null, JsonRequestBehavior.AllowGet);

            var exams = db.sp_Final_Exams_Schedule(login.StudentID);
            return Json(exams, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GPACRS(TicketParam model)
        {
            var GPA = (from gpa in db.AllGPAWithCrs
                      join ticket in db.MobileLogons on gpa.StudentID equals ticket.StudentID
                      where ticket.ID == model.id
                      select new GPACRS
                      {
                          GPA = gpa.GPA ?? 0,
                          CreditsEarned = gpa.CreditsEarned ?? 0,
                          CreditsTaken = gpa.CreditsTaken ?? 0
                      }).FirstOrDefault();

            return Json(GPA, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Academic_Calendar(CalendarParam model)
        {
            string key = string.Format("acaCalendar_{0}_{1}", model.Month, model.Year);
            acaCalendar cal = cache.Get(key) as acaCalendar;
            if (cal == null)
            {
                cal = new acaCalendar
                {
                    Month = model.Month,
                    Year = model.Year,
                    Events = (from evn in db.Academic_Calendar
                              where SqlFunctions.DatePart("month", evn.Date) == model.Month
                                 && SqlFunctions.DatePart("year", evn.Date) == model.Year
                              orderby evn.Date
                              select new acaEvent
                              {
                                  ID = evn.ID,
                                  Day = SqlFunctions.DatePart("day", evn.Date) ?? 0,
                                  Date = evn.Date ?? DateTime.MaxValue,
                                  Description = evn.Dic_Events.Description,
                                  Priority = evn.Dic_Events.Priority,
                                  Semester = evn.Semester
                              }).ToList()
                };
                cache.Add(key, cal, DateTime.Now.AddDays(1));
            }
            return Json(cal, JsonRequestBehavior.AllowGet);
        }
        private static int WeekOfYearISO8601(DateTime date)
        {
            var day = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(date);
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date.AddDays(4 - (day == 0 ? 7 : day)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        [HttpPost]
        public JsonResult Course(CourseTicketParam model)
        {
            var Mates = from reg in db.W_Registration_for_Courses_S98_WithCurSem
                        join ticket in db.MobileLogons on reg.StudentID equals ticket.StudentID
                        where ticket.ID == model.id
                        join lim in db.C_Limit on
                            new { reg.CourseID, SemesterID = reg.Semester, Section = reg.Group, reg.TutorialID } equals
                            new { lim.C_W_Courses_And_Semesters.CourseID, lim.C_W_Courses_And_Semesters.SemesterID, lim.Section, lim.TutorialID }
                        join _reg in db.W_Registration_for_Courses_S98_WithCurSem on
                            new { lim.C_W_Courses_And_Semesters.CourseID, lim.C_W_Courses_And_Semesters.SemesterID, lim.Section, lim.TutorialID } equals
                            new { _reg.CourseID, SemesterID = _reg.Semester, Section = _reg.Group, _reg.TutorialID }                             
                        join stu in db.I_Students on _reg.StudentID equals stu.StudentID
                        join per in db.StPersonals on _reg.StudentID equals per.StudentID
                        where lim.ID == model.limit && _reg.Registration != "w"
                        select new Mate
                        {
                            StudentID = _reg.StudentID,
                            FirstName = stu.First_Name,
                            LastName = stu.Last_Name,
                            Email = per.E_mail,
                            Phone = per.Mobile_Phone
                        };

            return Json(Mates, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Personal(TicketParam model)
        {
            var MyClasses = from reg in db.W_Registration_for_Courses_S98_WithCurSem                            
                            join ticket in db.MobileLogons on reg.StudentID equals ticket.StudentID
                            where ticket.ID == model.id
                            join lim in db.C_Limit on
                                new { reg.CourseID, SemesterID = reg.Semester, Section = reg.Group } equals
                                new { lim.C_W_Courses_And_Semesters.CourseID, lim.C_W_Courses_And_Semesters.SemesterID, lim.Section }
                            join _instr in db.lecturer_usernames_grades on lim.Instructor equals _instr.full_name into jinstrs
                            from instr in jinstrs.DefaultIfEmpty()
                            where !lim.IsCanceled && 
                                  !lim.C_W_Courses_And_Semesters.IsCanceled &&
                                SqlFunctions.DateAdd("day", 1, lim.DateTo) > DateTime.Now && reg.Registration != "w"
                            orderby lim.WeekDay.ID, lim.Time_From
                            select new MyClass
                            {
                                ID = lim.ID,
                                WeekDay = lim.Day,
                                CourseID = lim.C_W_Courses_And_Semesters.CourseID,
                                Instructor = lim.Instructor,
                                Semester = lim.C_W_Courses_And_Semesters.SemesterID,
                                Hall = lim.Hall,
                                Title = lim.C_W_Courses_And_Semesters.TitleCourses,
                                Date_From = SqlFunctions.DateAdd("day", 1, lim.DateFrom).Value,
                                Date_To = SqlFunctions.DateAdd("day", 1, lim.DateTo),
                                Time_From = lim.Time_From,
                                Time_To = lim.Time_To,
                                LDrive = instr.l_drive,
                                Section = lim.Section
                            };                                
                            
            return Json(MyClasses, JsonRequestBehavior.AllowGet);
        }
        public FileResult Syllabus(string id, string sem, string ins)
        {
            using (ReportClass rptH = new ReportClass())
            {
                rptH.FileName = Server.MapPath(Url.Content("~/App_Data/Syllabus.rpt"));
                rptH.Load();
                rptH.SetDatabaseLogon(ConfigurationManager.AppSettings["web_faculty_Username"], ConfigurationManager.AppSettings["web_faculty_Password"]);

                ParameterValues pvUsr = new ParameterValues();
                ParameterDiscreteValue pdv0 = new ParameterDiscreteValue();
                pdv0.Value = ins;
                pvUsr.Add(pdv0);
                rptH.DataDefinition.ParameterFields["@Instructor"].ApplyCurrentValues(pvUsr);

                ParameterValues pvIns = new ParameterValues();
                ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
                pdv1.Value = id;
                pvIns.Add(pdv1);
                rptH.DataDefinition.ParameterFields["@CourseID"].ApplyCurrentValues(pvIns);

                ParameterValues pvSem = new ParameterValues();
                ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
                pdv2.Value = sem;
                pvIns.Add(pdv2);
                rptH.DataDefinition.ParameterFields["@SemesterID"].ApplyCurrentValues(pvIns);

                Stream stream = rptH.ExportToStream(ExportFormatType.PortableDocFormat);
                return File(stream, "application/pdf", "Syllabus.pdf");
            }
        }
    }
}