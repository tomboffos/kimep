﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using mobile.web.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace mobile.web.Controllers
{
    public class NotifyController : Controller
    {
        public const string API_KEY = "AAAACCYQLes:APA91bHJC9W8PPKBaxquskFMUDqDpp26dqt0tL5mhoUjlcE159XJvGIlOlsNNdQrUifr1OP9xJEaovjvYLmlWaQpmt7NTQFnqGbPGdsFtZ4fMAHCZwlcAJMgl4fMxEAs3po3pSMzqT8K";
        regofficeEntities db = new regofficeEntities();
        [HttpPost]
        public JsonResult Receive(domain.Models.TicketParam ticket)
        {
            IEnumerable<mobile_Message> messages = from msg in db.mobile_Message
                                                   where !msg.SentOn.HasValue && msg.I_Students.MobileLogons.Any(l => l.ID == ticket.id)
                                                   select msg;

            return Json(messages, JsonRequestBehavior.AllowGet);
        }
        public ViewResult Index()
        {
            return View();
        }
        [HttpPost]
        public ViewResult Index(NotifyMessage model)
        {
            string address = "https://fcm.googleapis.com/fcm/send";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("Authorization", "key=" + API_KEY);
            request.Method = "POST";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = string.Format("{{\r\n  \"to\": \"{0}\",\r\n  \"data\": {{\r\n    \"title\": \"{1}\",\r\n    \"message\": \"{2}\"\r\n  }}\r\n}}", "/topics/global", model.Title, model.Message);

                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)request.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Regex message_id = new Regex("{\"message_id\":(?<message_id>.*?)}", RegexOptions.ExplicitCapture);
                long id = 0;
                if (long.TryParse(message_id.Match(result).Groups["message_id"].Value, out id))
                {
                    Response.Write(id);
                }                
            }
            return View(model);
        }
        [HttpPost]
        public ViewResult Index1(NotifyMessage model)
        {
            if (ModelState.IsValid)
            {
                var jGcmData = new JObject();
                var jData = new JObject();

                jData.Add("title", model.Title);
                jData.Add("message", model.Message);
                jGcmData.Add("to", "/topics/global");
                jGcmData.Add("data", jData);

                var url = new Uri("https://fcm.googleapis.com/fcm/send");
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/json"));

                        client.DefaultRequestHeaders.TryAddWithoutValidation(
                            "Authorization", "key=" + API_KEY);

                        Task.WaitAll(client.PostAsync(url,
                            new StringContent(jGcmData.ToString(), Encoding.Default, "application/json"))
                                .ContinueWith(response =>
                                {
                                    var result = response.Result;
                                    if (result.IsSuccessStatusCode)
                                    {
                                        string ss = result.Content.ReadAsStringAsync().Result;
                                        //"{\"message_id\":7492252831341868279}"
                                        Regex message_id = new Regex("{\"message_id\":(?<message_id>.*?)}", RegexOptions.ExplicitCapture);
                                        string id = message_id.Match(ss).Groups["message_id"].Value;
                                        GCMReply reply = JsonConvert.DeserializeObject<GCMReply>(result.Content.ReadAsStringAsync().Result);
                                        Response.Write(string.Format("Message ID: {0}", reply.message_id));
                                    }
                                    else
                                    {
                                        Response.Write(result.Content.ReadAsStringAsync().Result);
                                    }                                    
                                }));
                    }
                }
                catch (Exception e)
                {
                    Response.Write("Unable to send GCM message:<br />");
                    Response.Write(e.StackTrace);
                }
            }
            return View(model);
        }
    }
}