﻿using mobile.web.Models;
using mobile.domain.Models;
using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;

namespace mobile.web.Controllers
{
    public class AuthController : Controller
    {
        regofficeEntities db = new regofficeEntities();
        [HttpPost]
        public JsonResult Login(Auth model)
        {
            ObjectParameter ID = new ObjectParameter("ID", typeof(Guid));
            ObjectParameter ExpiredOn = new ObjectParameter("ExpiredOn", typeof(DateTime));
            try
            {
                db.Database.Connection.Open();
                db.sp_MobileLogin(model.StudentID, model.Password, model.Token, ID, ExpiredOn);
                if (ID.Value is DBNull)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
                return Json(new Ticket {
                    id = (Guid)ID.Value,
                    ExpiredOn = (DateTime)ExpiredOn.Value
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Exception _ex = new Exception(string.Format("ID: {0}, ExpiredOn: {1}, StudentID: {2}, Password: {3}, Token: {4}", ID.Value, ExpiredOn.Value, model.StudentID, model.Password, model.Token), ex);
                throw _ex;
            }
        }
        [HttpPost]
        public JsonResult Ticket(TicketParam param)
        {
            try
            {
                Ticket ticket = (from t in db.MobileLogons
                                 where t.ID == param.id && t.ExpiredOn > DateTime.Now
                                 select new Ticket
                                 {
                                     id = t.ID,
                                     ExpiredOn = t.ExpiredOn
                                 }).FirstOrDefault();
            
                return Json(ticket, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Exception _ex = new Exception(string.Format("ID: {0}", param.id), ex);
                throw _ex;
            }
        }        
    }
}