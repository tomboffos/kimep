﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using mibile.intranet.Common;

namespace mibile.intranet.Models
{
    [MetadataType(typeof(mobile_News_Meta))]
    public partial class mobile_News
    {
        [FileType(Allowed = new string[] { ".jpg" }, ErrorMessage = "The file type should be JPG.")]
        [FileSize(MaxLength = 10485760, ErrorMessage = "The maximum file upload size is 10MB.")]
        [Display(Name = "Photo")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase Photo { get; set; }
    }
    public class mobile_News_Meta
    {
        [Key]
        public int ID;
        [StringLength(256)]
        [Required]
        public string Title;
        [StringLength(512)]
        [Display(Name = "Short Text")]
        [Required]
        [DataType(DataType.MultilineText)]
        public string ShortText;
        [Display(Name = "Full Text")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string FullText;
        [DataType(DataType.Url)]
        public string URL;
        [Display(Name = "Push ID")]
        public Nullable<long> PushID;
        [Display(Name = "Category")]
        public Nullable<int> CategoryID;
        [Display(Name = "Created On")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMM dd, yyyy hh:mm tt}")]
        public System.DateTime CreatedOn;
    }
}