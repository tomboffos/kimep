﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace mibile.intranet.Common
{
    public class FileSizeAttribute : ValidationAttribute
    {
        public int MaxLength { get; set; }
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file != null)
            {
                if (file.ContentLength > MaxLength)
                {
                    return false;
                }
            }
            return true;
        }
    }
    public class FileTypeAttribute : ValidationAttribute
    {
        public string[] Allowed { get; set; }
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file != null)
            {
                if (!Allowed.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower()))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
