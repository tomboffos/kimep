﻿using mibile.intranet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mibile.intranet.Controllers
{
    [Authorize(Roles=@"NTDOM\kimep.mobile.news")]
    public class NewsController : Controller
    {
        regofficeEntities db = new regofficeEntities();        
        public ActionResult Index()
        {
            var model = db.mobile_News.OrderByDescending(n => n.CreatedOn);
            return View(model);
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Details(int id)
        {
            var model = db.mobile_News.Find(id);
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            var model = db.mobile_News.Find(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(mobile_News model)
        {
            if (ModelState.IsValid)
            {
                var news = db.mobile_News.Find(model.ID);

                news.ShortText = model.ShortText;
                news.FullText = model.FullText;
                news.URL = model.URL;
                news.Title = model.Title;
                news.CreatedOn = DateTime.Now;

                db.SaveChanges();

                if (model.Photo != null)
                {
                    byte[] buffer = new byte[model.Photo.ContentLength];
                    model.Photo.InputStream.Read(buffer, 0, model.Photo.ContentLength);
                    db.mobile_News_Image.Add(new mobile_News_Image()
                    {
                        ContentType = model.Photo.ContentType,
                        NewsID = model.ID,
                        Name = model.Photo.FileName,
                        PostedOn = DateTime.Now,
                        Image = buffer
                    });

                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            return View(model);
        }
        public FileResult Photo(int id)
        {
            var photo = db.mobile_News_Image.Find(id);
            return File(photo.Image, photo.ContentType);
        }
        public JsonResult DeletePhoto(int id)
        {
            var photo = db.mobile_News_Image.Find(id);
            var news_id = photo.NewsID;
            db.mobile_News_Image.Remove(photo);
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            var model = db.mobile_News.Find(id);
            if (model != null)
            {
                db.mobile_News.Remove(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Create(mobile_News model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                db.mobile_News.Add(model);
                db.SaveChanges();

                if (model.Photo != null)
                {
                    byte[] buffer = new byte[model.Photo.ContentLength];
                    model.Photo.InputStream.Read(buffer, 0, model.Photo.ContentLength);
                    db.mobile_News_Image.Add(new mobile_News_Image()
                    {
                        ContentType = model.Photo.ContentType,
                        NewsID = model.ID,
                        Name = model.Photo.FileName,
                        PostedOn = DateTime.Now,
                        Image = buffer
                    });

                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}