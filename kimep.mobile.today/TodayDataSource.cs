﻿using CoreAnimation;
using Foundation;
using kimep.mobile.domain.Models;
using System;
using System.Drawing;
using System.Linq;
using UIKit;

namespace kimep.mobile.today
{
    public class TodayDataSource : UITableViewSource
    {
        private TodayModel _today;
        private TodayViewController _cntr;
        public TodayDataSource(TodayModel today, TodayViewController cntr)
        {
            _today = today;
            _cntr = cntr;
        }
        UITableViewCell ExamCell(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.RegisterNibForCellReuse(UINib.FromName("FinalExamsCell", null), "Cell1");
            var cellFinalExams = (FinalExamsCell)tableView.DequeueReusableCell("Cell1", indexPath);
            cellFinalExams.UpdateCell(_today.Exams[indexPath.Row]);
            return cellFinalExams;            
        }
        UITableViewCell MyClassCell(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.RegisterNibForCellReuse(UINib.FromName("MyClassesCell", null), "Cell1");
            var cellMyClasses = (MyClassesCell)tableView.DequeueReusableCell("Cell1", indexPath);
            cellMyClasses.UpdateCell(_today.Classes[indexPath.Row]);
            return cellMyClasses;
        }
        UITableViewCell AcademicCalendarCell(UITableView tableView, NSIndexPath indexPath)
        {
            tableView.RegisterNibForCellReuse(UINib.FromName("AcademicCalendarCell", null), "Cell2");
            var cellAcademicCalendar = (AcademicCalendarCell)tableView.DequeueReusableCell("Cell2", indexPath);
            cellAcademicCalendar.UpdateCell(_today.Events[indexPath.Row]);
            return cellAcademicCalendar;           
        }
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Section == 0)
            {
                if (_today.Exams.Any())
                {
                    return ExamCell(tableView, indexPath);
                }
                else if (_today.Classes.Any())
                {
                    return MyClassCell(tableView, indexPath);
                }
                else if (_today.Events.Any())
                {
                    return AcademicCalendarCell(tableView, indexPath);
                }
            }
            else if (indexPath.Section == 1)
            {
                if (_today.Exams.Any() && _today.Classes.Any())
                {
                    return MyClassCell(tableView, indexPath);
                }
                else if (_today.Events.Any())
                {
                    return AcademicCalendarCell(tableView, indexPath);
                }
            }
            else if (_today.Exams.Any() && _today.Classes.Any() && _today.Events.Any())
            {
                return AcademicCalendarCell(tableView, indexPath);
            }

            return null;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (section == 0)
            {
                if (_today.Exams.Any())
                {
                    return _today.Exams.Count();
                }
                else if (_today.Classes.Any())
                {
                    return _today.Classes.Count();
                }
                else if (_today.Events.Any())
                {
                    return _today.Events.Count();
                }
            }
            else if (section == 1)
            {
                if (_today.Exams.Any() && _today.Classes.Any())
                {
                    return _today.Classes.Count();
                }
                else if (_today.Events.Any())
                {
                    return _today.Events.Count();
                }
            }
            else if (_today.Exams.Any() && _today.Classes.Any() && _today.Events.Any())
            {
                return _today.Events.Count();
            }
            return 0;
        }
        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            string Title = string.Empty;
            if (section == 0)
            {
                if (_today.Exams.Any())
                {
                    Title = NSBundle.MainBundle.LocalizedString("Final Exams", "Final Exams");
                }
                else if (_today.Classes.Any())
                {
                    Title = NSBundle.MainBundle.LocalizedString("My Classes", "My Classes");
                }
                else if (_today.Events.Any())
                {
                    Title = NSBundle.MainBundle.LocalizedString("Academic Calendar", "Academic Calendar");
                }
            }
            else if (section == 1)
            {
                if (_today.Exams.Any() && _today.Classes.Any())
                {
                    Title = NSBundle.MainBundle.LocalizedString("My Classes", "My Classes");
                }
                else if (_today.Events.Any())
                {
                    Title = NSBundle.MainBundle.LocalizedString("Academic Calendar", "Academic Calendar");
                }
            }
            else if (_today.Exams.Any() && _today.Classes.Any() && _today.Events.Any())
            {
                Title = NSBundle.MainBundle.LocalizedString("Academic Calendar", "Academic Calendar");
            }

            UIView view = new UIView(new System.Drawing.RectangleF(0, 0, (float)tableView.Frame.Width, 20));
            UILabel label = new UILabel();
            label.Opaque = false;
            label.TextColor = UIColor.LightTextColor;
            label.Font = UIFont.FromName("Helvetica", 14f);
            label.Frame = new RectangleF(3, 5, (float)tableView.Frame.Width, 20);
            label.Text = Title.ToUpper();

            CALayer bottomBorder = new CALayer();
            bottomBorder.BorderColor = UIColor.LightTextColor.CGColor;
            bottomBorder.BorderWidth = 2;
            bottomBorder.Frame = new RectangleF(-1, (float)label.Frame.Size.Height - 1, (float)label.Frame.Size.Width, 1);

            label.Layer.AddSublayer(bottomBorder);


            view.AddSubview(label);
            return view;
        }
        public override nint NumberOfSections(UITableView tableView)
        {
            return 
                (_today.Exams != null && _today.Exams.Any() ? 1 : 0) +
                (_today.Classes != null && _today.Classes.Any() ? 1 : 0) +
                (_today.Events != null && _today.Events.Any() ? 1 : 0);
        }
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            _cntr.ExtensionContext.OpenUrl(NSUrl.FromString("kimepMobile://today"), null);
        }
    }
}
