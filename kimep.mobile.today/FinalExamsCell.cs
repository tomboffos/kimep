﻿using System;
using System.Linq;
using Foundation;
using kimep.mobile.domain.Models;
using UIKit;

namespace kimep.mobile.today
{
    public partial class FinalExamsCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("FinalExamsCell");
        public static readonly UINib Nib;

        static FinalExamsCell()
        {
            Nib = UINib.FromName("FinalExamsCell", NSBundle.MainBundle);
        }
        internal void UpdateCell(FinalExam finalexam)
        {
            BackgroundColor = UIColor.Clear;
            BackgroundView = new UIView();
            SelectedBackgroundView = new UIView();

            TimeZone tz = TimeZone.CurrentTimeZone;

            labelTime.Text = string.Format("{0:HH:mm}-{1:HH:mm}",
                                           tz.ToLocalTime(finalexam.Time_From),
                                           tz.ToLocalTime(finalexam.Time_To));

            labelHall.Text = finalexam.Hall;
            labelTitle.Text = finalexam.TitleCourses;

        }
        protected FinalExamsCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
