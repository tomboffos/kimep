﻿using System;

using Foundation;
using UIKit;

namespace kimep.mobile.today
{
    public partial class SectionHeaderCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("SectionHeaderCell");
        public static readonly UINib Nib;

        static SectionHeaderCell()
        {
            Nib = UINib.FromName("SectionHeaderCell", NSBundle.MainBundle);
        }

        protected SectionHeaderCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
    }
}
