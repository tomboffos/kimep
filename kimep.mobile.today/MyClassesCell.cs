﻿using System;
using System.Linq;
using Foundation;
using kimep.mobile.domain.Models;
using UIKit;
using kimep.mobile.domain.Common;

namespace kimep.mobile.today
{
    public partial class MyClassesCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("MyClassesCell");
        public static readonly UINib Nib;
        private IRequests requests = new Requests();
        static MyClassesCell()
        {
            Nib = UINib.FromName("MyClassesCell", NSBundle.MainBundle);
        }

        protected MyClassesCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }
        internal void UpdateCell(MyClass day)
        {
            BackgroundColor = UIColor.Clear;
            BackgroundView = new UIView();
            SelectedBackgroundView = new UIView();

            TimeZone tz = TimeZone.CurrentTimeZone;

            TimeLabel.Text = day.Time_From == day.Time_To ? "" :
                string.Format("{0:HH:mm}-{1:HH:mm}",
                tz.ToLocalTime(day.Time_From),
                tz.ToLocalTime(day.Time_To ?? DateTime.MinValue));
            TitleLabel.Text = day.Title;
            InstructorLabel.Text = day.Instructor;
            HallLabel.Text = day.Hall == "#NA" ? "" : day.Hall;
            DatesLabel.Text = string.Format("{0:MMM dd, yyyy} - {1:MMM dd, yyyy}", day.Date_From, day.Date_To);
        }
    }
}
