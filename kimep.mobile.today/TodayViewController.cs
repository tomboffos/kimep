﻿using System;

using NotificationCenter;
using Foundation;
using Social;
using UIKit;
using kimep.mobile.domain.Common;
using kimep.mobile.domain.Models;
using System.Net;
using CoreGraphics;

namespace kimep.mobile.today
{
    public partial class TodayViewController : SLComposeServiceViewController, INCWidgetProviding
    {
        IRequests req = new Requests();
        public TodayViewController(IntPtr handle) : base(handle)
        {
        }
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            TodayTableView.BackgroundColor = UIColor.Clear;
            ExtensionContext.SetWidgetLargestAvailableDisplayMode(NCWidgetDisplayMode.Expanded);            
        }
        [Export("widgetPerformUpdateWithCompletionHandler:")]
        public void WidgetPerformUpdate(Action<NCUpdateResult> completionHandler)
        {
            var maxSize = ExtensionContext.GetWidgetMaximumSize(NCWidgetDisplayMode.Compact);

            var webClient = new WebClient();
            webClient.DownloadStringCompleted += (s, e) =>
            {
                TodayModel today = req.Deserialize<TodayModel>(e.Result);
                InvokeOnMainThread(() =>
                {
                    TodayTableView.Source = new TodayDataSource(today, this);
                    TodayTableView.ReloadData();

                    switch (ExtensionContext.GetWidgetActiveDisplayMode())
                    {
                        case NCWidgetDisplayMode.Compact:
                            PreferredContentSize = new CGSize(maxSize.Width, 100);
                            // changes according to compact mode
                            break;
                        case NCWidgetDisplayMode.Expanded:
                            PreferredContentSize = new CGSize(maxSize.Width, TodayTableView.ContentSize.Height);
                            // changes according to expanded mode
                            break;
                    }
                });
            };
            Settings.Ticket = GetCache<Ticket>("ticket");
            if (Settings.Ticket != null)
            {                 
                string url = string.Format("https://www.kimep.kz/ext/mobile/today/index/{0}", Settings.Ticket.id);
                webClient.DownloadStringAsync(new Uri(url));
            }
            completionHandler(NCUpdateResult.NewData);
        }
        public T GetCache<T>(string key)
        {
            var plist = new NSUserDefaults("group.kz.kimep.kimepmobile", NSUserDefaultsType.SuiteName);
            var obj = plist.StringForKey(key);
                //NSUserDefaults.StandardUserDefaults.StringForKey(key);
            if (obj == null)
                return default(T);
            return req.Deserialize<T>(obj);
        }
        [Export("widgetActiveDisplayModeDidChange:withMaximumSize:")]
        public void WidgetActiveDisplayModeDidChange(NCWidgetDisplayMode activeDisplayMode, CGSize maxSize)
        {
            // Take action based on the display mode
            switch (activeDisplayMode)
            {
                case NCWidgetDisplayMode.Compact:
                    PreferredContentSize = new CGSize(maxSize.Width, 100);
                    // changes according to compact mode
                    break;
                case NCWidgetDisplayMode.Expanded:
                    PreferredContentSize = new CGSize(maxSize.Width, TodayTableView.ContentSize.Height);
                    // changes according to expanded mode
                    break;
            }
        }
    }
}
