﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace kimep.mobile.today
{
    [Register ("TodayViewController")]
    partial class TodayViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TodayTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (TodayTableView != null) {
                TodayTableView.Dispose ();
                TodayTableView = null;
            }
        }
    }
}