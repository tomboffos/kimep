﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using kimep.mobile.domain.Models;
using Newtonsoft.Json;
using System.IO;
using ModernHttpClient;

namespace kimep.mobile.domain.Common
{
    public static class Settings
    {
        public static Ticket Ticket { get; set; }
        public static PersonalModel personalInfo { get; set; }
        public static Object ProfileImage { get; set; }
	}
	public interface IRequests
	{
		Task<HttpResponseMessage> Send_Json(string param, string url);
		string Ticket(Guid id);
		string Login(int StudentID, string Password, string Token);
		string NotifyReceive(Guid id);
		string Academic_Calendar(int year, int month);
		string Personal();
		string Borrowed_Books();
		string Final_grades();
        string AssessmentScores();
        string News();
		string ReadNews(int id);
		string GPACRS();
		string Today();
		string Schedule();
        string FinalExams();
		string GetFileInfo(string FilePath);
		string Lectures(string DirectoryPath);
		string SizeSuffix(Int64 value, int decimalPlaces = 1);
		T Deserialize<T>(string obj);
		string Serialize(object obj);
		Task<int> Upload(byte[] data, string fileName);
        int WeekayToInt(string Weekday);
        string Contacts();
    }
    public class Requests : IRequests
    {   
        public async Task<int> Upload(byte[] data, string fileName)
        {
            int result = 0;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            using (NativeMessageHandler handler = new NativeMessageHandler())
            {
                using (var client = new HttpClient(handler))
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        content.Add(new StreamContent(new MemoryStream(data)), "avatar", fileName);
                        client.BaseAddress = new Uri("https://www.kimep.kz/ext/mobile/");
                        var response = await client.PostAsync(string.Format("avatar/upload/{0}", Settings.Ticket.id), content).ConfigureAwait(false);
                        if (response.IsSuccessStatusCode)
                        {
                            int.TryParse(await response.Content.ReadAsStringAsync(), out result);
                        }
                    }
                }
            }
            return result;
        }
        public async Task<HttpResponseMessage> Send_Json(string param, string url)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            using (NativeMessageHandler handler = new NativeMessageHandler())
            {
                using (var client = new HttpClient(handler))
                {
                    client.Timeout = TimeSpan.FromMilliseconds(10000);
                    client.BaseAddress = new Uri("https://www.kimep.kz/ext/mobile/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpContent contentPost = new StringContent(param, Encoding.UTF8, "application/json");
                    try
                    {
                        return await client.PostAsync(url, contentPost).ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                        throw new System.ArgumentException(ex.Message);
                    }                    
                }
            }
        }
        public string FinalExams()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/_finalexams").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string Contacts()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "contacts/contacts").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result; 
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string Ticket(Guid id)
        {
            try
            {
                string _param = JsonConvert.SerializeObject(new { id = id });
                var response = Send_Json(_param, "auth/ticket").Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException(ex.Message);
            }
            return null;
        }
        public string Login(int StudentID, string Password, string Token)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new { StudentID = StudentID, Password = Password, Token = Token });
                var response = Send_Json(param, "auth/login").Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException(ex.Message);
            }
            return null;
        }
        public string NotifyReceive(Guid id)
        {
            string param = JsonConvert.SerializeObject(new { id = id });
            var response = Send_Json(param, "notify/recieve").Result;
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }
            return null;
        }
        public string Academic_Calendar(int year, int month)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new CalendarParam { Year = year, Month = month });
                var response = Send_Json(param, "schedule/academic_calendar").Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException(ex.Message);
            }
            return null;
        }
        public string Personal()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "personal/info").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string Borrowed_Books()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "library/borrowed").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        
        public string AssessmentScores()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/AssessmentScores").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string Final_grades()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/final_grades").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string News()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "news/_news").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string ReadNews(int id)
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { ticket_id = Settings.Ticket.id, news_id = id });
                    var response = Send_Json(param, "news/read").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string GPACRS()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/GPACRS").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string Today()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "today/index").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string Schedule()
        {
            if (Settings.Ticket != null)
            {
                try
                {
                    string param = JsonConvert.SerializeObject(new { id = Settings.Ticket.id });
                    var response = Send_Json(param, "schedule/personal").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception ex)
                {
                    throw new System.ArgumentException(ex.Message);
                }
            }
            return null;
        }
        public string GetFileInfo(string FilePath)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new JsonFilePar { FilePath = FilePath });
                var response = Send_Json(param, "lectures/getfileinfo").Result;
                if (response.IsSuccessStatusCode)
                {
                   return response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException(ex.Message);
            }
            return null;
        }
        public string Lectures(string DirectoryPath)
        {
            try
            {
                string param = JsonConvert.SerializeObject(new JsonLecturesPar { DirectoryPath = DirectoryPath });
                var response = Send_Json(param, "lectures/directory").Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception ex)
            {
                throw new System.ArgumentException(ex.Message);
            }
            return null;
        }
        readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return "0.0 bytes"; }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }
        public T Deserialize<T>(string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj);
        }
        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }        
        public int WeekayToInt(string Weekday)
        {
            switch(Weekday.ToLower())
            {
                case "monday":
                    return 1;
                case "tuesday":
                    return 2;
                case "wednesday":
                    return 3;
                case "thursday":
                    return 4;
                case "friday":
                    return 5;
                case "saturday":
                    return 6;
                case "sunday":
                    return 7;
            }
            return 0;
        }
	}
}