﻿using System;

namespace kimep.mobile.domain.Models
{
    public class MenuSection
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Icon { get; set; }
        public MenuItem[] Items { get; set; }
    }
    public class MenuItem
    {
        public Guid ID { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Icon { get; set; }
        public Object Controller { get; set; }
    }
}