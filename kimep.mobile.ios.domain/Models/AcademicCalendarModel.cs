﻿using System;
using System.Collections.Generic;

namespace kimep.mobile.domain.Models
{
    public class CalendarParam
    {
        public int Month { get; set; }
        public int Year { get; set; }
    }
    [Serializable]
    
    public class acaCalendar
    {

        public int Month { get; set; }

        public int Year { get; set; }

        public List<acaEvent> Events { get; set; }
    }
    [Serializable]
    
    public class acaEvent
    {

        public int ID { get; set; }

        public int Day { get; set; }
 
        public DateTime Date { get; set; }

        public int Priority { get; set; }

        public string Semester { get; set; }

        public string Description { get; set; }
    }
}
