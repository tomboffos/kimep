﻿using System;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
    public class Schedule
    {
        public string WeekDay { get; set; }
        public MyClass[] Classes { get; set; }
    }
    public class CourseTicketParam
    {
        public int limit { get; set; }
        public Guid id { get; set; }
    }
    [Serializable]
    
    public class Mate
    {
        
        public int StudentID { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string Email { get; set; }
        
        public string Phone { get; set; }
    }
    [Serializable]
    
    public class MyClass
    {
        
        public int ID { get; set; }
        
        public bool Alert { get; set; }

        public string WeekDay { get; set; }
        
        public string Semester { get; set; }
        
        public string Title { get; set; }
        
        public string CourseID { get; set; }
        
        public string Hall { get; set; }
        public string Section { get; set; }

        public string Instructor { get; set; }
        public string LDrive { get; set; }

        public DateTime Time_From { get; set; }
        
        public DateTime? Time_To { get; set; }
        
        public DateTime Date_From { get; set; }
        
        public DateTime? Date_To { get; set; }
    }
    [Serializable]

    public class MyClassNotification
    {
        public int ID { get; set; }
        public bool Once { get; set; }
    }
}