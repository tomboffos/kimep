﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
    
    public class JsonFilePar
    {        
        public string FilePath { get; set; }
    }
    
    public class LectureFileInfo
    {
        
        public long Length { get; set; }
        
        public DateTime CreationTimeUtc { get; set; }
    }
    
    public class JsonLecturesPar
    {
        
        public string DirectoryPath { get; set; }
        
        public string KeyWord { get; set; }
    }
    public class LecturesPath
    {
        public string Path { get; set; }
        public List<LectureModel> Lectures { get; set; }
    }
    [Serializable]
    public class LectureModel
    {        
        public long ID { get; set; }
        
        public Guid UID { get; set; }
        
        public string Name { get; set; }
        
        public string FullName { get; set; }
        
        public DateTime CreationTime { get; set; }
        
        public string Icon { get; set; }
        
        public string Type { get; set; }
        
        public long Size { get; set; }
        public bool IsFavorite { get; set; }
    }
}
