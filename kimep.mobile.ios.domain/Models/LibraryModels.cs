﻿using System;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
    [Serializable]
    
    public class BorrowedBook
    {
        public long ID { get; set; }
        
        public string BookId { get; set; }
        
        public string Author { get; set; }
        
        public string Name { get; set; }
        
        public DateTime? DateDUE { get; set; }
    }
}
