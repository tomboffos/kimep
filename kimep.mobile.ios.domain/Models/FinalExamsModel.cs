﻿using System;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
    public class FinalExamsDate
    {
        public DateTime Date { get; set; }
        public FinalExam[] FinalExams { get; set; }
    }
    [Serializable]
    public class FinalExam
    {
        public string CourseID { get; set; }
        public string Group { get; set; }
        public string TitleCourses { get; set; }
        public string Hall { get; set; }
        public string Code { get; set; }
        public DateTime Time_From { get; set; }
        public DateTime Time_To { get; set; }
        public DateTime Date { get; set; }
    }
}