﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kimep.mobile.domain.Models
{
    public class AssessmentScoresSemester
    {
        public string Semester { get; set; }
        public AssessmentScoreModel[] Scores { get; set; }
    }
    [Serializable]
    public class AssessmentScoreModel
    {
        public string Semester { get; set; }
        public string Code { get; set; }
        public string TitleCourses { get; set; }
        public string Registration { get; set; }
        public Nullable<double> Score1 { get; set; }
        public Nullable<double> Score2 { get; set; }
        public Nullable<double> Score3 { get; set; }
        public string FinalAssessment { get; set; }
    }
}
