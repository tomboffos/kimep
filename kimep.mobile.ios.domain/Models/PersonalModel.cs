﻿using System;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
    [Serializable]
    
    public class PersonalModel
    {
        
        public int StudentID { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string ProgramID { get; set; }
        public DateTime? XRay { get; set; }
    }
}
