﻿using System;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
	[Serializable]	
	public partial class ShortNews
	{		
		public int ID { get; set; }		
		public string Title { get; set; }		
		public string ShortText { get; set; }		
		public System.DateTime CreatedOn { get; set; }		
		public Nullable<int> CategoryID { get; set; }		
		public string Category { get; set; }
        public string Image { get; set; }
        public DateTime? ReadOn { get; set; }
        public int Auditory { get; set; }
        public int _Auditory { get; set; }
    }
	[Serializable]	
	public partial class News
	{		
		public int ID { get; set; }		
		public string Title { get; set; }		
		public string ShortText { get; set; }		
		public string FullText { get; set; }		
		public string URL { get; set; }		
		public Nullable<long> PushID { get; set; }		
		public System.DateTime CreatedOn { get; set; }		
		public Nullable<int> CategoryID { get; set; }		
		public string Category { get; set; }		
		public DateTime? ReadOn { get; set; }
        public string[] Images { get; set; }
	}
}
