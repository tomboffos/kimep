﻿using System;

namespace kimep.mobile.domain.Models
{
    [Serializable]
    public class DepartmentModel
    {
        public string Division { get; set; }
        public ContactModel[] Contacts { get; set; }
    }
    [Serializable]
    public class ContactModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
