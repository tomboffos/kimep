﻿using System;

namespace kimep.mobile.domain.Models
{
    public class Auth
    {
        public int StudentID { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
    public class TicketParam
    {
        public Guid id { get; set; }
    }
    public class Ticket
    {
        public Guid id { get; set; }
        public DateTime ExpiredOn { get; set; }
    }
}