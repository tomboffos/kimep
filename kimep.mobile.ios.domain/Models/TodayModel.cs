﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
    [Serializable]
    
    public class TodayModel
    {
        public List<FinalExam> Exams { get; set; }
        public List<MyClass> Classes { get; set; }
        public List<acaEvent> Events { get; set; }
    }
}
