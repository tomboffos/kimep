﻿using System;
using System.Runtime.Serialization;

namespace kimep.mobile.domain.Models
{
    public class FinalGradesSemester
    {
        public string Semester { get; set; }
        public FinalGrade[] Grades { get; set; }
    }
    [Serializable]
    public class FinalGrade
    {
        public int ID { get; set; }
        public string Semester { get; set; }
        public string Title { get; set; }

        public string Grade { get; set; }

        public double? Point { get; set; }
    }
    [Serializable]
    public class GPACRS
    {
        public double GPA { get; set; }
        
        public double CreditsEarned { get; set; }
        
        public double CreditsTaken { get; set; }
    }
}
